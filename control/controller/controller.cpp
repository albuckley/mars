
#include "include/controller.h"

/***********************************************************************
 * Create usrp object with void pointer C wrapper
 **********************************************************************/
void * create_usrp_obj(char * address){
  h_usrp = new Usrp(address);
  return reinterpret_cast<void *>(h_usrp);
};


