# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/cmake-gui

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/alex/mars/control

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/alex/mars/control/build

# Include any dependencies generated for this target.
include CMakeFiles/UHD_CONTROLLER.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/UHD_CONTROLLER.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/UHD_CONTROLLER.dir/flags.make

CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o: CMakeFiles/UHD_CONTROLLER.dir/flags.make
CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o: ../controller/controller.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/alex/mars/control/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o -c /home/alex/mars/control/controller/controller.cpp

CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/alex/mars/control/controller/controller.cpp > CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.i

CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/alex/mars/control/controller/controller.cpp -o CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.s

CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o.requires:
.PHONY : CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o.requires

CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o.provides: CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o.requires
	$(MAKE) -f CMakeFiles/UHD_CONTROLLER.dir/build.make CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o.provides.build
.PHONY : CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o.provides

CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o.provides.build: CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o

# Object files for target UHD_CONTROLLER
UHD_CONTROLLER_OBJECTS = \
"CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o"

# External object files for target UHD_CONTROLLER
UHD_CONTROLLER_EXTERNAL_OBJECTS =

libUHD_CONTROLLER.so: CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o
libUHD_CONTROLLER.so: CMakeFiles/UHD_CONTROLLER.dir/build.make
libUHD_CONTROLLER.so: lib/libcontroller_methods.a
libUHD_CONTROLLER.so: CMakeFiles/UHD_CONTROLLER.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX shared library libUHD_CONTROLLER.so"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/UHD_CONTROLLER.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/UHD_CONTROLLER.dir/build: libUHD_CONTROLLER.so
.PHONY : CMakeFiles/UHD_CONTROLLER.dir/build

CMakeFiles/UHD_CONTROLLER.dir/requires: CMakeFiles/UHD_CONTROLLER.dir/controller/controller.cpp.o.requires
.PHONY : CMakeFiles/UHD_CONTROLLER.dir/requires

CMakeFiles/UHD_CONTROLLER.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/UHD_CONTROLLER.dir/cmake_clean.cmake
.PHONY : CMakeFiles/UHD_CONTROLLER.dir/clean

CMakeFiles/UHD_CONTROLLER.dir/depend:
	cd /home/alex/mars/control/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/alex/mars/control /home/alex/mars/control /home/alex/mars/control/build /home/alex/mars/control/build /home/alex/mars/control/build/CMakeFiles/UHD_CONTROLLER.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/UHD_CONTROLLER.dir/depend

