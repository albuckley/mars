/*
 * uhd_public_api.h
 * 
 * Prototypes for wrapper API expossed to controll utility
 * 
 * Alexander Buckley 2014
 * NUIM MARS - Maynooth Adaptive Radio Systems
 * 
 */

extern "C" {
  void * create_usrp_obj(char *);
  void set_carrier_frequency(void *, double);
  void make_threads(void *, const double, const double);
  void kill_threads(void);

  void set_channel(void *);

  //temp
  int kbhit (void);
  void changemode(int);
}

