/*
If a library gets compiled, all functions or class methods it offers get listed as so called symbols in a table. 
Each symbol points to the offset in the library where the corresponding code can be found.

Adding also functions or class methods to the table which are not intended to be called from the outside is of no use. 
It just bloats the table and asks others to make calls to them against your will. 
For the library myshared you do not want to add the methods of the classes MyInternalCClass and MyInternalDClass.

In the buildsystem you need to put explicitly a tag to all the classes and functions which the library should provide externally. 
This tag has to have a different name for each library.

For this add a file myshared_export.h to the directory of the library, containing:
*/

/*
This macro code defines the tag MYSHARED_EXPORT for the library.

Now you prepare all classes by adding a include for the header with the library's export tag and 
putting the export tag defined above in front of the class name, like this:

#include "myshared_export.h"
 
class MYSHARED_EXPORT MyAClass {
// class declaration as usual
};
*/


#ifndef MYSHARED_EXPORT_H
#define MYSHARED_EXPORT_H
 
// needed for KDE_EXPORT and KDE_IMPORT macros
#include <kdemacros.h>
 
#ifndef MYSHARED_EXPORT
# if defined(MAKE_MYSHARED_LIB)
   // We are building this library
#  define MYSHARED_EXPORT KDE_EXPORT
# else
   // We are using this library
#  define MYSHARED_EXPORT KDE_IMPORT
# endif
#endif
 
# ifndef MYSHARED_EXPORT_DEPRECATED
#  define MYSHARED_EXPORT_DEPRECATED KDE_DEPRECATED MYSHARED_EXPORT
# endif
 
#endif

