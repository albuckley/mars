



/*
 * test_lib_uhd.cpp
 * 
 * Test file to duplicate the action of a calling software such as Labview
 * 
 * Alexander Buckley 2014
 * NUIM MARS - Maynooth Adaptive Radio Systems
 * 
 */

#include "../uhd_public_api.h"


//#include <uhd/utils/thread_priority.hpp>
#include <iostream>

int main( int argc, char* argv[] ){
//uhd::set_thread_priority_safe();

  // new usrp handler
  void * usrp = create_usrp_obj(argv[1]);

  // set I or Q or IQ mode
  set_channel(usrp);

  // set dboard LO
  set_carrier_frequency(usrp, 2e9);

  //create a transmitter thread
  const double tx_wave_freq = 500e3;
  const double tx_wave_ampl = 25;
  make_threads(usrp, tx_wave_freq, tx_wave_ampl);

  // wait for key borad press to terminate
  std::cout<<std::endl<<"press key to terminate program"<<std::endl;
  changemode(1);
  while ( !kbhit() )
    {
      // do nothing
    }
  changemode(0);


  kill_threads();

return 0;
}
