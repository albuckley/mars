
#include "foreign_links.hpp"
#include "controller.hpp"

class Usrp
{
  public:
    Usrp(string device_addr_input){
      usrp_device_addr = device_addr_input;
      usrp_device = uhd::usrp::multi_usrp::make(string(usrp_device_addr));
    }
    uhd::usrp::multi_usrp::sptr usrp_device;
    string usrp_device_addr;
    unsigned int bob = 77;
};

Usrp * h_usrp;
