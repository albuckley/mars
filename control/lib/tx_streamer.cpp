
#include "tx_streamer.hpp"


/***********************************************************************
 * Sinusoid wave table
 **********************************************************************/
//typedef std::complex<float> samp_type;
static const size_t wave_table_len = 8192;
static const double tau = 6.28318531;
class wave_table{
  public:
    wave_table(const double ampl){
      table.resize(wave_table_len);
        for (size_t i = 0; i < wave_table_len; i++){
          //table[i] = std::complex<float>(std::polar(ampl, (tau*i)/wave_table_len));
          table[i] = std::complex<float>(ampl*sin((tau*i)/wave_table_len), ampl*sin((tau*i)/wave_table_len));
        }
    }

    inline std::complex<float> operator()(const size_t index) const{
      return table[index % wave_table_len];
    }

  private:
    std::vector<std::complex<float> > table;
};


/***********************************************************************
 * Make TX Thread C wrapper
 **********************************************************************/
void make_threads(void * usrp_handle_in, const double wave_freq, const double wave_ampl){
  // recast pointer to usrp object
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  // launch thread
  threads.create_thread(boost::bind(&transmission_thread, usrp_handle, wave_freq, wave_ampl));
}

/***********************************************************************
 * Kill TX Thread C wrapper
 **********************************************************************/
void kill_threads(void){
  //stop the transmitter
  threads.interrupt_all();
  threads.join_all();
}

/***********************************************************************
 * Transmit thread
 **********************************************************************/
void transmission_thread(Usrp * usrp_handle, const double tx_wave_freq, const double tx_wave_ampl){

  uhd::set_thread_priority_safe();

  //create a transmit streamer
  uhd::stream_args_t stream_args("fc32"); //complex floats
  uhd::tx_streamer::sptr tx_stream = usrp_handle->usrp_device->get_tx_stream(stream_args);

  //setup variables and allocate buffer
  uhd::tx_metadata_t md;
  md.has_time_spec = false;
  std::vector<std::complex<float> > buff(tx_stream->get_max_num_samps()*10);

  //values for the wave table lookup
  size_t index = 0;
  const double tx_rate = usrp_handle->usrp_device->get_tx_rate();
  const size_t step = boost::math::iround(wave_table_len * tx_wave_freq/tx_rate);
  // create table, LUT?
   wave_table table(tx_wave_ampl);

   //fill buff and send until interrupted
   while (not boost::this_thread::interruption_requested()){
     for (size_t i = 0; i < buff.size(); i++){
       buff[i] = table(index += step);
     }
     tx_stream->send(&buff.front(), buff.size(), md);
   }

   //send a mini EOB packet
   md.end_of_burst = true;
   tx_stream->send("", 0, md);
}
