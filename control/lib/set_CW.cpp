
#include "set_CW.hpp"

/***********************************************************************
 *Set Carrier Frequency 
 **********************************************************************/
void set_carrier_frequency(void * usrp_handle_in, double frequency){
  // recast pointer to usrp object
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);

  // set ant
  usrp_handle->usrp_device->set_tx_antenna("TX", 0);

  //send tx tune request
  uhd::tune_result_t tune_result;
  uhd::tune_request_t tx_tune_req(frequency);
  tx_tune_req.dsp_freq_policy = uhd::tune_request_t::POLICY_MANUAL;
  tx_tune_req.dsp_freq = 0;
  tune_result = usrp_handle->usrp_device->set_tx_freq(tx_tune_req, 0);

  // wait for lock and check result of set freq -> try locked flag?
  boost::this_thread::sleep(boost::posix_time::milliseconds(50));
  cout<<" tx freq returned: "<<usrp_handle->usrp_device->get_tx_freq()<<endl;
  return;
}
