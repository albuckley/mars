
#include "iq_DC_offset.hpp"

/***********************************************************************
 * set I or Q
 **********************************************************************/
void set_channel(void * usrp_handle_in){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  std::string subdev("AB:A:0");
  usrp_handle->usrp_device->set_tx_subdev_spec(subdev);
  return;
};
