//
// Copyright 2013 NUI Maynooth
//
// Alex Buckley
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

// IO Pin functions
#define POWER_IO     (1 << 7)   // Low enables power supply
#define ANTSW_IO     (1 << 6)   // On TX DB, 0 = TX, 1 = RX, on RX DB 0 = main ant, 1 = RX2
#define MIXER_IO     (1 << 5)   // Enable appropriate mixer
#define LOCKDET_MASK (1 << 2)   // Input pin
#define MUX_1	     (1 << 9)	// Mux control line 1
#define MUX_2	     (1 << 10)  // Mux control line 2
#define MUX_3	     (1 << 11)  // Mux control line 3

// MUX selection constants
#define M_GAIN	     0
#define M_PLL	     MUX_1
#define M_DET	     MUX_2
#define M_FIL	     MUX_1 | MUX_2
#define MUX_MASK     MUX_1 | MUX_2 | MUX_3

// Mixer constants
#define MIXER_ENB    MIXER_IO
#define MIXER_DIS    0

// Power constants
#define POWER_UP     0
#define POWER_DOWN   POWER_IO

// Antenna constants
#define ANT_TX       0          //the tx line is transmitting
#define ANT_RX       ANTSW_IO   //the tx line is receiving
#define ANT_TXRX     0          //the rx line is on txrx
#define ANT_RX2      ANTSW_IO   //the rx line in on rx2
#define ANT_XX       0          //dont care how the antenna is set

// various other constants
#define RX_GAIN_RANGE_LOW  -45
#define RX_GAIN_RANGE_HIGH  0
#define RX_GAIN_RANGE_STEP  3
#define RX_BANDWIDTH_LP 2.0*20.0e6 
#define RX_FRQ_RANGE_LOW 350e6
#define RX_FRQ_RANGE_HIGH 4e9

#include <uhd/types/dict.hpp>
#include <uhd/usrp/subdev_spec.hpp>
#include <uhd/types/ranges.hpp>
#include <uhd/types/sensors.hpp>
#include <uhd/utils/assert_has.hpp>
#include <uhd/utils/log.hpp>
#include <uhd/utils/static.hpp>
#include <uhd/utils/algorithm.hpp>
#include <uhd/utils/msg.hpp>
#include <uhd/usrp/dboard_id.hpp>
#include <uhd/usrp/dboard_base.hpp>
#include <uhd/usrp/dboard_manager.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/format.hpp>
#include <boost/bind.hpp>
#include <boost/math/special_functions/round.hpp>

// added for debug
#include <iostream>
#include <bitset>


using namespace uhd;
using namespace std;
using namespace uhd::usrp;
using namespace boost::assign;

/***********************************************************************
 * The MARSRX dboard constants
 **********************************************************************/
static const freq_range_t marsrx_freq_range(RX_FRQ_RANGE_LOW, RX_FRQ_RANGE_HIGH);
static const std::vector<std::string> marsrx_antennas = list_of("RX")("CAL");
static const uhd::dict<std::string, gain_range_t> marsrx_gain_ranges = map_list_of

    ("PGA0", gain_range_t(RX_GAIN_RANGE_LOW, RX_GAIN_RANGE_HIGH, float(RX_GAIN_RANGE_STEP)))
;

/***********************************************************************
 * The MARSRX dboard
 **********************************************************************/
class marsrx : public rx_dboard_base{
public:
    marsrx(ctor_args_t args);
    ~marsrx(void);

private:
    double       _lo_freq;
    std::string  _ant;
    uhd::dict<std::string, float> _gains;

    double set_lo_freq(double freq);
    void set_ant(const std::string &ant);
    double set_gain(float gain, const std::string &name);

    void update_atr(void);

    /*!
     * Set the LO frequency for the particular dboard unit.
     * \param unit which unit rx or tx
     * \param target_freq the desired frequency in Hz
     * \return the actual frequency in Hz
     */
    double set_lo_freq(dboard_iface::unit_t unit, double target_freq);

    // does MARS have locked sensor feedback?
    /*!
     * Get the lock detect status of the LO.
     * \param unit which unit rx or tx
     * \return true for locked
     */
    sensor_value_t get_locked(dboard_iface::unit_t unit){
        const bool locked = (this->get_iface()->read_gpio(unit) & LOCKDET_MASK) != 0;
        return sensor_value_t("LO", locked, "locked", "unlocked");
    }
 
};

/***********************************************************************
 * Register the MARSRX dboard
 **********************************************************************/
static dboard_base::sptr make_marsrx(dboard_base::ctor_args_t args){
    return dboard_base::sptr(new marsrx(args));
}

UHD_STATIC_BLOCK(reg_marsrx_dboards){
    dboard_manager::register_dboard(0x0089, &make_marsrx, "MARSRX");
}

/***********************************************************************
 * Structors
 **********************************************************************/
marsrx::marsrx(ctor_args_t args) : rx_dboard_base(args){

    ////////////////////////////////////////////////////////////////////
    // Register properties
    ////////////////////////////////////////////////////////////////////

    this->get_rx_subtree()->create<std::string>("name")
        .set(get_rx_id().to_pp_string());

    this->get_rx_subtree()->create<sensor_value_t>("sensors/lo_locked")
        .publish(boost::bind(&marsrx::get_locked, this, dboard_iface::UNIT_RX));
    
    BOOST_FOREACH(const std::string &name, marsrx_gain_ranges.keys()){
        this->get_rx_subtree()->create<double>("gains/"+name+"/value")
	    .coerce(boost::bind(&marsrx::set_gain, this, _1, name))
            .set(marsrx_gain_ranges[name].start());
        this->get_rx_subtree()->create<meta_range_t>("gains/"+name+"/range")
            .set(marsrx_gain_ranges[name]);
    }
    this->get_rx_subtree()->create<double>("freq/value")
        .coerce(boost::bind(&marsrx::set_lo_freq, this, _1))
        .set(marsrx_freq_range.start());
    this->get_rx_subtree()->create<meta_range_t>("freq/range")
        .set(marsrx_freq_range);
    this->get_rx_subtree()->create<std::string>("connection")
        .set("IQ");
    this->get_rx_subtree()->create<bool>("enabled")
        .set(true); //always enabled -- work on this
    this->get_rx_subtree()->create<bool>("use_lo_offset")
        .set(false);
    this->get_rx_subtree()->create<std::string>("antenna/value")
        .subscribe(boost::bind(&marsrx::set_ant, this, _1))
        .set("RX");
    this->get_rx_subtree()->create<std::vector<std::string> >("antenna/options")
        .set(marsrx_antennas);

    // not implemented on mars? - bandwidth not tunable?
    this->get_rx_subtree()->create<double>("bandwidth/value")
      // .coerce(boost::bind(&marsrx::set_bandwidth, this, _1))
        .set(RX_BANDWIDTH_LP); //_bandwidth in lowpass, convert to complex bandpass
    //this->get_rx_subtree()->create<meta_range_t>("bandwidth/range")
    //    .set(_bandwidth_range);
    
    //enable the clocks that we need
    this->get_iface()->set_clock_enabled(dboard_iface::UNIT_RX, true);

    //set the gpio directions
    boost::uint16_t output_enables = POWER_IO | ANTSW_IO | MIXER_IO;
    this->get_iface()->set_pin_ctrl(dboard_iface::UNIT_RX, output_enables);
    this->get_iface()->set_gpio_ddr(dboard_iface::UNIT_RX, output_enables);
    this->get_iface()->set_gpio_ddr(dboard_iface::UNIT_RX, MUX_MASK, MUX_MASK);
    // this->get_iface()->set_pin_ctrl(dboard_iface::UNIT_RX, POWER_UP, POWER_IO);

    //setup the rx atr
    update_atr();

    //Manually set the mixer gpio pin
    this->get_iface()->set_gpio_out(dboard_iface::UNIT_RX, MIXER_ENB, MIXER_IO);

    //Manually set the power gpio pin
     this->get_iface()->set_gpio_out(dboard_iface::UNIT_RX, POWER_UP, POWER_IO);

    //set some default values
    set_lo_freq((marsrx_freq_range.start() + marsrx_freq_range.stop())/2.0);

    BOOST_FOREACH(const std::string &name, marsrx_gain_ranges.keys()){
        set_gain(marsrx_gain_ranges[name].start(), name);
    }

}

marsrx::~marsrx(void){
    /* NOP */
}

/***********************************************************************
 * Antenna Handling
 **********************************************************************/
void marsrx::update_atr(void){
    //setup the rx atr (this does not change with antenna)
    this->get_iface()->set_atr_reg(dboard_iface::UNIT_RX, dboard_iface::ATR_REG_IDLE,        POWER_UP | ANT_XX | MIXER_DIS);
    this->get_iface()->set_atr_reg(dboard_iface::UNIT_RX, dboard_iface::ATR_REG_TX_ONLY,     POWER_UP | ANT_XX | MIXER_DIS);
    this->get_iface()->set_atr_reg(dboard_iface::UNIT_RX, dboard_iface::ATR_REG_RX_ONLY,     POWER_UP | ANT_RX | MIXER_ENB);
    this->get_iface()->set_atr_reg(dboard_iface::UNIT_RX, dboard_iface::ATR_REG_FULL_DUPLEX, POWER_UP | ANT_RX2| MIXER_ENB);

}


void marsrx::set_ant(const std::string &ant){
    //validate input
    assert_has(marsrx_antennas, ant, "mars rx antenna name");

    //set the rx atr regs that change with antenna setting
    if (ant == "CAL") {
        this->get_iface()->set_atr_reg(dboard_iface::UNIT_RX, dboard_iface::ATR_REG_RX_ONLY,     POWER_UP | ANT_TX | MIXER_ENB);
        this->get_iface()->set_atr_reg(dboard_iface::UNIT_RX, dboard_iface::ATR_REG_FULL_DUPLEX, POWER_UP | ANT_TX | MIXER_ENB);
    } 
    else {
        update_atr();
    }
    //shadow the setting
    _ant = ant;
}

/***********************************************************************
 * Gain Handling
 **********************************************************************/
double marsrx::set_gain(float gain, const std::string &name){
    assert_has(marsrx_gain_ranges.keys(), name, "marsrx gain name");
    if(name == "PGA0"){
    	boost::uint32_t reg = boost::uint32_t(15 + gain/3);

        _gains[name] = gain;

        //Set up the GPIO pins and write to the spi bus
        this->get_iface()->set_gpio_out(dboard_iface::UNIT_RX, M_GAIN, MUX_MASK);
	this->get_iface()->write_spi(
	    dboard_iface::UNIT_RX, spi_config_t::EDGE_RISE,
	    reg, 8
	);
    }
    else UHD_THROW_INVALID_CODE_PATH();
    return gain;
}

/***********************************************************************
 * Tuning
 **********************************************************************/
double marsrx::set_lo_freq(double freq){
    _lo_freq = set_lo_freq(dboard_iface::UNIT_RX, freq);
    return _lo_freq;
}

double marsrx::set_lo_freq(
    dboard_iface::unit_t unit,
    double target_freq
){
    UHD_LOGV(often) << boost::format(
    	"MARSRX tune: target frequency %f Mhz"
    ) % (target_freq/1e6) << std::endl;

    //clip the input and convert to MHz
    target_freq = marsrx_freq_range.clip(target_freq);
    target_freq = target_freq/1e6;
    
    // default LO register values (default with clock ref of 64 MHz)
    boost::uint32_t mars_registers[6] = {	0x012C0000,
						0x0000FD01,
						0x18040EC2,
						0x000004B3,
						0x0005003C,
						0x00C00005};

    // get reference clock value and set registers to use 100 MHz if appropriate
    double ref_freq = this->get_iface()->get_clock_rate(unit);
    if((ref_freq != 6.4e+07)&&(ref_freq != 1e+08)){
      cout<<"Unsupported reference clock rate, must be 64 Mhz or 100MHz"<<endl;
    }
    if(ref_freq == 1e+08){
      mars_registers[2] = 0x18064EC2;
    }

    boost::uint32_t INT = 0;
    boost::uint32_t DIV = 0;
    boost::uint32_t MOD = 0;
    boost::uint32_t FRAC = 0;
    boost::uint32_t prescaler = 0;

    boost::uint32_t int_mask = 65535<<15;
    boost::uint32_t div_mask = 7<<20;
    boost::uint32_t mod_mask = 4095<<3;
    boost::uint32_t frac_mask = 4095<<3;
    boost::uint32_t prescaler_mask = 1<<27;

    double freq = target_freq;
    double PFD = 4;              // Comparison frequency (can be input)
    double step = 0.001;         // Step frequency (can be input) 


    // determine prescaler value
    if(target_freq > 3000){
      prescaler = 1;
    }
    else{
      prescaler = 0;
    }
    
    // Determine DIV based on req’d frequency
    if(350 <= freq && freq < 550)
    {
     	DIV = 3;
    }
    else if(550 <= freq && freq < 1100)
    {
     	DIV = 2;
    }
    else if(1100 <= freq && freq < 2200)
    {
     	DIV = 1;
    }
    else if(2200 <= freq && freq <= 4000)
    {
     	DIV = 0;
    }
    
    // Round freq to nearest step 	
    freq = (double)boost::math::lround((freq / step)) * step;
    // Modulus
    MOD = PFD/step;
    // Integer value of freq/PFD
    INT = (boost::uint32_t)floor(freq/PFD);
    // Fractional value
    FRAC = (boost::uint32_t)(freq-(INT*PFD))*MOD/PFD;

    // overwrite bits if different form default
    mars_registers[4] = (mars_registers[4] & (~div_mask)) | (DIV<<20);
    mars_registers[1] = (mars_registers[1] & (~mod_mask)) | (MOD<<3);
    mars_registers[1] = (mars_registers[1] & (~prescaler_mask)) | (prescaler<<27);
    mars_registers[0] = (mars_registers[0] & (~int_mask)) | (INT<<15);
    mars_registers[0] = (mars_registers[0] & (~frac_mask)) | (FRAC<<3);

    //Set the gpio lines to control the MUX
    this->get_iface()->set_gpio_out(dboard_iface::UNIT_RX, M_PLL, MUX_MASK);  

    //write the registers
    //correct power-up sequence to write registers (5, 4, 3, 2, 1, 0)
    int addr;
    for(addr=5; addr>=0; addr--){
	this->get_iface()->write_spi(
	    unit, spi_config_t::EDGE_RISE,
	    mars_registers[addr], 32
        );
    }
    //cout<<"RX freq exit"<<endl;
    //return the actual frequency
    UHD_LOGV(often) << boost::format(
	    "MARSRX tune: actual frequency %f Mhz"
    ) % freq << std::endl;
    return freq*1e6;
}
