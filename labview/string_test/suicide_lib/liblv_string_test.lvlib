﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="13008000">
	<Property Name="NI.Lib.Icon" Type="Bin">%Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,"!!!*Q(C=\:;P&lt;BN"%)&gt;`;3-FL$)L+'B=3XG!I977_A2_B3EX-1U)-+CKU#(^%V,*,R"A5"AS+.SP9'KY`7ZPH&lt;K6($&gt;3+B6UVL.X^ZP&gt;G?`WVK?4KJV+RSK(4!`:BPCID$JD;08_MD^O\GL]JFJJP`,TYPLX?/HVP@&amp;$]_`D.VWG&lt;&lt;R03\=^+&lt;`%&lt;`::@``4[`V7ZY`+TCW0NJ?&gt;47N]/CX4XMIPB_HU?C&gt;?3O&gt;F'S^&gt;PRPPOL+.03Z?NLH\`+X@K&lt;`(_PP@(T]U`X`]+?+&lt;B`@@ZOYB_Z/`^]8"18`:J#.V,S)]]=!&gt;.X7BEOC*HOC*HOC*(OC"(OC"(OC"\OC/\OC/\OC/&lt;OC'&lt;OC'&lt;OC'8BO[U)5O&gt;&amp;9F+:Y53J)G#:,"I#DJ%J[%*_&amp;*?,B5QJ0Q*$Q*4],$%#5]#5`#E`!E0%R4QJ0Q*$Q*4]*$KEK3N;($E`#18A&amp;0Q"0Q"$Q"$S56]!1!1&lt;%A=:!%$!8"Y#4A#8A#(EY6]!1]!5`!%`!16M!4]!1]!5`!QZ3[+F&amp;JOI9/$WHE]$A]$I`$Y`#17A[0Q_0Q/$Q/$_8E]$A]$I24U%E/AJR*TA$HQO&amp;R?$D)Y8&amp;Y("[(R_%B6*_1VZ8J;,K'$I`"9`!90!;0Q5-+'4Q'D]&amp;D]"A]J*8"9`!90!;0Q5-J'4Q'D]&amp;DA"B&amp;+3]DG4(2''1)"A_`OFOM0K7I*&amp;;&lt;V$;PWK:5WWRKGUBN=[A^&gt;,7(K@;1V":@&lt;6(6&amp;ENN%&gt;2O4AV;$5;NC.LE&lt;K$7^#N]C3`Q/4\$*`A9(_+$&lt;OI4$VSPVVKN6FIOFVIM&amp;JL0ZZL.:JJ-*BK0RRI/BRI-"P?@%[^JP2VNXUM8H&amp;`.TNZ=D&lt;[?8ZW@H&gt;@D[-PNJ^H&lt;7W)P0D0GQ_DF]]PDUZ0,^[@@,E@T[B^HL^ZV=@TE-&gt;\F_X\`8PI8XIV[JL,T'=9;`1!O1?Z1!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">318799872</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="add one.vi" Type="VI" URL="../VIs/add one.vi"/>
	<Item Name="create obj test.vi" Type="VI" URL="../VIs/create obj test.vi"/>
	<Item Name="liblv_string_test.so" Type="Document" URL="/usr/local/lib/liblv_string_test.so"/>
	<Item Name="pass ptr test 2.vi" Type="VI" URL="../VIs/pass ptr test 2.vi"/>
	<Item Name="pass ptr test.vi" Type="VI" URL="../VIs/pass ptr test.vi"/>
	<Item Name="test star.vi" Type="VI" URL="../VIs/test star.vi"/>
</Library>
