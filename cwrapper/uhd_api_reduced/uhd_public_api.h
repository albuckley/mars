/*
 * uhd_public_api.h
 * 
 * Prototypes for wrapper API expossed to controll utility
 * 
 * Alexander Buckley 2014
 * NUIM MARS - Maynooth Adaptive Radio Systems
 * 
 */

extern "C" {
  void * create_usrp_obj(char *);
  unsigned int boost_round_func(void);
  void make_type(void);
  //void make_sensor(void);
  //void make_streamCMD(void);
}


