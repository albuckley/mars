/*
 * lv_uhd_interface.cpp
 * 
 * Contains UHD wrapped functions
 * 
 * Alexander Buckley 2014
 * NUIM MARS - Maynooth Adaptive Radio Systems
 * 
 */

#include "uhd_public_api.h"
#include "lv_uhd_internals.hpp"

/***********************************************************************
 * Create usrp object with void pointer C wrapper
 **********************************************************************/
void * create_usrp_obj(char * address){
  h_usrp = new Usrp(address);
  return reinterpret_cast<void *>(h_usrp);
};

unsigned int boost_round_func(void){
  try{
    return  boost::math::iround(0.57);
  }
  catch(int e){
    cout << "An exception occurred. Exception Nr. " << e << '\n';
  }
  return 0;
}


void make_type(void){
  size_t b = 0;
}
/*  
void make_sensor(void){
  string name = "name";
  string utrue = "utrue";
  string ufalse = "ufalse";
  bool value = true;

 sensor_value_t sensor = sensor_value_t(
            name,
            value,
            utrue,
            ufalse
        );

  }
*/
/*
void make_streamCMD(void){
  uhd::stream_cmd_t stream_cmd(uhd::stream_cmd_t::STREAM_MODE_NUM_SAMPS_AND_DONE);
}
*/  

/*
void make_dbID_type(boost::uint16_t input){
  dboard_id_t larry;
  larry = uhd::usrp::dboard_id_t(input);
}
*/

