/*
 * lv_uhd_internals.hpp
 * 
 * Private function protypes for lv_uhd_interface.cpp
 * 
 * Alexander Buckley 2014
 * NUIM MARS - Maynooth Adaptive Radio Systems
 * 
 */

#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/device.hpp>
#include <uhd/config.hpp>
#include <uhd/device.hpp>
#include <uhd/types/dict.hpp>
#include <uhd/types/stream_cmd.hpp>
#include <uhd/types/sensors.hpp>

#include <uhd/property_tree.hpp>
#include <uhd/utils/thread_priority.hpp>
#include <uhd/utils/algorithm.hpp>
#include <uhd/usrp/dboard_id.hpp>

//#include <boost/thread.hpp>
#include <boost/math/special_functions/round.hpp>

#include <iomanip>
#include <stdio.h>
#include <iostream>
#include <complex>

using namespace uhd;
using namespace usrp;
using namespace std;



class Usrp
{
  public:
    Usrp(string device_addr_input){
      usrp_device_addr = device_addr_input;
      //usrp_device = uhd::usrp::multi_usrp::make(string(usrp_device_addr));
      //found_addr = uhd::device::find(usrp_device_addr);
    }
  //uhd::usrp::multi_usrp::sptr usrp_device;
//uhd::device_addrs_t found_addr;
    string usrp_device_addr;
    unsigned int bob = 77;
};

Usrp * h_usrp;
