/*
 * lv_uhd_interface.cpp
 * 
 * Contains UHD wrapped functions
 * 
 * Alexander Buckley 2014
 * NUIM MARS - Maynooth Adaptive Radio Systems
 * 
 */

#include "uhd_public_api.h"
#include "lv_uhd_internals.hpp"

/***********************************************************************
 * Create usrp object with void pointer C wrapper
 **********************************************************************/
void * create_usrp_obj(char * address){
  h_usrp = new Usrp(address);
  //std::cout<<"address from create object: " <<h_usrp<<"\n";
  return reinterpret_cast<void *>(h_usrp);
};

/***********************************************************************
 * set I or Q
 **********************************************************************/
void set_channel(void * usrp_handle_in){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  std::string subdev("A:0");
  usrp_handle->usrp_device->set_tx_subdev_spec(subdev);
  return;
};

/***********************************************************************
 * Set IQ Balance
 **********************************************************************/
void set_tx_iq_balance_ratio(void * usrp_handle_in, std::complex<double> correction){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  usrp_handle->usrp_device->set_tx_iq_balance(correction);
  return;
};

void set_rx_iq_balance_ratio(void * usrp_handle_in, std::complex<double> correction){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  usrp_handle->usrp_device->set_rx_iq_balance(correction);
  return;
};

/***********************************************************************
 * Set TX DC Offset
 **********************************************************************/
void set_tx_dc_offset(void * usrp_handle_in, std::complex<double> correction){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  usrp_handle->usrp_device->set_tx_dc_offset(correction);
  return;
};

void set_rx_dc_offset(void * usrp_handle_in, std::complex<double> correction){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  usrp_handle->usrp_device->set_rx_dc_offset(correction);
  return;
};

/***********************************************************************
 * Set Sample Rate
 **********************************************************************/
void assign_tx_rate(void * usrp_handle_in, double rate){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  usrp_handle->usrp_device->set_tx_rate(rate);
  return;
};

void assign_rx_rate(void * usrp_handle_in, double rate){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  usrp_handle->usrp_device->set_rx_rate(rate);
  return;
};

/***********************************************************************
 * Set Gain
 **********************************************************************/
void assign_tx_gain(void * usrp_handle_in, double gain){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  usrp_handle->usrp_device->set_tx_gain(gain);
  return;
};

void assign_rx_gain(void * usrp_handle_in, double gain){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  usrp_handle->usrp_device->set_rx_gain(gain);
  return;
};

/***********************************************************************
 * Set Antenna - must edit my c++ test file to use this now
 **********************************************************************/
void set_tx_antenna(void * usrp_handle_in, char * antenna){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  usrp_handle->usrp_device->set_tx_antenna(antenna, 0);
}

void set_rx_antenna(void * usrp_handle_in, char * antenna){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  usrp_handle->usrp_device->set_rx_antenna(antenna, 0);
}

/***********************************************************************
 * Set Center Frequency  
 **********************************************************************/
void set_tx_center_frequency(void * usrp_handle_in, double frequency){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  
  /* send tx tune request */
  uhd::tune_result_t tune_result;
  uhd::tune_request_t tx_tune_req(frequency);
  tx_tune_req.dsp_freq_policy = uhd::tune_request_t::POLICY_MANUAL;
  tx_tune_req.dsp_freq = 0;
  tune_result = usrp_handle->usrp_device->set_tx_freq(tx_tune_req, 0);

  /* wait for lo lock  */
  boost::this_thread::sleep(boost::posix_time::milliseconds(0.1));
  boost::system_time start = boost::get_system_time();
  while (not usrp_handle->usrp_device->get_tx_sensor("lo_locked").to_bool()){
    if (boost::get_system_time() > start + boost::posix_time::milliseconds(1000)){
      throw std::runtime_error("timed out waiting for TX LO to lock");
    }
  }
  return;
}

void set_rx_center_frequency(void * usrp_handle_in, double frequency){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  
  /* send rx tune request */
  uhd::tune_result_t tune_result;
  uhd::tune_request_t rx_tune_req(frequency);
  rx_tune_req.dsp_freq_policy = uhd::tune_request_t::POLICY_MANUAL;
  rx_tune_req.dsp_freq = 0;
  tune_result = usrp_handle->usrp_device->set_rx_freq(rx_tune_req, 0);

  /* wait for lo lock  */
  boost::this_thread::sleep(boost::posix_time::milliseconds(0.1));
  boost::system_time start = boost::get_system_time();
  while (not usrp_handle->usrp_device->get_rx_sensor("lo_locked").to_bool()){
    if (boost::get_system_time() > start + boost::posix_time::milliseconds(1000)){
      throw std::runtime_error("timed out waiting for RX LO to lock");
    }
  }
  return;
}

/***********************************************************************
 * Get Lo locked signal
 **********************************************************************/
bool get_tx_lo_locked(void * usrp_handle_in){
  bool locked = false;
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  locked = usrp_handle->usrp_device->get_tx_sensor("lo_locked").to_bool();
  return locked;
}

bool get_rx_lo_locked(void * usrp_handle_in){
  bool locked = false;
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  locked = usrp_handle->usrp_device->get_rx_sensor("lo_locked").to_bool();
  return locked;
}

/***********************************************************************
 * Update TX Tone
 **********************************************************************/
void update_transmission_tone(double new_tx_wave_freq, double new_tx_wave_ampl){
  pthread_mutex_lock(&tx_lock);
  tx_wave_f    = new_tx_wave_freq;
  tx_wave_ampl = new_tx_wave_ampl;
  change_tone = true;
  pthread_mutex_unlock(&tx_lock);
}

/***********************************************************************
 * return data functions
 **********************************************************************/
/*
boost::python::list return_cal_data(void){
  //PyObject* return_cal_data(void){
  pthread_mutex_lock(&rx_lock);
  cal_data[0] = tone_dbrms;
  cal_data[1] = imag_dbrms;
  cal_data[2] = suppression;
  pthread_mutex_unlock(&rx_lock);
  return std_vector_to_py_list(cal_data);
}
*/
double return_cal_data_SB_supression(void){
  double return_supression;

  pthread_mutex_lock(&rx_lock);
  //std::cout<<"\t\t\t\t\t\t GET DAT FROM WRAPPER SB"<<std::endl;
  return_supression = SB_suppression;
  pthread_mutex_unlock(&rx_lock);
  boost::this_thread::sleep(boost::posix_time::milliseconds(1));
  return return_supression;
}

double return_cal_data_CW_power(void){
  double return_DC_pwr;
  pthread_mutex_lock(&rx_lock);
  //std::cout<<"\t\t\t\t\t\t GET DAT FROM WRAPPER DC"<<std::endl;
  return_DC_pwr = DC_power;
  pthread_mutex_unlock(&rx_lock);
  boost::this_thread::sleep(boost::posix_time::milliseconds(1));
  return return_DC_pwr;
}

double return_tone_dbrms(void){
  double return_data;
  pthread_mutex_lock(&rx_lock);
  //std::cout<<"\t\t\t\t\t\t GET DAT FROM WRAPPER TONE RMS"<<std::endl;
  return_data = tone_dbrms;
  pthread_mutex_unlock(&rx_lock);
  boost::this_thread::sleep(boost::posix_time::milliseconds(1));
  return return_data;
}

double return_imag_dbrms(void){
  double return_data;
  pthread_mutex_lock(&rx_lock);
  //std::cout<<"\t\t\t\t\t\t GET DAT FROM WRAPPER TONE RMS"<<std::endl;
  return_data = imag_dbrms;
  pthread_mutex_unlock(&rx_lock);
  boost::this_thread::sleep(boost::posix_time::milliseconds(1));
  return return_data;
}

/***********************************************************************
 * Create a wave table
 **********************************************************************/
wave_table * make_wave_table(double signal_amp){
  wave_table * wave_LUT = new wave_table(signal_amp);
  return wave_LUT;
};

/***********************************************************************
 * Make Threads 
 **********************************************************************/
void make_tx_thread(void * usrp_handle_in){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  tx_thread.create_thread(boost::bind(&transmission_thread, usrp_handle));
}

void make_rx_thread(void * usrp_handle_in){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  rx_thread.create_thread(boost::bind(&receiver_thread, usrp_handle));
}

/***********************************************************************
 * Kill Threads 
 **********************************************************************/
void kill_tx_thread(void){
  /* stop the transmitter */
  tx_thread.interrupt_all();
  tx_thread.join_all();
}

void kill_rx_thread(void){
  /* stop the transmitter */
  rx_thread.interrupt_all();
  rx_thread.join_all();
}
/***********************************************************************
 * Transmit thread
 **********************************************************************/
  void transmission_thread(Usrp * usrp_handle){
  uhd::set_thread_priority_safe();

  /* create a transmit streamer */
  uhd::stream_args_t stream_args("fc32"); //complex floats
  uhd::tx_streamer::sptr tx_stream = usrp_handle->usrp_device->get_tx_stream(stream_args);

  /* setup variables and allocate buffer */
  uhd::tx_metadata_t md;
  md.has_time_spec = false;
  std::vector<std::complex<float> > buff(tx_stream->get_max_num_samps()*10);

  //std::cout<< "max samps "<<tx_stream->get_max_num_samps()<<"\n"<<std::endl;
  /* values for the wave table lookup */
  size_t index = 0;
  const double tx_rate = usrp_handle->usrp_device->get_tx_rate();
  size_t step = boost::math::iround(wave_table_len * tx_wave_f/tx_rate);
  std::cout<< "tx_rate "<<tx_rate<<"\n"<<std::endl;

  /* create signal look up table */
  wave_table table(tx_wave_ampl);
  //float test = 2;
  /* fill buff and send until interrupted */

  size_t n = 0;
  size_t period = tx_rate;

  while (not boost::this_thread::interruption_requested()){

    /* update signal tone if requested */
    pthread_mutex_lock(&tx_lock);
    if(change_tone){
      step = boost::math::iround(wave_table_len * tx_wave_f/tx_rate);
      change_tone = false;
    }
    pthread_mutex_unlock(&tx_lock); 

    for (size_t i = 0; i < buff.size(); i++){
      buff[i] = table(index += step);
      //buff[i] = tx_wave_ampl * sin(2*pi * n * tx_rate * tx_wave_f);
      //n = n + 1;
      //n = n % period;
    }
    tx_stream->send(&buff.front(), buff.size(), md);

  }// end while

   /* send a mini EOB packet */
  md.end_of_burst = true;
  tx_stream->send("", 0, md);
}

/***********************************************************************
 * Receiver thread
 **********************************************************************/
void receiver_thread(Usrp * usrp_handle_in){
  Usrp * usrp_handle = reinterpret_cast<Usrp*>(usrp_handle_in);
  uhd::set_thread_priority_safe();

  size_t nsamps = default_num_samps;

  // create a receive streamer 
  uhd::stream_args_t stream_args("fc32"); //complex floats
  uhd::rx_streamer::sptr rx_stream = usrp_handle->usrp_device->get_rx_stream(stream_args);

  //re-usable buffer for samples
  std::vector<samp_type> buff;

  //store the results here
  std::vector<result_t> results;

  //frequency constants for this tune event
  double actual_rx_rate = usrp_handle->usrp_device->get_rx_rate();
  double actual_tx_freq = usrp_handle->usrp_device->get_tx_freq();
  double actual_rx_freq = usrp_handle->usrp_device->get_rx_freq();
  //double bb_tone_freq = actual_tx_freq - actual_rx_freq;
  //double bb_imag_freq = -bb_tone_freq;
  double bb_tone_freq = -tx_wave_f;
  double bb_imag_freq = tx_wave_f;

  while (not boost::this_thread::interruption_requested()){

    //actual_tx_freq = usrp_handle->usrp_device->get_tx_freq();
    //actual_rx_freq = usrp_handle->usrp_device->get_rx_freq();
    //bb_tone_freq = actual_tx_freq - actual_rx_freq;
    //bb_imag_freq = -bb_tone_freq;

    // for LSB
    pthread_mutex_lock(&rx_lock);
    bb_tone_freq = -tx_wave_f;
    bb_imag_freq = tx_wave_f;
    pthread_mutex_unlock(&rx_lock);

    //receive some samples
   
     boost::this_thread::sleep(boost::posix_time::milliseconds(1));
    //boost::thread::yield();

    pthread_mutex_lock(&rx_lock);
    // std::cout<<"\t\t\t RX THREAD DATA CAPTURE"<<std::endl;
    capture_samples(usrp_handle->usrp_device, rx_stream, buff, nsamps);
    tone_dbrms = compute_tone_dbrms(buff, bb_tone_freq/actual_rx_rate);
    imag_dbrms = compute_tone_dbrms(buff, bb_imag_freq/actual_rx_rate);
    SB_suppression = -(tone_dbrms - imag_dbrms);
    DC_power = compute_tone_dbrms(buff, 0/actual_rx_rate);
    pthread_mutex_unlock(&rx_lock);
    //std::cout<<"tone_dbrms "<<tone_dbrms<<"\t imag_dbrms "<<imag_dbrms<<"\t SB_suppression   "<<SB_suppression<<"\t bb_tone_freq "<<bb_tone_freq<<"\t bb_imag_freq "<<bb_imag_freq<<"\t DC_power "<<DC_power<<std::endl;

    //boost::this_thread::sleep(boost::posix_time::milliseconds(200));
     

  }// end while

}
