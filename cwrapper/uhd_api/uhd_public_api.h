/*
 * uhd_public_api.h
 * 
 * Prototypes for wrapper API expossed to controll utility
 * 
 * Alexander Buckley 2014
 * NUIM MARS - Maynooth Adaptive Radio Systems
 * 
 */
#include <stdio.h>
#include <complex>
#include <vector>

#include <boost/python.hpp>
#include <boost/python/module.hpp>


using namespace std;
using namespace boost::python;
using boost::python::list;
namespace py = boost::python;
/*
template<class T>
struct std_vector_to_py_list
{
  static PyObject* convert(const std::vector<T> & v)
  {
    py::object get_iter = py::iterator<std::vector<T> >();
    py::object iter = get_iter(v);
    py::list l(iter);
    return l;
  }
};
*/

extern "C" {
  void * create_usrp_obj(char *);

  void set_tx_center_frequency(void *, double);
  void set_rx_center_frequency(void *, double);

  void assign_tx_gain(void *, double);
  void assign_rx_gain(void *, double);

  bool get_tx_lo_locked(void *);
  bool get_rx_lo_locked(void *);

  void assign_tx_rate(void *, double);
  void assign_rx_rate(void *, double);

  void update_transmission_tone(double, double);
  //py::list return_cal_data(void);
  //PyObject* return_cal_data(void);
  double return_cal_data_SB_supression(void);
  double return_cal_data_CW_power(void);
  double return_tone_dbrms(void);
  double return_imag_dbrms(void);

  void make_tx_thread(void *);
  void make_rx_thread(void *);
  void kill_tx_thread(void);
  void kill_rx_thread(void);

  void set_channel(void *);
  void set_tx_antenna(void *, char *);
  void set_rx_antenna(void *, char *);

  void set_tx_iq_balance_ratio(void *, std::complex<double>);
  void set_rx_iq_balance_ratio(void *, std::complex<double>);
  void set_tx_dc_offset(void *, std::complex<double>);
  void set_rx_dc_offset(void *, std::complex<double>);

  //temp
  int kbhit (void);
  void changemode(int);
}



BOOST_PYTHON_MODULE(liblv_uhd_pub_api)
{
  using namespace boost::python;
  
  // register the to-python converter
  /*
  to_python_converter<
    std::vector<double>,
    std_vector_to_py_list>();
  */
  def("create_usrp_obj", create_usrp_obj, return_value_policy<return_opaque_pointer>());
  def("set_tx_center_frequency", set_tx_center_frequency);
  def("set_rx_center_frequency", set_rx_center_frequency);
  def("assign_tx_gain", assign_tx_gain);
  def("assign_rx_gain", assign_rx_gain);
  def("get_lo_tx_locked", get_tx_lo_locked);
  def("get_lo_rx_locked", get_rx_lo_locked);
  def("assign_tx_rate", assign_tx_rate);
  def("assign_rx_rate", assign_rx_rate);
  def("update_transmission_tone", update_transmission_tone);
  def("return_cal_data_SB_supression", return_cal_data_SB_supression);
  def("return_cal_data_CW_power", return_cal_data_CW_power);
  def("return_tone_dbrms", return_tone_dbrms);
  def("return_imag_dbrms", return_imag_dbrms);
  def("make_tx_thread", make_tx_thread);
  def("make_rx_thread", make_rx_thread);
  def("kill_tx_thread", kill_tx_thread);
  def("kill_rx_thread", kill_rx_thread);
  def("set_channel", set_channel);
  def("set_tx_antenna", set_tx_antenna);
  def("set_rx_antenna", set_rx_antenna);
  def("set_tx_iq_balance_ratio", set_tx_iq_balance_ratio);
  def("set_rx_iq_balance_ratio", set_rx_iq_balance_ratio);
  def("set_tx_dc_offset", set_tx_dc_offset);
  def("set_rx_dc_offset", set_rx_dc_offset);
}
/*
template<typename T>
struct Vector_to_python_list
{
   
  static PyObject* convert(std::vector<T> const& v)
  {

    list l;
    typename vector<T>::const_iterator p;
    for(p=v.begin();p!=v.end();++p){
      l.append(object(*p));
    }
    return incref(l.ptr());
  }
};

// register the to-python converter
  to_python_converter<
    std::vector<double>,
    Vector_to_python_list<double> 
>();


*/
