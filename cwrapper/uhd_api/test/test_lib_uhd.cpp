/*
 * test_lib_uhd.cpp
 * 
 * Test file to duplicate the action of a calling software such as Labview
 * 
 * Alexander Buckley 2014
 * NUIM MARS - Maynooth Adaptive Radio Systems
 * 
 */

#include "../uhd_public_api.h"

#include <boost/thread/thread.hpp>
#include <curses.h>
#include <iostream>

double tx_wave_freq         = 507.123e3;
double tx_wave_ampl         = 25;
double default_tx_rate      = 12.5e6;
double default_carrier_f    = 2e9;
double default_carrier_gain = 15.75;

static const double tau = 6.28318531;

int main( int argc, char* argv[] ){

  /* new usrp handler */
  void * usrp = create_usrp_obj(argv[1]);

  /* set some optimum defaults */
  assign_tx_rate(usrp, default_tx_rate);
  assign_tx_gain(usrp, default_carrier_gain);

  /* set carrier f */
  set_carrier_frequency(usrp, default_carrier_f);

  /*create a transmitter thread (uses default tone and ampl) */
  make_threads(usrp);

  /* reset tone and ampl */
  update_transmission_tone(tx_wave_freq, tx_wave_ampl);

  /* initialise iq correction  (ampl_corr, phase_corr) */
  const std::complex<double> correction(0, 0);
  set_tx_iq_balance_ratio(usrp, correction);

  /* set up curses window properties */
  initscr(); // Initalise the ncurses library 
  cbreak();  // option to disable buffering etc.
  noecho();  // disable the echoing of keystrokes
  refresh(); // update display

  /* program begin display header */
  std::cout<<std::endl<<"Tone Transmission and Brute Force IQ Balance Tests: press key to start \r\n";
  std::cout<<std::endl<<"current tone = "<<tx_wave_freq<<" \r\n\n";
  refresh();
  getch();   // pause

  /*************************************************/
  /* Carrier frequency and gain tests              */
  /*************************************************/
  double phase_inc = 0.03; // test signal speed control used by all tests
  double phase = 0;
  double test_CW_freq, test_CW_gain;
  double pk_variance_cwf = 500e3;
  double pk_variance_cwG = 15.75;
  double input_char = 'A';

  /**************************/
  /* Carrier frequency test */  // pass
  /**************************/
  std::cout<<std::endl<<"Test varying Carrier Frequency: press key to start, 'k' to escape \r\n\n";
  refresh();
  getch();               // pause to begin
  nodelay(stdscr, TRUE); // set getch() non-blocking

  /* vary CW_f until user terminates with 'k' */
  while(input_char != 'k'){
    test_CW_freq = default_carrier_f + pk_variance_cwf * sin(tau*phase);
    phase = phase + phase_inc;
    set_carrier_frequency(usrp, test_CW_freq);
    std::cout<<"current test CW_f = "<<test_CW_freq<<"\r";
    refresh();
    boost::this_thread::sleep( boost::posix_time::milliseconds(200) );
    input_char = getch();  
  }
  /* return CW_f to default value */
  set_carrier_frequency(usrp, default_carrier_f);
  std::cout<<std::endl<<"\n reset CW_f = "<<default_carrier_f<<" \r\n\n";
  nodelay(stdscr, FALSE); // return getch() to blocking (default)



  /**************************/
  /* Carrier gain test      */  // pass though sbx clearly can not do 31.5db
  /**************************/
  std::cout<<std::endl<<"Test varying Carrier Gain: press key to start, 'k' to escape \r\n\n";
  refresh();
  getch();               // pause to begin
  nodelay(stdscr, TRUE); // set getch() non-blocking
  input_char = 'A';

  /* vary CW_f until user terminates with 'k' */
  while(input_char != 'k'){
    test_CW_gain = default_carrier_gain + pk_variance_cwG * sin(tau*phase);
    phase = phase + phase_inc;
    assign_tx_gain(usrp, test_CW_gain);
    std::cout<<"current test CW_gain = "<<test_CW_gain<<"\r";
    refresh();
    boost::this_thread::sleep( boost::posix_time::milliseconds(200) );
    input_char = getch();  
  }
  /* return CW_gain to default value */
  assign_tx_gain(usrp, default_carrier_gain);
  std::cout<<std::endl<<"\n reset CW_gain = "<<default_carrier_gain<<" \r\n\n";
  nodelay(stdscr, FALSE); // return getch() to blocking (default)




  /*************************************************/
  /* Output tone frequency and amplitude tests     */ 
  /*************************************************/
  phase = 0;
  double test_wave_freq, test_wave_amp;
  double pk_variance_f = 200e3;
  double pk_variance_A = 50;
  input_char = 'A';

  /************************/
  /* tone frequency test  */  //pass
  /************************/
  std::cout<<std::endl<<"Test varying frequency: press key to start, 'k' to escape \r\n\n";
  refresh();
  getch();               // pause to begin
  nodelay(stdscr, TRUE); // set getch() non-blocking

  /* vary tone until user terminates with 'k' */
  while(input_char != 'k'){
    test_wave_freq = tx_wave_freq + pk_variance_f * sin(tau*phase);
    phase = phase + phase_inc;
    update_transmission_tone(test_wave_freq, tx_wave_ampl);
    std::cout<<"current test tone = "<<test_wave_freq<<"\r";
    refresh();
    boost::this_thread::sleep( boost::posix_time::milliseconds(200) );
    input_char = getch();  
  }
  /* return tone to default value */
  update_transmission_tone(tx_wave_freq, tx_wave_ampl);
  std::cout<<std::endl<<"\n reset tone = "<<tx_wave_freq<<" \r\n\n";
  nodelay(stdscr, FALSE); // return getch() to blocking (default)

  /************************/
  /* tone amplitude test  */ // pass
  /************************/
  std::cout<<std::endl<<"Test varying amplitude: press key to start, 'k' to escape \r\n\n";
  refresh();
  getch();               // pause to begin
  nodelay(stdscr, TRUE); // set getch() non-blocking
  input_char = 'A';

  /* vary amplitude until user terminates with 'k' */
  while(input_char != 'k'){
    test_wave_amp = tx_wave_ampl + pk_variance_A * sin(tau*phase);
    phase = phase + phase_inc;
    update_transmission_tone(tx_wave_freq, test_wave_amp);
    std::cout<<"current test amplitude = "<<test_wave_amp<<"\r";
    refresh();
    boost::this_thread::sleep( boost::posix_time::milliseconds(200) );
    input_char = getch();  
  }
  /* return tone to default value */
  update_transmission_tone(tx_wave_freq, tx_wave_ampl);
  nodelay(stdscr, FALSE); // return getch() to blocking (default)
  
  /****************************************************/
  /* cycle through some IQ amplitude and phase values */
  /****************************************************/

  /*****************/
  /* Amplitude     */  // pass
  /*****************/
  phase = 0;
  double ampl_start = -0.3;
  double ampl_stop = 0.3;
  double ampl_range = ampl_stop - ampl_start;
  double ampl_num_trials = 100; // divisions
  double ampl_step = ampl_range/ampl_num_trials;

  input_char = 'A';
  nodelay(stdscr, TRUE); // set getch() non-blocking

  std::cout<<std::endl<<"\nBrute Force Amplitude \r\n\n";
  refresh();
  for(double ampl_test = ampl_start; ampl_test <= ampl_stop; ampl_test = ampl_test + ampl_step){
    set_tx_iq_balance_ratio(usrp, (ampl_test, 0));
    std::cout<< "IQ balance ampl: "<< ampl_test <<"\r\n";
    refresh();
    boost::this_thread::sleep( boost::posix_time::milliseconds(200) );
    input_char = getch(); 
    if(input_char == 'k'){break;}
  }
  /****************/
  /* Phase        */  // pass
  /****************/
  double phase_start = -0.3;
  double phase_stop = 0.3;
  double phase_range = phase_stop - phase_start;
  double phase_num_trials = 100; // divisions
  double phase_step = phase_range/phase_num_trials;

  input_char = 'A';

  std::cout<<std::endl<<"Brute Force Phase \r\n\n";
  refresh();
  for(double phase_test = phase_start; phase_test <= phase_stop; phase_test = phase_test + phase_step){
    set_tx_iq_balance_ratio(usrp, (0, phase_test));
    std::cout<< "IQ balance phase: "<< phase_test <<"\r\n";
    refresh();
    boost::this_thread::sleep( boost::posix_time::milliseconds(200) );
    input_char = getch(); 
    if(input_char == 'k'){break;}
  }

  nodelay(stdscr, FALSE); // return getch() to blocking (default)

  /**********************************/
  /* Proagram end and clean up      */
  /************************|*********/
  std::cout<< "\n Press key to termnate (destroys display data)";
  refresh();
  getch();  // pause to terminate
  /* End curses mode */
  echo();
  endwin();   	

  kill_threads();
  return 0;
}
