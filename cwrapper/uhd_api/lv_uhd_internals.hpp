/*
 * lv_uhd_internals.hpp
 * 
 * Private function protypes for lv_uhd_interface.cpp
 * 
 * Alexander Buckley 2014
 * NUIM MARS - Maynooth Adaptive Radio Systems
 * 
 */

#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/config.hpp>
#include <uhd/device.hpp>
#include <uhd/types/dict.hpp>
#include <uhd/property_tree.hpp>
#include <uhd/utils/thread_priority.hpp>
#include <uhd/utils/algorithm.hpp>

#include <uhd/transport/udp_simple.hpp>

#include <boost/thread.hpp>
#include <boost/math/special_functions/round.hpp>
#include <boost/math/constants/constants.hpp>

#include <iomanip>
#include <stdio.h>
#include <iostream>
#include <complex>
#include <vector>

//#include <stdlib.h>

//#include <pthread.h>


using namespace uhd;
using namespace usrp;
using namespace std;

typedef std::complex<float> samp_type;
struct result_t{double freq, real_corr, imag_corr, best, delta;};
/***********************************************************************
 * Usrp device creator
 **********************************************************************/
class Usrp
{
  public:
    Usrp(string device_addr_input){
      usrp_device_addr = device_addr_input;
      usrp_device = uhd::usrp::multi_usrp::make(string(usrp_device_addr));
    }
    uhd::usrp::multi_usrp::sptr usrp_device;
    string usrp_device_addr;
};

Usrp * h_usrp;

boost::thread_group tx_thread;
boost::thread_group rx_thread;

pthread_mutex_t tx_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t rx_lock = PTHREAD_MUTEX_INITIALIZER;

double tx_wave_f = 507.123e3;
double tx_wave_ampl = 25;
bool change_tone = false;

//double * cal_data = new double[3];
double tone_dbrms, imag_dbrms, SB_suppression, DC_power;

std::vector<double> cal_data(3);

void transmission_thread(Usrp *);
void receiver_thread(Usrp *);

static const size_t default_num_samps = 10000;
const double PI  =3.141592653589793238463;
const double pi = boost::math::constants::pi<double>();
/***********************************************************************
 * Sinusoid wave table
 **********************************************************************/
static const size_t wave_table_len = 8192;
static const double tau = 6.28318531;
class wave_table{
  public:
  wave_table(const double ampl){
      table.resize(wave_table_len);
        for (size_t i = 0; i < wave_table_len; i++){
          /* lower SSB */
	  table[i] = std::complex<float>(ampl*sin((tau*i)/wave_table_len), ampl*cos((tau*i)/wave_table_len));
          /* upper SSB */
          //table[i] = std::complex<float>(ampl*cos((tau*i)/wave_table_len), ampl*sin((tau*i)/wave_table_len));
          /* DSB */
          //table[i] = std::complex<float>(ampl*cos((tau*i)/wave_table_len), ampl*sin((tau*i)/wave_table_len));
        }
    }

    inline std::complex<float> operator()(const size_t index) const{
      return table[index % wave_table_len];
    }

  private:
    std::vector<std::complex<float> > table;
};

/***********************************************************************
 * Compute power of a tone
 **********************************************************************/
static inline double compute_tone_dbrms(
    const std::vector<samp_type > &samples,
    const double freq //freq is fractional
){
    //shift the samples so the tone at freq is down at DC
    //and average the samples to measure the DC component
    samp_type average = 0;
    for (size_t i = 0; i < samples.size(); i++){
        average += samp_type(std::polar(1.0, -freq*tau*i)) * samples[i];
    }

    return 20*std::log10(std::abs(average/float(samples.size())));
}

/***********************************************************************
 * Data capture routine
 **********************************************************************/
static void capture_samples(
    uhd::usrp::multi_usrp::sptr usrp,
    uhd::rx_streamer::sptr rx_stream,
    std::vector<samp_type > &buff,
    const size_t nsamps_requested
){
    buff.resize(nsamps_requested);
    uhd::rx_metadata_t md;

    uhd::stream_cmd_t stream_cmd(uhd::stream_cmd_t::STREAM_MODE_NUM_SAMPS_AND_DONE);
    stream_cmd.num_samps = buff.size();
    stream_cmd.stream_now = true;
    usrp->issue_stream_cmd(stream_cmd);
    const size_t num_rx_samps = rx_stream->recv(&buff.front(), buff.size(), md);
    /*
    //validate the received data
    if (md.error_code != uhd::rx_metadata_t::ERROR_CODE_NONE){
        throw std::runtime_error(str(boost::format(
            "Unexpected error code 0x%x"
        ) % md.error_code));
    }
    */
    //we can live if all the data didnt come in
    if (num_rx_samps > buff.size()/2){
        buff.resize(num_rx_samps);
        return;
    }
    if (num_rx_samps != buff.size()){
        throw std::runtime_error("did not get all the samples requested");
    }
}



