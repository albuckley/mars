



#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/config.hpp>
#include <uhd/device.hpp>
#include <uhd/types/dict.hpp>

#include <stdio.h>
#include <iostream>

using namespace uhd;
using namespace std;

/* test stuff */
class Test
{
  public:
    Test(char * tsin){
      test_string = tsin;
    }
    string test_string;
    unsigned int bob = 77;
};
Test * h_test;

extern "C" unsigned int add_one(unsigned int x);

extern "C" unsigned int test_star(char * sin);

extern "C" void * create_obj_test(char * test_string_pointer);

extern "C" void * pass_ptr_test (void);

extern "C" unsigned int pass_ptr_test_2 (void*dummy);

/* real stuff */
class Usrp
{
  public:
    string address_data;
    Usrp(char * device_addr){
      address_data = device_addr;
      //usrp_device = uhd::usrp::multi_usrp::make(string(device_addr));
    }
    //uhd::usrp::multi_usrp::sptr usrp_device;
};

Usrp * h_usrp;



