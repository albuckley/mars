

#include <uhd/utils/safe_main.hpp>
#include <uhd/utils/thread_priority.hpp>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/thread/thread.hpp>


#include "lv_string_test.h"

#include <uhd/usrp/multi_usrp.hpp>

#include "stdio.h"
#include <iostream>
#include <iomanip>

int UHD_SAFE_MAIN( int argc, char* argv[] ){

  string strMatch ("addr=192.168.10.2");

  std::cout << endl;

  //void * foo = create_usrp(argv[1]);

  void * foo1 = create_obj_test(argv[1]);
  Test * foo2 = reinterpret_cast<Test *>(foo1);
  cout<<"The entered string was: "<<foo2->test_string<<endl;

  if (strMatch.compare(argv[1]) != 0)
    std::cout << "Input " << argv[1] << " does NOT match " << strMatch << '\n';
  else
    std::cout << "Input " << argv[1] << " does match " << strMatch << '\n';



  std::cout << endl;

return EXIT_SUCCESS;
}
