



#include "lv_string_test.h"
#include <iomanip>


/* test stuff */
void * create_obj_test(char * test_string_pointer){
  h_test = new Test(test_string_pointer);
  return reinterpret_cast<void *>(h_test);
};
void * pass_ptr_test (void){
  return reinterpret_cast<void *>(h_test);
}

unsigned int pass_ptr_test_2 (void*dummy){
  Test * duplicate = reinterpret_cast<Test *>(dummy);
  return duplicate->bob;
}

unsigned int add_one(unsigned int x){
  unsigned int y = 1 + x;
  return y;
}


unsigned int test_star(char * sin){// ok
  int x =99;
  string strMatch ("bingo");
  if (strMatch.compare(sin) != 0)
    x = 0;
  else
    x = 1;
  return x;
};

