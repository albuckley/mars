



#include "lv_uhd_test.h"

#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/config.hpp>
#include <uhd/device.hpp>
#include <uhd/types/dict.hpp>

#include <stdio.h>
#include <iostream>
#include <iomanip>

using namespace uhd;
using namespace std;

class Usrp
{
  public:

    Usrp(char * device_addr){
      address_data = device_addr;
      usrp_device = uhd::usrp::multi_usrp::make(string(device_addr));
    }
    string address_data;
    unsigned int bob = 88;
    uhd::usrp::multi_usrp::sptr usrp_device;
};

Usrp * h_usrp;


void * create_obj_test(char * p_input){
  h_usrp = new Usrp(p_input);
  return reinterpret_cast<void *>(h_usrp);
};


unsigned int pass_ptr_test_2 (void* p_input){
  Usrp * duplicate = reinterpret_cast<Usrp *>(p_input);
  return duplicate->bob;
}


