
#include <boost/math/special_functions/round.hpp>

#include <iomanip>
#include <stdio.h>

#include <complex>
#include <vector>

#include <fstream>
#include <iostream>

using namespace std;

//fstream a_file;

//a_file.open("wave_tabel_data.txt", ios::out);

std::ofstream a_file("wave_data.txt");

double tx_wave_ampl = 25;
double tau = 6.28318531;

double tx_wave_f = 1000e3;
double tx_rate = 12500000;
size_t wave_table_len = 8192;

size_t step = boost::math::iround(wave_table_len * tx_wave_f/tx_rate);

std::vector<std::complex<float> > buff(363*10);  // from: tx_stream->get_max_num_samps()

class wave_table{
  public:
  wave_table(const double ampl){
      table.resize(wave_table_len);
        for (size_t i = 0; i < wave_table_len; i++){
	  	  table[i] = std::complex<float>(ampl*sin((tau*i)/wave_table_len), ampl*cos((tau*i)/wave_table_len));
        }
  }

  inline std::complex<float> operator()(const size_t index) const{
    return table[index % wave_table_len];
  }

  private:
    std::vector<std::complex<float> > table;
};

wave_table table(tx_wave_ampl);



int main(void){
  cout<<"starting program"<<endl;

  double ii = 0;
  size_t index = 0;
  //while (ii < tx_rate*2){
  	cout<<"running loop"<<endl;
    for (size_t k = 0; k < buff.size(); k++){
      buff[k] = table(index += step);
    }
    cout<<"buff size "<<buff.size()<<endl;
    for (size_t j = 0; j < buff.size(); j++){
      a_file<<buff[j]<<"\n";
    }
    

    ++ ii;

  //}// end while

  a_file.close();

  return 0;
}