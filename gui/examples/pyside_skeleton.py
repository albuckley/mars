#!/usr/bin/python
# -*- coding: utf-8 -*-

# simple.py

import sys
from PySide import QtGui, QtCore

# WIDGET
class Example1(QtGui.QWidget):
    
    def __init__(self):
        super(Example1, self).__init__()
        
        self.init()
        
    def init(self):
              

        #hbox.addStretch(1)          # stops hbox widgets from filling to width

        

        #################################################
        # Frames  -- using QSplitter
        #################################################
        # make sub frames
        frm_topfull         = QtGui.QFrame(self)
        frm_topleft         = QtGui.QFrame(self)
        frm_left            = QtGui.QFrame(self)
        frm_bottomleft      = QtGui.QFrame(self)
        frm_topright        = QtGui.QFrame(self)
        frm_right           = QtGui.QFrame(self)
        frm_bottomright     = QtGui.QFrame(self)
        frm_bottomfull      = QtGui.QFrame(self)

        # sub frame internal frames
        #frm_topfulla         = QtGui.QFrame(self)
        #frm_topfullb         = QtGui.QFrame(self)
        #frm_topfullc         = QtGui.QFrame(self)
        #a_splitter_test = QtGui.QSplitter(QtCore.Qt.Horizontal)
        #a_splitter_test.addWidget(frm_topfulla)
        #a_splitter_test.addWidget(frm_topfullb)
        #a_splitter_test.addWidget(frm_topfullc)

        # sub frame style
        frm_topfull.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_topleft.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_left.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_bottomleft.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_topright.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_right.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_bottomright.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_bottomfull.setFrameShape(QtGui.QFrame.StyledPanel)

        # place sub frames into splitters (vert)
        splitter_hrz1 = QtGui.QSplitter(QtCore.Qt.Horizontal)
        splitter_hrz1.addWidget(frm_topleft) # imsert splitter
        #splitter_hrz1.addWidget(a_splitter_test)
        splitter_hrz1.addWidget(frm_topright)

        splitter_hrz2 = QtGui.QSplitter(QtCore.Qt.Horizontal)
        splitter_hrz2.addWidget(frm_left)
        splitter_hrz2.addWidget(frm_right)

        splitter_hrz3 = QtGui.QSplitter(QtCore.Qt.Horizontal)
        splitter_hrz3.addWidget(frm_bottomleft)
        splitter_hrz3.addWidget(frm_bottomright)

        # vertical splitter contains horizontal ones
        splitter_vert = QtGui.QSplitter(QtCore.Qt.Vertical)
        splitter_vert.addWidget(frm_topfull)
        splitter_vert.addWidget(splitter_hrz1)
        splitter_vert.addWidget(splitter_hrz2)
        splitter_vert.addWidget(splitter_hrz3)
        splitter_vert.addWidget(frm_bottomfull)
        #splitter_vert.addWidget(a_splitter_test)

        # Box layout to contains it all
        box_master = QtGui.QHBoxLayout(self)
        box_master.addWidget(splitter_vert)
        self.setLayout(box_master)
        #QtGui.QApplication.setStyle(QtGui.QStyleFactory.create('Cleanlooks'))

        #################################################
        # 0bjects
        #################################################
        
        # Signal select stuff

        # Carrier stuff
        label_cw    = QtGui.QLabel('Carrier')
        lcd_cw_amp  = QtGui.QLCDNumber(self)
        lcd_cw_frq  = QtGui.QLCDNumber(self)
        sld_cw_amp  = QtGui.QSlider(QtCore.Qt.Horizontal)
        sld_cw_frq  = QtGui.QSlider(QtCore.Qt.Horizontal)

        # Tone stuff
        label_tone   = QtGui.QLabel('Tone')
        #label_tone.alignment() # no effect
        lcd_tone_amp = QtGui.QLCDNumber(self)
        lcd_tone_frq = QtGui.QLCDNumber(self)
        sld_tone_amp = QtGui.QSlider(QtCore.Qt.Horizontal)
        sld_tone_frq = QtGui.QSlider(QtCore.Qt.Horizontal)
        

        # IQ Balance stuff

        #okButton     = QtGui.QPushButton("OK", bottomfull)
        #cancelButton = QtGui.QPushButton("Cancel", bottomfull)

        # test stuff
        aButton1     = QtGui.QPushButton("OK 1")
        aButton2     = QtGui.QPushButton("OK 2")
        aButton3     = QtGui.QPushButton("OK 3")

        #################################################
        # put objects in correct frame using box layout class 
        #################################################
        box_carrier = QtGui.QVBoxLayout(frm_left) 
        box_carrier.addWidget(label_cw)
        box_carrier.addWidget(lcd_cw_amp)
        box_carrier.addWidget(sld_cw_amp)
        box_carrier.addWidget(lcd_cw_frq)
        box_carrier.addWidget(sld_cw_frq)

        a_box_test_hrz = QtGui.QHBoxLayout(frm_right) 
        a_box_test_hrz.addWidget(aButton1)
        a_box_test_hrz.addWidget(aButton2)
        a_box_test_hrz.addWidget(aButton3)

        box_tone = QtGui.QVBoxLayout(frm_right) 
        #box_tone.addWidget(label_tone)
        #box_tone.addWidget(a_box_test_hrz)
        box_tone.addWidget(lcd_tone_amp)
        box_tone.addWidget(sld_tone_amp)
        box_tone.addWidget(lcd_tone_frq)
        box_tone.addWidget(sld_tone_frq)
        
        
        # frame
        #a_Frame         = QtGui.QFrame(self)
        #a_Frame.setFrameShape(QtGui.QFrame.StyledPanel)
        # splitter
        #a_splitter_hrz  = QtGui.QSplitter(QtCore.Qt.Horizontal, frm_bottomleft)
        #a_splitter_hrz .addWidget(a_Frame)
        #a_splitter_hrz .addWidget(aButton1)
        #a_splitter_hrz .addWidget(aButton2)
        #a_splitter_hrz .addWidget(aButton3)
        # box
        #a_box           = QtGui.QHBoxLayout(self)
        #a_box.addWidget(a_splitter_hrz)
        # objects placed in frame
        
        
        #a_splitter_test = QtGui.QSplitter(QtCore.Qt.Horizontal, frm_topleft)
        #a_splitter_test.addWidget(aButton1)
        #a_splitter_test.addWidget(aButton2)
        #a_splitter_test.addWidget(aButton3)
        #################################################
        # Connections
        #################################################
        sld_cw_amp.valueChanged.connect(lcd_cw_amp.display)
        sld_cw_frq.valueChanged.connect(lcd_cw_frq.display)
        sld_tone_amp.valueChanged.connect(lcd_tone_amp.display)
        sld_tone_frq.valueChanged.connect(lcd_tone_frq.display)

        
        # An instance of the Communicate class is created. 
        # We connect a close() slot of the QtGui.QMainWindow to the closeApp signal.
        #  not exactly the best exa,ple as it closes the window on click
        # self.c = Communicate()
        # self.c.closeApp.connect(self.close)

        # window frame
        self.setGeometry(300, 300, 500, 500)            # location and size in pixels
        self.setWindowTitle('Signal & slot - slider')
        self.show()

    def keyPressEvent(self, e):
        # press escape and widget will close
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()

    def mousePressEvent(self, event):

        self.c.closeApp.emit()

# Objects created from QtCore.QObject can emit SIGNALS. 
# If we click on the button, a clicked signal is generated.
# We create a new signal called closeApp. This signal is emitted, during a mouse press event. 
# The signal is connected to the close() slot of the QtGui.QMainWindow.
# We create a class based on a QtCore.QObject. It creates a closeApp signal when instantiated.
class Communicate(QtCore.QObject):
    
    closeApp = QtCore.Signal()

class applicationWindow(QtGui.QMainWindow):   		   # applicationWindow class inherits from QtGui.QWidget class
    
    def __init__(self):						           # applicationWindow class  constructor
        super(applicationWindow, self).__init__()	   # inherited class constructor
        # call initialisation routine
        self.initUI()						

    # initialisation method definition
    def initUI(self):						

    	# creates a text area
    	#textEdit = QtGui.QTextEdit()
    	# makes textEdit widget the central widget accupying all space (all of the back ground window area)
    	#self.setCentralWidget(textEdit)

    	

    	# set up the main window properties
    	# self.setGeometry(300, 300, 250, 150)					# location and size in pixels
        self.setWindowTitle('Icon')								# title bar text
        self.resize(300, 300)									# window size
        self.center()											# our centering on screen function
        self.setWindowIcon(QtGui.QIcon('~/mars/gui/signal.png')) # fail
    
    	# The statusbar is a widget that is used for displaying status information.
    	self.statusBar().showMessage('Ready')  

        # an action example (open file)
        openFile = QtGui.QAction(QtGui.QIcon('open.png'), 'Open', self)
        openFile.setShortcut('Ctrl+O')
        openFile.setStatusTip('Open new File')
        openFile.triggered.connect(self.showFileOpenDialog)

    	# an action example (exit)
    	exitAction = QtGui.QAction(QtGui.QIcon('exit24.png'), 'Exit', self) # ICON FAIL
        exitAction.setShortcut('Ctrl+Q')						# asigned hotkey
        exitAction.setStatusTip('Exit application')				# updates status bar with roll over help text
        exitAction.triggered.connect(self.close)				# triggered signal is connected to the close method

        # create menu bar - populated here with our exit action
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')						# make file meneu in menu bar
        fileMenu.addAction(exitAction)							# add our exit action example to file menu
        fileMenu.addAction(openFile) 

        # create a toolbar
        toolbar = self.addToolBar('Exit')					# instantiate toolbar
        toolbar.addAction(exitAction)						# populate toolbar with exit action (icon failed)

        # set up font for tooltips
    	QtGui.QToolTip.setFont(QtGui.QFont('SansSerif', 10))	

        # roll over for main window area
        self.setToolTip('This is a <b>QWidget</b> widget')	


        topBarsOffsetY = 40

        # text edit object
        self.textEdit = QtGui.QTextEdit()
        # make back ground text editor -> change this to it#s pwn widget
        # FOr now this area is used to display data open from by file broswer dialog example
        self.setCentralWidget(self.textEdit)

        # a label - would make sense to add meneu and tolbar offsets
        label1 = QtGui.QLabel('I am a Label', self)
        label1.move(10, 10 +topBarsOffsetY)	


        # do nothing button example
        btn1 = QtGui.QPushButton('Button 1', self)					# create button object
        btn1.setToolTip('This is a <b>QPushButton</b> widget')	    # rollover text for button
        btn1.resize(btn1.sizeHint())								# gives recomended size for button
        btn1.move(10, 50 +topBarsOffsetY) 							# button position
        btn1.clicked.connect(self.buttonClicked) 

        # quit button
        qbtn = QtGui.QPushButton('Quit', self)					# paramters: label and parent widget. The parent widget is the Example widget, which is a QtGui.QWidget by inheritance.
        qbtn.resize(qbtn.sizeHint())							# gives recomended size for button
        qbtn.move(10, 100 +topBarsOffsetY)						# button position (x, y)
        # action to take on press
        qbtn.clicked.connect(QtCore.QCoreApplication.instance().quit)	# The event processing system in PySide is built with the signal & slot mechanism. 
        																# If we click on the button, the signal clicked is emitted.
        																# The slot can be a Qt slot or any Python callable.
    																	# The QtCore.QCoreApplication contains the main event loop.
    																	# It processes and dispatches all events. 
    																	# The instance() method gives us the current instance.
    																	# QtCore.QCoreApplication is created with the QtGui.QApplication.
    																	# The clicked signal is connected to the quit() method, which terminates the application. 
    																	# The communication is done between two objects. The sender and the receiver.
    																	# The sender is the push button, the receiver is the application object.
        


        self.show()

    def center(self):						# centers window on screen (main window if you have 2)
        
        qr = self.frameGeometry()									# We get a rectangle specifying the geometry of the main window. This includes any window frame.
        cp = QtGui.QDesktopWidget().availableGeometry().center()	# We figure out the screen resolution of our monitor. And from this resolution, we get the center point.
        qr.moveCenter(cp)											# Our rectangle has already its width and height. Now we set the center of the rectangle to the center of the screen. 
        self.move(qr.topLeft())										# We move the top-left point of the application window to the top-left point of the qr rectangle, thus centering the window on our screen.
    

    def closeEvent(self, event):		# acts on window frame x button (probably should tie it to quit button as well)
        
        reply = QtGui.QMessageBox.question(self, 'Message',
            "Are you sure to quit?", QtGui.QMessageBox.Yes | 
            QtGui.QMessageBox.No, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()    

    def buttonClicked(self):
        # multiple event s can call same handler, sender() method determines which one
        sender = self.sender()
        self.statusBar().showMessage(sender.text() + ' was pressed')

    def mousePressEvent(self, event):
        
        self.c.closeApp.emit()

    def showFileOpenDialog(self):

        fname, _ = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '/home')
        
        f = open(fname, 'r')
        
        with f:
            data = f.read()
            self.textEdit.setText(data)

def main():
    
    app = QtGui.QApplication(sys.argv)
    appWin = applicationWindow()
    ex = Example1()
    sys.exit(app.exec_())  		# exit clean with exit status


if __name__ == '__main__':
    main()