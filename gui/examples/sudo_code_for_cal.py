

import csv

def calibration_routine(self):

	best_supression = 0
	chosen_wave_f 	= 0
	IQiterations_A 	= 4
	IQiterations_B 	= 2
	DCiterations    = 4

	# convergent algorythm step size
	step_I 		= 0.001
	step_Q 		= step_I
	step_dc_I 	= step_I
	step_dc_Q 	= step_I

	# CW cal frequencies
	CW_start 	= 400 # MHz
	CW_stop 	=  4000 # MHz
	CW_step 	=  100  # 100 MHz

	# modulation frequency range
	f_mod_start = 400
	f_mod_stop	= 700
	f_mod_step	= 25

	# cal values to start with
	self.initial_tx_I_gain_correction 	= - 0.4
	self.initial_tx_iq_Q_correction 	= - 0.4

	# initialise measuments vector
	Supres 			= [0.0]
	# force entry to while loops for each IQiterations
	delta_Supres 	= 100
	# initialise best result with a bad result
	best_supression = 0

	# initialise result vector index
	index_all 	 = 0  	# data tracking index
	all_I 		 = [0.0]
	all_Q 		 = [0.0]
	all_supres 	 = [0.0]
	all_deltaS   = [0.0]
	all_index 	 = [0]

	# initialise trial data 
	r 			 = 0   # result index
	result_I[r]  = [0.0]
	result_Q[r]  = [0.0]
	result_S[r]  = [0.0]
	result_fm[r] = [0.0]
	result_cw[r] = [0.0]
	result_i[r]  = [0] 

	def do_calibration(self):

		# open files for recording data
		resultsFile 	= csv.writer(open("results.csv", "wb"))
		evo_dataFile 	= csv.writer(open("evo_data.csv", "wb"))

		######################################
		# calibrate for range of frequencies #
		######################################
		for CW in range(CW_start, CW_stop, CW_step):  # <<<< record

			# user feedback display
			print CW

			# reset trial values
			self.rx_I_gain_correction = initial_tx_I_gain_correction
			self.rx_iq_Q_correction = initial_tx_iq_Q_correction

			# reset best result with a bad result
			best_supression = 0
			find_mod_tone_with_best_intermod(self):
			
			# set hardware to use this modulation for the upcoming IQ calibration
			self.rx_wave_f = chosen_wave_f

			#########################################
			#### Calibrate I and Q Gain #############
			#########################################
			for cal_IQ_1 in range(IQiterations_A)
				adjust_I_gain_for_best_supression(self)
				adjust_Q_gain_for_best_supression(self)
			print "finnished cal IQ gain A"

			#########################################
			#### Calibrate I and Q DC offset ########
			#########################################
			for cal_DC in range(DCiterations):
				adjust_I_offset_for_best_supression(self)
				adjust_Q_offset_for_best_supression(self)
			print "finnished cal IQ DC Offset"

			#########################################
			#### IQ Gain again with new DC offset ###
			#########################################
			for cal_IQ_2 in range(IQiterations_B)
				adjust_I_gain_for_best_supression(self)
				adjust_Q_gain_for_best_supression(self)
			print "finnished cal IQ gain B"

			# record results of calibration at this CW
			record_results(self)

			# seed next CW calibration with values from previous CW
			self.initial_tx_I_gain_correction = self.rx_I_gain_correction
			self.initial_tx_iq_Q_correction = self.rx_iq_Q_correction

		print "writing data to file"
		# store results to files
		for rr in range(len(result_i)):
			resultsFile.writerow(result_i[rr], result_cw[rr], result_I[rr], result_Q[rr], result_S[rr], result_fm[rr])
		# store evolution data
		for dd in range(len(all_index)):
			evo_dataFile.writerow(all_index[dd], all_I[dd], all_Q[dd], all_supres[dd], all_deltaS[dd])

		# close files
		resultsFile.close()
	 	evo_dataFile.close()
		##########################
		#### end algorythm  ######
		##########################
		print " ===== Completed Successfully ====="
	
	# find modulation frequency with lowest intermodulation products located at image frequency
	def find_mod_tone_with_best_intermod(self):
		for f_mod in range(f_mod_start, f_mod_stop, f_mod_step): 
			# set hardware
			tx_wave_f = f_mod
			# measure image rms power relative to signal (LSB SSB)
			supression = get_supression_AVG()
			# values aught to be negative
			if(best_supression < supression)
				best_supression = supression
				chosen_wave_f = f_mod # <<<< record

	###################################################
	###### minimise image Supres with I gain cal ######
	###################################################
	def adjust_I_gain_for_best_supression(self):
		# force entry into loop
		delta_Supres = 100
		# reset measurements vector
		n = 0;
		# while adjustments produce significant change
		while(abs(delta_Supres) > targetSupReso):
			# incement calibration value
			self.rx_I_gain_correction = self.rx_I_gain_correction + step_I # <<<< record
			self.tx_I_correction_sliderChanged_handler(self, self.rx_I_gain_correction)
			# load AVG array of result data
			Supres[n+1] = get_supression_AVG() # <<<< record
			# calculate change in result
			delta_Supres = Supres[n+1] - Supres[n] # <<<< record
			record_evolution_values(self)
			# values are worse, go the other way next time
			if(delta_Supres > 0):
				step_I = step_I * (-1)
			# shift result data vector
			Supres[n+1] = Supres[n] # <<<< record
			# incement result data vector index
			++n
		# ensure that next time we tune I, we start by going in the oposite direction
		step_I = step_I * (-1)

	###################################################
	###### minimise image Supres with Q gain cal ######
	###################################################
	def adjust_Q_gain_for_best_supression(self):
		# force entry into loop
		delta_Supres = 100
		# reset measurements vector
		n = 0;
		# while adjustments produce significant change
		while(abs(delta_Supres) > targetSupReso):
			# incement calibration value
			self.rx_iq_Q_correction = self.rx_iq_Q_correction + step_Q # <<<< record
			self.rx_Q_correction_sliderChanged_handler(self, self.rx_Q_gain_correction)
			#load AVG array of result data
			Supres[n+1] = get_supression_AVG() # <<<< record
			# calculate change in result
			delta_Supres = Supres[n+1] - Supres[n] # <<<< record
			# values are worse, go the other way
			if(delta_Supres > 0):
				step_Q = step_Q * (-1)
			# shift result data vector
			Supres[n+1] = Supres[n]
			# incement result data vector index
			++n
		# ensure that next time we tune Q, we start by going in the oposite direction
		step_Q = step_Q * (-1)

	
	##########################################################
	###### minimise Carrier power with DC I offset cal ######
	##########################################################
	def adjust_I_offset_for_best_supression(self):
		# force entry into loop
		delta_Supres = 100
		# reset measurements vector
		n = 0;
		# while adjustments produce significant change
		while(abs(delta_Supres) > targetSupReso):
			# incement calibration value
			self.rx_dc_I_offset = self.rx_dc_I_offset + step_dc_I # <<<< record
			self.rx_dc_I_offset_sliderChanged_handler(self, self.rx_dc_I_offset)
			#load AVG array of result data
			Supres[n+1] = get_supression_AVG() # <<<< record
			# calculate change in result
			delta_Supres = Supres[n+1] - Supres[n] # <<<< record
			# values are worse, go the other way
			if(delta_Supres > 0):
				step_dc_QI = step_dc_I * (-1)
			# shift result data vector
			Supres[n+1] = Supres[n]
			# incement result data vector index
			++n
		# ensure that next time we tune Q, we start by going in the oposite direction
		step_dc_I = step_dc_I * (-1)

	##########################################################
	###### minimise Carrier power with DC Q offset cal ######
	##########################################################
	def adjust_Q_offset_for_best_supression(self):
		# force entry into loop
		delta_Supres = 100
		# reset measurements vector
		n = 0;
		# while adjustments produce significant change
		while(abs(delta_Supres) > targetSupReso):
			# incement calibration value
			self.rx_dc_Q_offset = self.rx_dc_Q_offset + step_dc_Q # <<<< record
			self.rx_dc_Q_offset_sliderChanged_handler(self, self.rx_dc_Q_offset)
			#load AVG array of result data
			Supres[n+1] = get_supression_AVG() # <<<< record
			# calculate change in result
			delta_Supres = Supres[n+1] - Supres[n] # <<<< record
			# values are worse, go the other way
			if(delta_Supres > 0):
				step_dc_Q = step_dc_Q * (-1)
			# shift result data vector
			Supres[n+1] = Supres[n]
			# incement result data vector index
			++n
		# ensure that next time we tune Q, we start by going in the oposite direction
		step_dc_Q = step_dc_Q * (-1)


    #######################################################################
	#### record all evolution values -> all_I, all_Q, all_S, all_deltaS ###
	#######################################################################
	def record_evolution_values(self):
		all_I[index_all] 		= self.rx_I_gain_correction
		all_Q[index_all] 		= self.rx_iq_Q_correction
		all_supres[index_all] 	= Supres[n+1]
		all_deltaS[index_all] 	= delta_Supres
		all_index[index_all]	= index_all
		++index_all

	#######################################################################
	#### record all results per CW - > CW, I_cal, Q_cal, Supress ##########
	#######################################################################
	def record_results(self):
		result_I[r]  = self.rx_I_gain_correction
		result_Q[r]  = self.rx_iq_Q_correction
		result_S[r]  = Supres[n-1]
		result_fm[r] = chosen_wave_f
		result_cw[r] = CW
		result_i[r]  = r
		++r









