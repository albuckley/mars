# Particle Swarm Optimisation ver. 1
# Maynooth Adaptive Radio Systems
# Alexander Buckley Feb 2014
#
# PSO1_3
#
# This first version uses two independant and uncoupled swarms
# Each swarm evaluates their own cost separately, 
# carrier power (for DC offset) and image power (gain correction)
# When one swarm finds a better solution, the other swarm then will use that for all it's own evaluations.
#
# This leads to a senario where one swarm is unablwe to find any better solution relative to a past best, given the other swarm 
# has moved it's soolution in a non favorable direction.
#
# This can create a 10 - 15 db discrepancy between what is currently possible given the other swarms current solution
# vrs what was achievable given one of the other swarms past but now discarded solutions.
#
# plots to PSO viewer for visual feed back
#
# Initial conditions: particles initialised around the perimiter of the constraints box or zero, or random




import random
import numpy
import math
import csv
import time
import sys

import liblv_uhd_pub_api
import cal_LUT
from dataObject import DataObject

from PySide import QtGui, QtCore, QtOpenGL
import pyqtgraph.opengl as gl
import numpy as np

ITERATIONS = 40#25

# control params
omega = 0.3925
phi_p = 2.5586
phi_g = 1.3358

# option to start with particle along perimiter instead of random
DO_PERIMITER_INIT = 0
DO_ZERO_INIT      = 1

# number of particles
S = 8 * 3 #25 #25    # must be divisible by 8 for initialisation perimiter method to work well (4 sides spanning 4 quadrants)

# constraints box (just use +ve numbers, *(-1) applied later when needed)
limit = 0.5
limit_gains_x = limit
limit_gains_y = limit
limit_ofset_x = limit
limit_ofset_y = limit

# upper and lower bounds (random number generator)
bound = 0.5 #0.5
Iofset_lo = -bound
Iofset_up =  bound
Qofset_lo = -bound
Qofset_up =  bound

Igains_lo = -bound
Igains_up =  bound
Qgains_lo = -bound
Qgains_up =  bound

power_AVG_window_length = 10

class AParticle(DataObject):
    __slots__ = ('position_I', 'position_Q', 'best_position_I', 'best_position_Q', 'velocity_I', 'velocity_Q')

class Solution(DataObject):
    __slots__ = ('I_gains','Q_gains','I_ofset','Q_ofset')

swarm_best_count_gain  = 0
swarm_best_count_ofset = 0

#solutions = Solution(0.0, 0.0, 0.0, 0.0)  # best known position of all in swarm, ie 'g'

p_gains = []
p_ofset = []

cw_samples = [500,750,1000,1500,2250,2500,2750,3000,3500,3800]

# CW cal frequencies
CW_start    = 400 # 400 MHz
CW_stop     = 4000 #4000 # 4000 MHz
CW_step     = 20  # 100 MHz

#### Results stuff
# initialise result vector index
index_all    = 0    # data tracking index
all_I        = [0.0]
all_Q        = [0.0]
all_supres   = [0.0]
all_deltaS   = [0.0]
all_index    = [0]

# initialise trial data 
r            = 0   # result index
result_CW    = ['CW']
result_Fmod  = ['Fmod'] 
result_Igain = ['Igain']
result_Qgain = ['Qgain']
result_Idc   = ['Idc']
result_Qdc   = ['Qdc']
result_SBpwr = ['SBpwr']
result_CWpwr = ['CWpwr']
result_Tonepwr = ['Tonepwr']
result_Imagpwr = ['Imagpwr']

# paramter_code values for LUT:
CARRIER = 0
IGAIN   = 1
QGIAN   = 2
IDC     = 3
QDC     = 4

my_gain_best     = 0.0
my_ofset_best    = 0.0
swarm_gain_best  = 0.0
swarm_ofset_best = 0.0

# open files for recording data
results_filename = "results.csv"

# open the manual tx cal data file
with open('./cal_data/tx_manual_MARS.csv', 'rb') as data:
    rows = csv.reader(data)
    next(rows) #Skip the headers.
    cal_dat_tx_manual = [[float(item) for number, item in enumerate(row) if item ] for row in rows]

cw_samples = [500,750,1000,1500,2250,2500,2750,3000,3500,3800]

def init_dislpay_particles(self):
     # new particles
    self.numPars = S
    self.size = np.empty(self.numPars)
    self.color = np.empty([self.numPars, 4])
    self.gains_pos = np.empty([self.numPars, 3], dtype=float)
    self.ofset_pos = np.empty([self.numPars, 3], dtype=float)
    self.sp_gains_1 = gl.GLScatterPlotItem(pos=self.gains_pos, size=self.size, color=self.color, pxMode=False)
    self.sp_ofset_1 = gl.GLScatterPlotItem(pos=self.ofset_pos, size=self.size, color=self.color, pxMode=False)

    # each individuals best
    self.numPars2 = S
    self.size2 = np.empty(self.numPars2)
    self.color2 = np.empty([self.numPars2, 4])
    self.gains_pos2 = np.empty([self.numPars2, 3], dtype=float)
    self.ofset_pos2 = np.empty([self.numPars2, 3], dtype=float)
    self.sp_gains_2 = gl.GLScatterPlotItem(pos=self.gains_pos2, size=self.size2, color=self.color2, pxMode=False)
    self.sp_ofset_2 = gl.GLScatterPlotItem(pos=self.ofset_pos2, size=self.size2, color=self.color2, pxMode=False)

    # swarms best (keeps history)
    dummy_int = int(int(ITERATIONS) * int(S)) # maximum posible aray size needed if every new particle was best
    self.numPars3 = dummy_int
    self.size3 = np.empty(self.numPars3)
    self.color3 = np.empty([self.numPars3, 4])
    self.gains_pos3 = np.empty([self.numPars3, 3], dtype=float)
    self.ofset_pos3 = np.empty([self.numPars3, 3], dtype=float)
    self.sp_gains_3 = gl.GLScatterPlotItem(pos=self.gains_pos3, size=self.size3, color=self.color3, pxMode=False)
    self.sp_ofset_3 = gl.GLScatterPlotItem(pos=self.ofset_pos3, size=self.size3, color=self.color3, pxMode=False)

def init_swarm(self, p_gains, p_ofset, S):
    # declare variables
    global swarm_best_count_gain, swarm_best_count_ofset, my_gain_best, my_ofset_best, swarm_gain_best, swarm_ofset_best

    swarm_best_count_gain  = 0
    swarm_best_count_ofset = 0

    p_gains[:] = []
    p_ofset[:] = []
    solutions = Solution(0.0, 0.0, 0.0, 0.0)  # best known position of all in swarm, ie 'g'
    pos_I_gains = 0
    pos_Q_gains = 0
    pos_I_ofset = 0
    pos_Q_ofset = 0

    ini_PSO_plat_data(self,S)

    print "\n initializing swarm"

    for i in range(S):
        #print "making new particle ", i
        #sys.stdout.write(' p')
        #sys.stdout.write(str(i))
        QtCore.QCoreApplication.processEvents()

        #### init values ####
        # position (two ways, random or perimiter)
        if(DO_PERIMITER_INIT):
            # perimiter method
            init_pos_perim_gains = init_position_perimiter(self, limit_gains_x, limit_gains_y, i)
            pos_I_gains = init_pos_perim_gains[0]
            pos_Q_gains = init_pos_perim_gains[1]

            init_pos_perim_ofset= init_position_perimiter(self, limit_gains_x, limit_gains_y, i)
            pos_I_ofset = init_pos_perim_ofset[0]
            pos_Q_ofset = init_pos_perim_ofset[1]

        elif(DO_ZERO_INIT):
            pos_I_gains = 0.0
            pos_Q_gains = 0.0

            pos_I_ofset = 0.0
            pos_Q_ofset = 0.0

        else:
            # random method
            pos_I_gains = random.uniform(Igains_lo, Igains_up)  
            pos_Q_gains = random.uniform(Qgains_lo, Qgains_up)

            pos_I_ofset = random.uniform(Iofset_lo, Iofset_up)
            pos_Q_ofset = random.uniform(Qofset_lo, Qofset_up)

        # best position
        best_pos_I_gains = pos_I_gains
        best_pos_Q_gains = pos_Q_gains

        best_pos_I_ofset = pos_I_ofset
        best_pos_Q_ofset = pos_Q_ofset

        # velocity
        vel_I_gains = random.uniform(-abs(Igains_up-Igains_lo),abs(Igains_up-Igains_lo))
        vel_Q_gains = random.uniform(-abs(Qgains_up-Qgains_lo),abs(Qgains_up-Qgains_lo))

        vel_I_ofset = random.uniform(-abs(Iofset_up-Iofset_lo),abs(Iofset_up-Iofset_lo))
        vel_Q_ofset = random.uniform(-abs(Qofset_up-Qofset_lo),abs(Qofset_up-Qofset_lo))

        #### create new data particle
        if(DO_PERIMITER_INIT):
            new_p_gains = AParticle(pos_I_gains, pos_Q_gains, best_pos_I_gains, best_pos_Q_gains, 0.01, 0.01)
            new_p_ofset = AParticle(pos_I_ofset, pos_Q_ofset, best_pos_I_ofset, best_pos_Q_ofset, 0.01, 0.01)
        else:
            new_p_gains = AParticle(pos_I_gains, pos_Q_gains, best_pos_I_gains, best_pos_Q_gains, vel_I_gains, vel_Q_gains)
            new_p_ofset = AParticle(pos_I_ofset, pos_Q_ofset, best_pos_I_ofset, best_pos_Q_ofset, vel_I_ofset, vel_Q_ofset)

        #### add particle to swarm
        p_gains.append(new_p_gains) 
        p_ofset.append(new_p_ofset) 

        # first iteration will always be individual best
        p_gains[i].best_position_I = p_gains[i].position_I
        p_gains[i].best_position_Q = p_gains[i].position_Q     
        if(i==0): # first init value will always be best
            solutions.I_gains = p_gains[i].best_position_I
            solutions.Q_gains = p_gains[i].best_position_Q
            my_gain_best = offset_function(self, p_gains[i].best_position_I, p_gains[i].best_position_Q )
            swarm_gain_best = gain_function(self, solutions.I_gains, solutions.Q_gains ) 
        # same for offset
        p_ofset[i].best_position_I = p_ofset[i].position_I
        p_ofset[i].best_position_Q = p_ofset[i].position_Q     
        if(i==0): # first init value will always be best
            solutions.I_ofset = p_ofset[i].best_position_I
            solutions.Q_ofset = p_ofset[i].best_position_Q
            my_ofset_best = offset_function(self, p_ofset[i].best_position_I, p_ofset[i].best_position_Q )
            swarm_ofset_best = offset_function(self, solutions.I_ofset, solutions.Q_ofset ) 

        # calculate power 
        new_gain_pwr = gain_function(self, p_gains[i].position_I, p_gains[i].position_Q)
        

        set_gain_points(self, p_gains[i].position_I * 10, p_gains[i].position_Q * 10, new_gain_pwr, i)
        set_individual_best_gain_points(self, p_gains[i].best_position_I * 10, p_gains[i].best_position_Q * 10, new_gain_pwr, i) # literaly different on this use

        #### check which one is best in swarm
        if( new_gain_pwr < swarm_gain_best):
            #print new_gain_pwr, " is less than swarm ", swarm_gain_best
            solutions.I_gains = p_gains[i].best_position_I
            solutions.Q_gains = p_gains[i].best_position_Q
            swarm_gain_best = new_gain_pwr
            swarm_best_count_gain = swarm_best_count_gain + 1
            set_swarm_best_gain_points(self, solutions.I_gains * 10, solutions.Q_gains * 10, swarm_gain_best, swarm_best_count_gain)
            #print "gain swarm best count: ", swarm_best_count_gain

        # calculate power 
        new_ofset_pwr = offset_function(self, p_ofset[i].position_I, p_ofset[i].position_Q ) 
        

        set_ofset_points(self, p_ofset[i].position_I * 10, p_ofset[i].position_Q * 10, new_ofset_pwr, i)
        set_individual_best_ofset_points(self, p_ofset[i].best_position_I * 10, p_ofset[i].best_position_Q * 10, new_ofset_pwr, i) # literaly different on this use

        #### check which one is best in swarm
        if(new_ofset_pwr < swarm_ofset_best):
            solutions.I_ofset = p_ofset[i].best_position_I
            solutions.Q_ofset = p_ofset[i].best_position_Q
            swarm_ofset_best = new_ofset_pwr
            swarm_best_count_ofset = swarm_best_count_ofset + 1
            set_swarm_best_ofset_points(self, solutions.I_ofset * 10, solutions.Q_ofset * 10, swarm_ofset_best, swarm_best_count_ofset)
            #swarm_ofset_best = new_ofset_pwr

        
        QtCore.QCoreApplication.processEvents()
        #for j in range(10000):
            #sys.stdout.write('.')
            #while(1):
                #sys.stdout.write('.')
                #QtCore.QCoreApplication.processEvents()


    print " completed init swarm"

    return solutions

def init_position_perimiter(self, limx, limy, index): 
    # the goal here is to write out particle on perimiter
    # it is a bit screwed up as it does not start in one corner and work it's way around which was the intent.
    # ANyway, it is pretty close so not on the top of my todo list to fix (it works)
    init_pos = (0,0)
    #print "limx: ", limx
    #print "limy: ", limy
    #print "index: ", index
    #print "mod index top: ", index % 8
    #print "mod index right: ", index % (8*2)
    #print "mod index botto: ", index % (8*3)
    #print "mod index left: ", index % (8*4)
    if(index <= (index % (S/4))):   # top side
        #print "top side"
        init_pos = (-limx + ((index % 8)*8*limx/S), limy)
    elif(index <= (index % ((S/4)*2))):  # right side
        #print "right side"
        init_pos = (limx, limy - ((index % 8)*8*limy/S))
    elif(index <= (index % ((S/4)*3))):  # bottom side
        #print "bottom side"
        init_pos = (limx - ((index % 8)*8*limx/S), -limy)
    elif(index <= (index % ((S/4)*4))):  # left side
        #print "left side"
        init_pos = (-limx, -limy + ((index % 8)*8*limy/S))
    return init_pos

def find_minimum(self, p_gains,  p_ofset, S, solutions):
    global omega, phi_p, phi_g, swarm_best_count_gain, swarm_best_count_ofset, my_gain_best, my_ofset_best, swarm_gain_best, swarm_ofset_best

    for iteration in range(1, ITERATIONS):  # should i ofset the count by one given init ran and populated arrays??
        print " iteration ", iteration, " of ", ITERATIONS
        for i in range(S):
            QtCore.QCoreApplication.processEvents()

            # pick two random numbers (R_p, r_g_gains)
            r_p_gains = random.uniform(0, 1)  # ok to use same for each dimension?
            r_g_gains = random.uniform(0, 1)

            r_p_ofset = random.uniform(0, 1)  
            r_g_ofset = random.uniform(0, 1)

            # update particle velocity for each dimension 
            p_gains[i].velocity_I = omega*p_gains[i].velocity_I + phi_p*r_p_gains*(p_gains[i].best_position_I - p_gains[i].position_I) + phi_g*r_g_gains*(solutions.I_gains - p_gains[i].position_I)
            p_gains[i].velocity_Q = omega*p_gains[i].velocity_Q + phi_p*r_p_gains*(p_gains[i].best_position_Q - p_gains[i].position_Q) + phi_g*r_g_gains*(solutions.Q_gains - p_gains[i].position_Q)

            p_ofset[i].velocity_I = omega*p_ofset[i].velocity_I + phi_p*r_p_ofset*(p_ofset[i].best_position_I - p_ofset[i].position_I) + phi_g*r_g_ofset*(solutions.I_ofset - p_ofset[i].position_I)
            p_ofset[i].velocity_Q = omega*p_ofset[i].velocity_Q + phi_p*r_p_ofset*(p_ofset[i].best_position_Q - p_ofset[i].position_Q) + phi_g*r_g_ofset*(solutions.Q_ofset - p_ofset[i].position_Q)


            # update particle temp position variable 'Particle erratic'
            Pe_gains_x = p_gains[i].position_I + p_gains[i].velocity_I
            Pe_gains_y = p_gains[i].position_Q + p_gains[i].velocity_Q

            Pe_ofset_x = p_ofset[i].position_I + p_ofset[i].velocity_I
            Pe_ofset_y = p_ofset[i].position_Q + p_ofset[i].velocity_Q

            # Make sure particle within constraints
            p_gains[i].position_I = check_in_box(Pe_gains_x, p_gains[i].position_I, limit_gains_x)
            p_gains[i].position_Q = check_in_box(Pe_gains_y, p_gains[i].position_Q, limit_gains_y)
            p_ofset[i].position_I = check_in_box(Pe_ofset_x, p_ofset[i].position_I, limit_ofset_x)
            p_ofset[i].position_Q = check_in_box(Pe_ofset_y, p_ofset[i].position_Q, limit_ofset_y)


            # f(x)
            # get new gain (set other populations best first)
            self.rx_dc_I_offset_sliderChanged_handler(solutions.I_ofset * self.dcOffsetDivisor)
            self.rx_dc_Q_offset_sliderChanged_handler(solutions.Q_ofset * self.dcOffsetDivisor)
            new_gain_pwr = gain_function(self, p_gains[i].position_I, p_gains[i].position_Q)

            # get new offset (set other populations best first)
            self.rx_I_gain_correction_sliderChanged_handler(solutions.I_gains * self.iqBalanceDivisor)
            self.rx_Q_gain_correction_sliderChanged_handler(solutions.Q_gains * self.iqBalanceDivisor)
            new_ofset_pwr = offset_function(self, p_ofset[i].position_I, p_ofset[i].position_Q ) 

            # plot points new
            set_gain_points(self, p_gains[i].position_I * 10, p_gains[i].position_Q * 10, new_gain_pwr, i)
            set_ofset_points(self, p_ofset[i].position_I * 10, p_ofset[i].position_Q * 10, new_ofset_pwr, i)

            # GAIN
            # if f(x) < f(p), update particle best known position
            if(new_gain_pwr < my_gain_best):
                p_gains[i].best_position_I = p_gains[i].position_I
                p_gains[i].best_position_Q = p_gains[i].position_Q
                my_gain_best = new_gain_pwr
                set_individual_best_gain_points(self, p_gains[i].best_position_I * 10, p_gains[i].best_position_Q * 10, my_gain_best, i)

                # if f(x) < f(g), update swarms best known position
                if(new_gain_pwr < swarm_gain_best):
                    solutions.I_gains = p_gains[i].position_I
                    solutions.Q_gains = p_gains[i].position_Q
                    swarm_gain_best = new_gain_pwr
                    swarm_best_count_gain = swarm_best_count_gain + 1
                    set_swarm_best_gain_points(self, solutions.I_gains * 10, solutions.Q_gains * 10, swarm_gain_best, swarm_best_count_gain)

            
            # OFFSET
            # if f(x) < f(p), update particle best known position
            if(new_ofset_pwr < my_ofset_best):
                p_ofset[i].best_position_I = p_ofset[i].position_I
                p_ofset[i].best_position_Q = p_ofset[i].position_Q
                my_ofset_best = new_ofset_pwr
                set_individual_best_ofset_points(self, p_ofset[i].best_position_I * 10, p_ofset[i].best_position_Q * 10, my_ofset_best, i)

                # if f(x) < f(g), update swarms best known position
                if(new_ofset_pwr < swarm_ofset_best):
                    solutions.I_ofset = p_ofset[i].best_position_I
                    solutions.Q_ofset = p_ofset[i].best_position_Q
                    swarm_ofset_best = new_ofset_pwr
                    swarm_best_count_ofset = swarm_best_count_ofset + 1
                    set_swarm_best_ofset_points(self, solutions.I_ofset * 10, solutions.Q_ofset * 10, swarm_ofset_best, swarm_best_count_ofset)


        print " swarm gain best ", swarm_gain_best, " swarm ofset best ", swarm_ofset_best


    #minimum = [solutions.I_gains, solutions.Q_gains]
    print "\n completed minimum search"
    print "best gains: ", solutions.I_gains, solutions.Q_gains
    print "best ofset: ", solutions.I_ofset, solutions.Q_ofset
    print "\n"
    return solutions



def gain_function(self, x, y):
    #z = 3*math.pow(x,2) +6*math.pow(y,2) + 2*x + 3*y +1
    self.rx_I_gain_correction = x
    self.rx_Q_gain_correction = y
    self.rx_I_gain_correction_sliderChanged_handler(self.rx_I_gain_correction * self.iqBalanceDivisor)
    self.rx_Q_gain_correction_sliderChanged_handler(self.rx_Q_gain_correction * self.iqBalanceDivisor)

    imag_power = get_imag_dbrms_AVG(self)
    return imag_power

def offset_function(self, x, y):
    #z = 5*math.pow(x,2) +4*math.pow(y,2) + 2*x + 3*y +3
    self.rx_dc_I_offset = x
    self.rx_dc_Q_offset = y
    self.rx_dc_I_offset_sliderChanged_handler(self.rx_dc_I_offset * self.dcOffsetDivisor)
    self.rx_dc_Q_offset_sliderChanged_handler(self.rx_dc_Q_offset * self.dcOffsetDivisor)

    CW_power   = get_DC_pwr_AVG(self)
    return CW_power

def function(x, y):

    # 4*x^2 +5*y^2 + 2*x + 3*y +2  >>>> around -0.3 and -0.3
    z = 4*math.pow(x,2) +5*math.pow(y,2) + 2*x + 3*y +2
    return z


###############################################################################################
###############################################################################################
###############################################################################################
#                                   MAIN routine
###############################################################################################
###############################################################################################
###############################################################################################
def optimise_parameters(self):
    global my_gain_best, my_ofset_best, swarm_gain_best, swarm_ofset_best
    init_dislpay_particles(self)

    #for CW in range(CW_start, CW_stop, CW_step): 
    for CW in cw_samples: 
    #for CW in range(3000, 3200, 200):
        print " Frequency (cw): ", CW

        # get manual tx calibration data for this CW
        self.tx_iq_I_correction = cal_LUT.look_up_calibration_value(CW, IGAIN, cal_dat_tx_manual)
        self.tx_iq_Q_correction = cal_LUT.look_up_calibration_value(CW, QGIAN, cal_dat_tx_manual)
        self.tx_dc_I_offset     = cal_LUT.look_up_calibration_value(CW, IDC, cal_dat_tx_manual)
        self.tx_dc_Q_offset     = cal_LUT.look_up_calibration_value(CW, QDC, cal_dat_tx_manual)

        # set tx calibration
        self.tx_I_correction_sliderChanged_handler(self.tx_iq_I_correction * self.iqBalanceDivisor)
        self.tx_Q_correction_sliderChanged_handler(self.tx_iq_Q_correction * self.iqBalanceDivisor)
        self.tx_dc_I_offset_sliderChanged_handler(self.tx_dc_I_offset  * self.dcOffsetDivisor)
        self.tx_dc_Q_offset_sliderChanged_handler(self.tx_dc_Q_offset  * self.dcOffsetDivisor)

        # new frequencies
        self.tx_cw_frq_sliderChanged_handler(CW)
        self.rx_cw_frq_sliderChanged_handler(CW)  
        self.tx_tone_frq_sliderChanged_handler(CW + CW/2 + 7)

        # do PSO
        my_gain_best     = 0.0
        my_ofset_best    = 0.0
        swarm_gain_best  = 0.0
        swarm_ofset_best = 0.0
        solutions = init_swarm(self, p_gains, p_ofset, S)
        minimum   = find_minimum(self, p_gains, p_ofset, S, solutions)


        #self.rx_dc_I_offset_sliderChanged_handler(solutions.I_ofset * self.dcOffsetDivisor)
        #self.rx_dc_Q_offset_sliderChanged_handler(solutions.Q_ofset * self.dcOffsetDivisor)
        #self.rx_I_gain_correction_sliderChanged_handler(solutions.I_gains * self.iqBalanceDivisor)
        #self.rx_Q_gain_correction_sliderChanged_handler(solutions.Q_gains * self.iqBalanceDivisor)

        self.rx_I_gain_correction   = solutions.I_gains
        self.rx_Q_gain_correction   = solutions.Q_gains
        self.rx_dc_I_offset         = solutions.I_ofset
        self.rx_dc_Q_offset         = solutions.Q_ofset

        SB_supression = 0
        CW_power      = get_DC_pwr_AVG(self)
        tone_power    = get_tone_dbrms_AVG(self)
        imag_power    = get_imag_dbrms_AVG(self)


        print "\n"
        print "Final Values"
        print "I Gain:\t", self.rx_I_gain_correction
        print "Q Gain:\t", self.rx_Q_gain_correction
        print "I DC:\t", self.rx_dc_I_offset
        print "Q Dc:\t", self.rx_dc_Q_offset
        print "Best Supression: \t", SB_supression
        print "Dc power level: \t", CW_power
        print "tone power level: \t", tone_power
        print "imag power level: \t", imag_power
        print "\n\n\n"

        record_results(self, CW, self.tx_wave_freq, self.rx_I_gain_correction, self.rx_Q_gain_correction, self.rx_dc_I_offset, self.rx_dc_Q_offset, SB_supression, CW_power, tone_power, imag_power)


    ##########################
    #### end algorythm  ######
    ##########################

    # print to file:
    resultsFile     = csv.writer(open(results_filename, "wb"))
    for writre_index in range(len(result_Igain)):
        resultsFile.writerow([ result_CW[writre_index], result_Fmod[writre_index], result_Igain[writre_index], result_Qgain[writre_index], result_Idc[writre_index], result_Qdc[writre_index], result_SBpwr[writre_index], result_CWpwr[writre_index], result_Tonepwr[writre_index], result_Imagpwr[writre_index] ])


    #resultsFile.close()
    print " ===== Completed Successfully ====="

###############################################################################################################
##
###############################################################################################################


def check_in_box(Pe, Pi, lim):
    count = 0
    Pnew = 2
    if(abs(Pe)>lim):
        while(abs(Pnew)>abs(lim)):
            count = count + 1
            #sys.stdout.write('.')
            Pnew = bounce(Pe, Pi, lim)
            Pe = Pnew
            #if(count > 5):
                #print "Pe ", Pe, " Pi ", Pi, "Pnew", Pnew, "  out of control bouncing"
            #if(count > 10):
                #sys.exit()
    else:
        Pnew = Pe
    return Pnew

def bounce(Pe, Pi, lim):
    if(Pe<lim):
        lim = lim * (-1)
    return 2 * lim - Pe


def get_SB_supression_AVG(self):
    #print "inside get_SB_supression_AVG()"
    sumData = 0
    for sample_index in range(power_AVG_window_length):
        #sys.stdout.write('@')
        #print "GET AVG SB  ", sample_index
        cal_data = liblv_uhd_pub_api.return_cal_data_SB_supression()
        #print "acquired sample ", sample_index, " from liblv_uhd_pub_api.return_cal_data()    value = ", cal_data
        sumData = sumData + cal_data
    average_SB_power = sumData / power_AVG_window_length
    #print "finnished get_SB_supression_AVG() with AVG = ", average_SB_power
    return average_SB_power

def get_DC_pwr_AVG(self):
    #print "inside get_SB_supression_AVG()"
    sumData = 0
    #sys.stdout.write(' ')
    for sample_index in range(power_AVG_window_length):
        #sys.stdout.write('D')
        #print "GET AVG DC ", sample_index
        cal_data = liblv_uhd_pub_api.return_cal_data_CW_power()
        #print "acquired sample ", sample_index, " from liblv_uhd_pub_api.return_cal_data()    value = ", cal_data
        sumData = sumData + cal_data
    average_DC_power = sumData / power_AVG_window_length
    #print "finnished get_SB_supression_AVG() with AVG = ", average_DC_power
    return average_DC_power

def get_tone_dbrms_AVG(self):
    #print "inside get_tone_dbrms()"
    sumData = 0
    #sys.stdout.write(' ')
    for sample_index in range(power_AVG_window_length):
        #sys.stdout.write('T')
        #print "GET TONE PWR ", sample_index
        cal_data = liblv_uhd_pub_api.return_tone_dbrms()
        #print "acquired sample ", sample_index, " from liblv_uhd_pub_api.return_tone_dbrms()    value = ", cal_data
        sumData = sumData + cal_data
    average_tone_power = sumData / power_AVG_window_length
    #print "finnished get_tone_dbrms() with AVG = ", average_DC_power
    return average_tone_power


def get_imag_dbrms_AVG(self):
    #print "inside get_imag_dbrms()"
    sumData = 0
    #sys.stdout.write(' ')
    for sample_index in range(power_AVG_window_length):
        #sys.stdout.write('S')
        #print "GET IMAG PWR ", sample_index
        cal_data = liblv_uhd_pub_api.return_imag_dbrms()
        #print "acquired sample ", sample_index, " from liblv_uhd_pub_api.return_imag_dbrms()    value = ", cal_data
        sumData = sumData + cal_data
    average_imag_power = sumData / power_AVG_window_length
    #print "finnished get_imag_dbrms() with AVG = ", average_DC_power
    return average_imag_power


#######################################################################
####                        Plotting                         ##########
#######################################################################

def launch_PSO_evo_viewer(self):
    ##########################################################################
    # set up the PSO evolution viewer window with objects like grids
    ##########################################################################
    # for gains
    self.gains_w.opts['distance'] = 100
    self.gains_w.opts['center'] = QtGui.QVector3D(0, 0, -35)
    self.gains_w.show()
    self.gains_w.setWindowTitle('Gain Correction PSO') 

    self.gains_g1 = gl.GLGridItem()
    self.gains_g1.setSize(x=1,y=1)
    self.gains_w.addItem(self.gains_g1)

    self.gains_g2 = gl.GLGridItem()
    self.gains_g2.label = QtGui.QLabel('test 2') # fail
    self.gains_g2.setSize(x=1,y=1,z=1) # this is buggy in source :(
    self.gains_g2.scale(x=4.5,y=1,z=1)
    self.gains_g2.rotate(90, 0, 1, 0)
    self.gains_g2.translate(0,0,-45)
    self.gains_w.addItem(self.gains_g2)

    self.gains_g3 = gl.GLGridItem()

    self.gains_g3.setSize(x=1,y=1,z=1) # this is buggy in source :(
    self.gains_g3.scale(x=1,y=4.5,z=1)
    self.gains_g3.rotate(90, 1, 0, 0)
    self.gains_g3.translate(0,0,-45)
    self.gains_w.addItem(self.gains_g3)
    
    self.gains_ax = gl.GLAxisItem()
    self.gains_ax.label = QtGui.QLabel('test') # fail
    self.gains_ax.setSize(x=1,y=1,z=1)
    self.gains_ax.scale(x=10,y=10,z=-45)
    self.gains_w.addItem(self.gains_ax)

    
    # for offset
    self.ofset_w.opts['distance'] = 100
    self.ofset_w.opts['center'] = QtGui.QVector3D(0, 0, -35)
    self.ofset_w.show()
    self.ofset_w.setWindowTitle('Offset Correction PSO') 

    self.ofset_g1 = gl.GLGridItem()
    self.ofset_g1.setSize(x=1,y=1)
    self.ofset_w.addItem(self.ofset_g1)

    self.ofset_g2 = gl.GLGridItem()
    self.ofset_g2.label = QtGui.QLabel('test 2') # fail
    self.ofset_g2.setSize(x=1,y=1,z=1) # this is buggy in source :(
    self.ofset_g2.scale(x=4.5,y=1,z=1)
    self.ofset_g2.rotate(90, 0, 1, 0)
    self.ofset_g2.translate(0,0,-45)
    self.ofset_w.addItem(self.ofset_g2)

    self.ofset_g3 = gl.GLGridItem()

    self.ofset_g3.setSize(x=1,y=1,z=1) # this is buggy in source :(
    self.ofset_g3.scale(x=1,y=4.5,z=1)
    self.ofset_g3.rotate(90, 1, 0, 0)
    self.ofset_g3.translate(0,0,-45)
    self.ofset_w.addItem(self.ofset_g3)
    
    self.ofset_ax = gl.GLAxisItem()
    self.ofset_ax.label = QtGui.QLabel('test') # fail
    self.ofset_ax.setSize(x=1,y=1,z=1)
    self.ofset_ax.scale(x=10,y=10,z=-45)
    self.ofset_w.addItem(self.ofset_ax)

    #self.QUEUE = Queue()

def ini_PSO_plat_data(self, S):

    # plot dot type
    class PlotDot(DataObject):
        __slots__ = ('size', 'pos_x', 'pos_y', 'pos_z', 'color_r', 'color_b', 'color_g', 'opaque')

    # init array of plot dots
    #self.numPars = S
    self.gains_particles = [] * S
    self.ofset_particles = [] * S
    #self.particles[:] = [] * S

    #self.numPars2 = ITERATIONS
    self.gains_individual_best_particles = [] * S
    self.ofset_individual_best_particles = [] * S
    #self.individual_best_particles[:] = [] * ITERATIONS

    #self.numPars3 = ITERATIONS
    self.gains_swarms_best_particles = [] * (ITERATIONS * S)
    self.ofset_swarms_best_particles = [] * (ITERATIONS * S)
    #self.swarms_best_particles[:] = []

    self.opaque = 1
    self.dot_size1 = 0.4
    self.dot_size2 = 0.45
    self.dot_size3 = 0.5

    # swarm new values
    for i in range(self.numPars):
        self.gains_particle = PlotDot(self.dot_size1, 0., 0., 0., 1., 1., 1., 1.)
        self.gains_particles.append(self.gains_particle) 
    for i in range(self.numPars):
        self.ofset_particle = PlotDot(self.dot_size1, 0., 0., 0., 1., 1., 1., 1.)
        self.ofset_particles.append(self.ofset_particle) 

    # individual best values
    for i in range(self.numPars2):
        self.gains_individual_best_particle = PlotDot(self.dot_size2, 0., 0., 0., 1., 0., 0., 1.) # red
        self.gains_individual_best_particles.append(self.gains_individual_best_particle) 
    for i in range(self.numPars2):
        self.ofset_individual_best_particle = PlotDot(self.dot_size2, 0., 0., 0., 1., 0., 0., 1.) # red
        self.ofset_individual_best_particles.append(self.ofset_individual_best_particle) 

    # best particle
    for i in range(self.numPars3):
        self.gains_swarms_best_particle = PlotDot(self.dot_size3, 0., 0., 0., 0., 1., 0., 1.) # green?
        self.gains_swarms_best_particles.append(self.gains_swarms_best_particle) 
    for i in range(self.numPars3):
        self.ofset_swarms_best_particle = PlotDot(self.dot_size3, 0., 0., 0., 0., 1., 0., 1.) # green?
        self.ofset_swarms_best_particles.append(self.ofset_swarms_best_particle) 



    # init matricies of scatter plot objects - swarm new
    for i in range(self.numPars):
        self.gains_pos[i,0] = self.gains_particles[i].pos_x 
        self.gains_pos[i,1] = self.gains_particles[i].pos_y 
        self.gains_pos[i,2] = self.gains_particles[i].pos_z
        self.size[i]  = self.dot_size1
        self.color[i,0] = self.gains_particles[i].color_r
        self.color[i,1] = self.gains_particles[i].color_b
        self.color[i,2] = self.gains_particles[i].color_g
        self.color[i,3] = self.opaque

    for i in range(self.numPars):
        self.ofset_pos[i,0] = self.ofset_particles[i].pos_x 
        self.ofset_pos[i,1] = self.ofset_particles[i].pos_y 
        self.ofset_pos[i,2] = self.ofset_particles[i].pos_z
        self.size[i]  = self.dot_size1
        self.color[i,0] = self.ofset_particles[i].color_r
        self.color[i,1] = self.ofset_particles[i].color_b
        self.color[i,2] = self.ofset_particles[i].color_g
        self.color[i,3] = self.opaque

    # individual best
    for i in range(self.numPars2):
        self.gains_pos2[i,0] = self.gains_individual_best_particles[i].pos_x 
        self.gains_pos2[i,1] = self.gains_individual_best_particles[i].pos_y 
        self.gains_pos2[i,2] = self.gains_individual_best_particles[i].pos_z
        self.size2[i]  = self.dot_size2
        self.color2[i,0] = self.gains_individual_best_particles[i].color_r
        self.color2[i,1] = self.gains_individual_best_particles[i].color_b
        self.color2[i,2] = self.gains_individual_best_particles[i].color_g
        self.color2[i,3] = self.opaque

    for i in range(self.numPars2):
        self.ofset_pos2[i,0] = self.ofset_individual_best_particles[i].pos_x 
        self.ofset_pos2[i,1] = self.ofset_individual_best_particles[i].pos_y 
        self.ofset_pos2[i,2] = self.ofset_individual_best_particles[i].pos_z
        self.size2[i]  = self.dot_size2
        self.color2[i,0] = self.ofset_individual_best_particles[i].color_r
        self.color2[i,1] = self.ofset_individual_best_particles[i].color_b
        self.color2[i,2] = self.ofset_individual_best_particles[i].color_g
        self.color2[i,3] = self.opaque

    # swarm best
    for i in range(self.numPars3):
        self.gains_pos3[i,0] = self.gains_swarms_best_particles[i].pos_x 
        self.gains_pos3[i,1] = self.gains_swarms_best_particles[i].pos_y 
        self.gains_pos3[i,2] = self.gains_swarms_best_particles[i].pos_z
        self.size3[i]  = self.dot_size3
        self.color3[i,0] = self.gains_swarms_best_particles[i].color_r
        self.color3[i,1] = self.gains_swarms_best_particles[i].color_b
        self.color3[i,2] = self.gains_swarms_best_particles[i].color_g
        self.color3[i,3] = self.opaque

    for i in range(self.numPars3):
        self.ofset_pos3[i,0] = self.ofset_swarms_best_particles[i].pos_x 
        self.ofset_pos3[i,1] = self.ofset_swarms_best_particles[i].pos_y 
        self.ofset_pos3[i,2] = self.ofset_swarms_best_particles[i].pos_z
        self.size3[i]  = self.dot_size3
        self.color3[i,0] = self.ofset_swarms_best_particles[i].color_r
        self.color3[i,1] = self.ofset_swarms_best_particles[i].color_b
        self.color3[i,2] = self.ofset_swarms_best_particles[i].color_g
        self.color3[i,3] = self.opaque

    # make scatter plot object
    self.gains_w.addItem(self.sp_gains_1)
    self.gains_w.addItem(self.sp_gains_2)
    self.gains_w.addItem(self.sp_gains_3)

    self.ofset_w.addItem(self.sp_ofset_1)
    self.ofset_w.addItem(self.sp_ofset_2)
    self.ofset_w.addItem(self.sp_ofset_3)

    # set data
    self.sp_gains_1.setData(pos=self.gains_pos)
    self.sp_gains_2.setData(pos=self.gains_pos2)
    self.sp_gains_3.setData(pos=self.gains_pos3)

    self.sp_ofset_1.setData(pos=self.ofset_pos)
    self.sp_ofset_2.setData(pos=self.ofset_pos2)
    self.sp_ofset_3.setData(pos=self.ofset_pos3)

    QtCore.QCoreApplication.processEvents()


def set_gain_points(self, x, y, z, index):
            self.gains_pos[index,0] = x
            self.gains_pos[index,1] = y
            self.gains_pos[index,2] = z
            self.sp_gains_1.setData(pos=self.gains_pos)
            #self.w1.addItem(sp1)
            QtCore.QCoreApplication.processEvents()
def set_ofset_points(self, x, y, z, index):
            self.ofset_pos[index,0] = x
            self.ofset_pos[index,1] = y
            self.ofset_pos[index,2] = z
            self.sp_ofset_1.setData(pos=self.ofset_pos)
            #self.w1.addItem(sp1)
            QtCore.QCoreApplication.processEvents()

def set_individual_best_gain_points(self, x, y, z, index):
            self.gains_pos2[index,0] = x
            self.gains_pos2[index,1] = y
            self.gains_pos2[index,2] = z
            self.sp_gains_2.setData(pos=self.gains_pos2)
            #self.w1.addItem(sp1)
            QtCore.QCoreApplication.processEvents()
def set_individual_best_ofset_points(self, x, y, z, index):
            self.ofset_pos2[index,0] = x
            self.ofset_pos2[index,1] = y
            self.ofset_pos2[index,2] = z
            self.sp_ofset_2.setData(pos=self.ofset_pos2)
            #self.w1.addItem(sp1)
            QtCore.QCoreApplication.processEvents()

def set_swarm_best_gain_points(self, x, y, z, index):
            self.gains_pos3[index,0] = x
            self.gains_pos3[index,1] = y
            self.gains_pos3[index,2] = z
            self.sp_gains_3.setData(pos=self.gains_pos3)
            #self.w1.addItem(sp1)
            QtCore.QCoreApplication.processEvents()
def set_swarm_best_ofset_points(self, x, y, z, index):
            self.ofset_pos3[index,0] = x
            self.ofset_pos3[index,1] = y
            self.ofset_pos3[index,2] = z
            self.sp_ofset_3.setData(pos=self.ofset_pos3)
            #self.w1.addItem(sp1)
            QtCore.QCoreApplication.processEvents()
  



#######################################################################
#### record all results per CW - > CW, I_cal, Q_cal, Supress ##########
#######################################################################
def record_results(self, CW, Fmod, Igain, Qgain, Idc, Qdc, SB_supression, CW_power, tone_power, imag_power):
    result_CW.append(CW)
    result_Fmod.append(Fmod)
    result_Igain.append(round(Igain,6)) 
    result_Qgain.append(round(Qgain,6)) 
    result_Idc.append(round(Idc,6)) 
    result_Qdc.append(round(Qdc,6))
    result_SBpwr.append(round(SB_supression,1))
    result_CWpwr.append(round(CW_power,1))
    result_Tonepwr.append(round(tone_power,1))
    result_Imagpwr.append(round(imag_power,1))
    ++r


