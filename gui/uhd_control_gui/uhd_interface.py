#!/usr/bin/python
# -*- coding: utf-8 -*-


import sys
from PySide import QtGui, QtCore, QtOpenGL
import threading
from Queue import Queue

import liblv_uhd_pub_api
import calibration_routine_sweep
import calibration_routine_PSO_v2

from dataObject import DataObject
import numpy as np
import pyqtgraph.opengl as gl

import math

##################################################
#    Constants
##################################################
MHZ                     = 1000000.0
KHZ                     = 1000.0

iqBalanceRange          = 1000
dcBalanceRange          = 1000
numDCdispDigits         = 6
numIQdispDigits         = 6
tickDivisor             = 1000.0  # Decimal point required to force use of rational values

gainRange               = [-35, 25]
freqRange               = [400, 4000]
moduRange               = [0, 10000]

##################################################
#    Initilisation Values
##################################################
default_tx_device_address  = str("addr=192.168.10.3")
default_tx_antenna         = str("TX")
default_tx_center_freq     = 400 # (mhz)
default_tx_center_gain     = -25 #-25 
default_tx_rate            = 12500000
default_tx_wave_freq       = 600 #608 #1000  # (khz)
default_tx_wave_ampl       = 1
default_tx_iq_I_correction = 0 #-0.102 #-0.088
default_tx_iq_Q_correction = 0 #-0.514 #-0.526
default_tx_dc_I_offset     = 0 #0.2 #0.219
default_tx_dc_Q_offset     = 0 #0.176 #0.172

default_rx_device_address  = str("addr=192.168.10.3")
default_rx_antenna         = str("RX")
default_rx_center_freq     = 400 # (mhz)
default_rx_center_gain     = -25 #-25 
default_rx_rate            = 12500000
default_rx_I_gain_correction    = 0.0
default_rx_Q_gain_correction    = 0.0
default_rx_dc_I_offset     = 0.0
default_rx_dc_Q_offset     = 0.0


##################################################
#    Window and frame dimensions
##################################################
window_x               = 1024
window_y               = 700
X                      = 0 # used for vector index
Y                      = 1
frm_topfull_dmnsns     = [window_x, window_y/20]
frm_topleft_dmnsns     = [window_x/2, window_y/4]
frm_topright_dmnsns    = [window_x/2, window_y/4]
frm_left_dmnsns        = [window_x/4, window_y/2]
frm_centerleft_dmnsns  = [window_x/4, window_y/2]
frm_centerright_dmnsns = [window_x/4, window_y/2]
frm_right_dmnsns       = [window_x/4, window_y/2]
frm_bottomfull_dmnsns  = [window_x, window_y/16]

############################################################################################################################################
############################################################################################################################################
#                                                           Widget                                                                        #
############################################################################################################################################
############################################################################################################################################






class MasterWidget(QtGui.QWidget):
    
    def __init__(self):
        super(MasterWidget, self).__init__()
        
        self.init()
        
    def init(self):
        ###################################
        # temporary initilisations
        ###################################

        self.gains_w = gl.GLViewWidget()  # for gains
        self.ofset_w = gl.GLViewWidget()  # for ofset

        

        ############################################################################################################################################
        #          Variables with initialisation
        ############################################################################################################################################
        self.usrp_tx           = 0
        self.device_address_tx = default_tx_device_address
        self.tx_antenna        = default_tx_antenna
        self.tx_center_freq    = default_tx_center_freq
        self.tx_center_gain    = default_tx_center_gain
        self.tx_rate           = default_tx_rate
        self.tx_wave_freq      = default_tx_wave_freq
        self.tx_wave_ampl      = default_tx_wave_ampl
        self.tx_iq_I_correction= default_tx_iq_I_correction
        self.tx_iq_Q_correction= default_tx_iq_Q_correction
        self.tx_dc_I_offset    = default_tx_dc_I_offset
        self.tx_dc_Q_offset    = default_tx_dc_Q_offset

        self.usrp_rx           = 0
        self.device_address_rx = default_rx_device_address
        self.rx_antenna        = default_rx_antenna
        self.rx_center_freq    = default_rx_center_freq
        self.rx_center_gain    = default_rx_center_gain
        self.rx_rate           = default_rx_rate
        self.rx_I_gain_correction   = default_rx_I_gain_correction
        self.rx_Q_gain_correction   = default_rx_Q_gain_correction
        self.rx_dc_I_offset    = default_rx_dc_I_offset
        self.rx_dc_Q_offset    = default_rx_dc_Q_offset

        self.iqBalanceDivisor  = tickDivisor
        self.dcOffsetDivisor   = tickDivisor

        ############################################################################################################################################
        #          Create 0bjects
        ############################################################################################################################################


        # ===================================================================
        #          System Config object     *** Transmitter ***
        # ===================================================================
        # -------------------------------------
        #             labels
        # -------------------------------------
        self.label_config_tx     = QtGui.QLabel('TX Device Configuration')
        self.label_addr_tx       = QtGui.QLabel('Device Address') 
        self.label_rate_tx       = QtGui.QLabel('TX Rate (units?)') 
        self.label_antenna_tx    = QtGui.QLabel('TX Antenna') 
        self.label_signal_tx     = QtGui.QLabel('Signal Type') 
        # -------------------------------------
        #             buttons
        # -------------------------------------
        self.btn_create_usrp_tx  = QtGui.QPushButton('Create Device', self)
        # -------------------------------------
        #             Input
        # -------------------------------------
        self.address_input_bx_tx = QtGui.QLineEdit()
        self.address_input_bx_tx.insert(default_tx_device_address)

        self.rate_input_bx_tx    = QtGui.QLineEdit()
        self.rate_input_bx_tx.insert(str(self.tx_rate))

        self.antenna_select_tx   = QtGui.QComboBox() 
        self.antenna_select_tx.insertItem(1, "TX") 
        self.antenna_select_tx.insertItem(2, "TX/RX") 
        self.antenna_select_tx.insertItem(2, "CAL") 

        self.config_select_tx    = QtGui.QComboBox() 
        self.config_select_tx.insertItem(1, "LSB") 
        self.config_select_tx.insertItem(2, "USB")
        self.config_select_tx.insertItem(3, "DSB")

        # ===================================================================
        #            CW objects              *** Transmitter ***
        # ===================================================================
        # -------------------------------------
        #             labels
        # -------------------------------------
        self.label_cw_tx        = QtGui.QLabel('TX Signal') 
        self.label_cw_tx.setAlignment(QtCore.Qt.AlignHCenter)
        self.label_cw_tx_gain   = QtGui.QLabel('TX CW Gain')
        self.label_cw_tx_freq   = QtGui.QLabel('TX CW Frequency (MHz)')

        # -------------------------------------
        #             displays
        # -------------------------------------
        # amplitude
        self.lcd_cw_amp_tx  = QtGui.QLCDNumber(self)
        self.lcd_cw_amp_tx.display(default_tx_center_gain)
        # frequency
        self.lcd_cw_frq_tx  = QtGui.QLCDNumber(self)   
        self.lcd_cw_frq_tx.display(self.tx_center_freq)

        # -------------------------------------
        #             sliders
        # -------------------------------------
        # amplitude
        self.sld_cw_amp_tx  = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_cw_amp_tx.setRange(gainRange[0], gainRange[1])
        self.sld_cw_amp_tx.setValue(default_tx_center_gain)
        # frequency
        self.sld_cw_frq_tx  = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_cw_frq_tx.setRange(freqRange[0], freqRange[1]) 
        self.sld_cw_frq_tx.setValue(self.tx_center_freq) 

        # ===================================================================
        #               Tone objects              *** Transmitter ***
        # ===================================================================
        # -------------------------------------
        #             labels
        # -------------------------------------
        self.label_tone_ampl_tx = QtGui.QLabel('TX Modulation Amplitude ') 
        self.label_tone_freq_tx = QtGui.QLabel('TX Modulation Frequency (KHz)') 

        # -------------------------------------
        #             displays
        # -------------------------------------
        # amplitude
        self.lcd_tone_amp_tx = QtGui.QLCDNumber(self)
        self.lcd_tone_amp_tx.display(default_tx_wave_ampl)
        # frequency
        self.lcd_tone_frq_tx = QtGui.QLCDNumber(self)
        self.lcd_tone_frq_tx.display(default_tx_wave_freq)

        # -------------------------------------
        #             sliders
        # -------------------------------------
        # amplitude
        self.sld_tone_amp_tx = QtGui.QSlider(QtCore.Qt.Horizontal) # not working in api yet
        # frequency
        self.sld_tone_frq_tx = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_tone_frq_tx.setRange(moduRange[0], moduRange[1]) 
        self.sld_tone_frq_tx.setValue(default_tx_wave_freq) 

        # ===================================================================
        #            IQ Balance objects        *** Transmitter ***
        # ===================================================================
        # -------------------------------------
        #             labels
        # -------------------------------------
        self.label_cal_tx            = QtGui.QLabel('TX Calibration')
        self.label_cal_tx.setAlignment(QtCore.Qt.AlignHCenter)
        self.label_iq_I_correction_tx   = QtGui.QLabel('I Gain Correction')
        self.label_iq_Q_correction_tx   = QtGui.QLabel('Q Gain Correction')

        # -------------------------------------
        #             displays
        # -------------------------------------
        self.lcd_iq_I_correction_tx  = QtGui.QLCDNumber(self)
        self.lcd_iq_I_correction_tx.smallDecimalPoint()
        self.lcd_iq_I_correction_tx.setDigitCount(numIQdispDigits)
        self.lcd_iq_I_correction_tx.display(self.tx_iq_I_correction)

        self.lcd_iq_Q_correction_tx  = QtGui.QLCDNumber(self)
        self.lcd_iq_Q_correction_tx.setDigitCount(numIQdispDigits)
        self.lcd_iq_Q_correction_tx.smallDecimalPoint()
        self.lcd_iq_Q_correction_tx.display(self.tx_iq_Q_correction)
        
        # -------------------------------------
        #             sliders
        # -------------------------------------
        self.sld_iq_I_correction_tx  = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_iq_I_correction_tx.setRange(-iqBalanceRange, iqBalanceRange)
        self.sld_iq_Q_correction_tx  = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_iq_Q_correction_tx.setRange(-iqBalanceRange, iqBalanceRange)

        # ===================================================================
        #            DC Offset objects       *** Transmitter ***
        # ===================================================================
        # -------------------------------------
        #             labels
        # -------------------------------------
        self.label_dc_I_offset_tx   = QtGui.QLabel('I DC Offset')
        self.label_dc_Q_offset_tx   = QtGui.QLabel('Q DC Offset')

        # -------------------------------------
        #             displays
        # -------------------------------------
        self.lcd_dc_I_offset_tx  = QtGui.QLCDNumber(self)
        self.lcd_dc_I_offset_tx.smallDecimalPoint()
        self.lcd_dc_I_offset_tx.setDigitCount(numDCdispDigits)
        self.lcd_dc_I_offset_tx.display(self.tx_dc_I_offset)

        self.lcd_dc_Q_offset_tx  = QtGui.QLCDNumber(self)
        self.lcd_dc_Q_offset_tx.smallDecimalPoint()
        self.lcd_dc_Q_offset_tx.setDigitCount(numDCdispDigits)
        self.lcd_dc_Q_offset_tx.display(self.tx_dc_Q_offset)
        
        # -------------------------------------
        #             sliders
        # -------------------------------------
        self.sld_dc_I_offset_tx  = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_dc_I_offset_tx.setRange(-dcBalanceRange, dcBalanceRange)
        self.sld_dc_Q_offset_tx  = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_dc_Q_offset_tx.setRange(-dcBalanceRange, dcBalanceRange)


        # ===================================================================   
        #          System Config object     *** Receiver ***                    
        # ===================================================================   
        # -------------------------------------
        #             labels
        # -------------------------------------
        self.label_config_rx     = QtGui.QLabel('RX Device Configuration')
        self.label_addr_rx       = QtGui.QLabel('Device Address') 
        self.label_rate_rx       = QtGui.QLabel('RX Rate') 
        self.label_antenna_rx    = QtGui.QLabel('RX Antenna') 

        # -------------------------------------
        #             buttons
        # -------------------------------------
        self.btn_create_usrp_rx   = QtGui.QPushButton('Create Device', self)
        # -------------------------------------
        #             Input
        # -------------------------------------
        self.address_input_bx_rx = QtGui.QLineEdit()
        self.address_input_bx_rx.insert(default_rx_device_address)

        self.rate_input_bx_rx = QtGui.QLineEdit()
        self.rate_input_bx_rx.insert(str(self.rx_rate))

        self.antenna_select_rx   = QtGui.QComboBox() 
        self.antenna_select_rx.insertItem(1, "RX") 
        self.antenna_select_rx.insertItem(2, "RX2") 
        self.antenna_select_rx.insertItem(3, "CAL")

        # ===================================================================
        #            CW objects              *** Receiver ***
        # ===================================================================
        # -------------------------------------
        #             labels
        # -------------------------------------
        self.label_cw_rx        = QtGui.QLabel('RX Signal') 
        self.label_cw_rx.setAlignment(QtCore.Qt.AlignHCenter)
        self.label_cw_rx_gain   = QtGui.QLabel('RX CW Gain')
        self.label_cw_rx_freq   = QtGui.QLabel('RX CW Frequency (MHz)')

        # -------------------------------------
        #             displays
        # -------------------------------------
        # amplitude
        self.lcd_cw_amp_rx  = QtGui.QLCDNumber(self)
        self.lcd_cw_amp_rx.display(default_rx_center_gain)
        # frequency
        self.lcd_cw_frq_rx  = QtGui.QLCDNumber(self)   
        self.lcd_cw_frq_rx.display(self.rx_center_freq)
        # -------------------------------------
        #             sliders
        # -------------------------------------
        # amplitude
        self.sld_cw_amp_rx  = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_cw_amp_rx.setRange(gainRange[0], gainRange[1])
        self.sld_cw_amp_rx.setValue(default_rx_center_gain)
        # frequency
        self.sld_cw_frq_rx  = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_cw_frq_rx.setRange(freqRange[0], freqRange[1]) 
        self.sld_cw_frq_rx.setValue(self.rx_center_freq) 

        # ===================================================================
        #            IQ Balance objects        *** Receiver ***
        # ===================================================================
        # -------------------------------------
        #             labels
        # -------------------------------------
        self.label_cal_rx               = QtGui.QLabel('RX Calibration')
        self.label_cal_rx.setAlignment(QtCore.Qt.AlignHCenter)
        self.label_iq_I_correction_rx   = QtGui.QLabel('I Gain Correction')
        self.label_iq_Q_correction_rx   = QtGui.QLabel('Q Gain Correction')

        # -------------------------------------
        #             displays
        # -------------------------------------
        self.lcd_iq_I_correction_rx  = QtGui.QLCDNumber(self)
        self.lcd_iq_I_correction_rx.smallDecimalPoint()
        self.lcd_iq_I_correction_rx.setDigitCount(numIQdispDigits)
        self.lcd_iq_I_correction_rx.display(self.rx_I_gain_correction)

        self.lcd_iq_Q_correction_rx  = QtGui.QLCDNumber(self)
        self.lcd_iq_Q_correction_rx.setDigitCount(numIQdispDigits)
        self.lcd_iq_Q_correction_rx.smallDecimalPoint()
        self.lcd_iq_Q_correction_rx.display(self.rx_Q_gain_correction)
        
        # -------------------------------------
        #             sliders
        # -------------------------------------
        self.sld_iq_I_correction_rx  = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_iq_I_correction_rx.setRange(-iqBalanceRange, iqBalanceRange)
        self.sld_iq_Q_correction_rx  = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_iq_Q_correction_rx.setRange(-iqBalanceRange, iqBalanceRange)

        # ===================================================================
        #            DC Offset objects       *** Receiver ***
        # ===================================================================
        # -------------------------------------
        #             labels
        # -------------------------------------
        self.label_dc_I_offset_rx   = QtGui.QLabel('I DC Offset')
        self.label_dc_Q_offset_rx   = QtGui.QLabel('Q DC Offset')

        # -------------------------------------
        #             displays
        # -------------------------------------
        self.lcd_dc_I_offset_rx  = QtGui.QLCDNumber(self)
        self.lcd_dc_I_offset_rx.smallDecimalPoint()
        self.lcd_dc_I_offset_rx.setDigitCount(numDCdispDigits)
        self.lcd_dc_I_offset_rx.display(self.rx_dc_I_offset)

        self.lcd_dc_Q_offset_rx  = QtGui.QLCDNumber(self)
        self.lcd_dc_Q_offset_rx.smallDecimalPoint()
        self.lcd_dc_Q_offset_rx.setDigitCount(numDCdispDigits)
        self.lcd_dc_Q_offset_rx.display(self.rx_dc_Q_offset)
        
        # -------------------------------------
        #             sliders
        # -------------------------------------
        self.sld_dc_I_offset_rx  = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_dc_I_offset_rx.setRange(-dcBalanceRange, dcBalanceRange)
        self.sld_dc_Q_offset_rx  = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.sld_dc_Q_offset_rx.setRange(-dcBalanceRange, dcBalanceRange)


        # ===================================================================
        #            Misc objects 
        # ===================================================================
        # -------------------------------------
        #             Buttons
        # -------------------------------------
        # quit button
        btn_quit = QtGui.QPushButton('Quit', self)                  
        btn_quit.resize(btn_quit.sizeHint())                                # gives recomended size for button

        # calibrate rx button
        btn_sweep_cal_rx = QtGui.QPushButton('Cal RX sweep', self)                  
        btn_sweep_cal_rx.resize(btn_sweep_cal_rx.sizeHint())
        btn_PSO_cal_rx = QtGui.QPushButton('Cal RX PSO', self)                  
        btn_PSO_cal_rx.resize(btn_PSO_cal_rx.sizeHint())
        btn_PSO_viewer = QtGui.QPushButton('PSO Viewer', self)                  
        btn_PSO_viewer.resize(btn_PSO_viewer.sizeHint())
        #btn_PSO_test = QtGui.QPushButton('PSO test', self)                  
        #btn_PSO_test.resize(btn_PSO_test.sizeHint())

        ############################################################################################################################################
        #        Make Frames  -- using QSplitter
        ############################################################################################################################################
        # make sub frames
        frm_topfull         = QtGui.QFrame(self) # unused so far   
        frm_topleft         = QtGui.QFrame(self)
        frm_topright        = QtGui.QFrame(self)   
        frm_left            = QtGui.QFrame(self)
        frm_centerleft      = QtGui.QFrame(self)
        frm_centerright     = QtGui.QFrame(self)
        frm_right           = QtGui.QFrame(self)
        frm_bottomfull      = QtGui.QFrame(self)
        
        # Assign Frame Dimensions
        frm_topfull.resize(frm_topfull_dmnsns[X], frm_topfull_dmnsns[Y])
        frm_topleft.resize(frm_topleft_dmnsns[X], frm_topleft_dmnsns[Y])
        frm_topright.resize(frm_topright_dmnsns[X], frm_topright_dmnsns[Y])
        frm_left.resize(frm_left_dmnsns[X], frm_left_dmnsns[Y])
        frm_centerleft.resize(frm_centerleft_dmnsns[X], frm_centerleft_dmnsns[Y])
        frm_centerright.resize(frm_centerright_dmnsns[X], frm_centerright_dmnsns[Y])
        frm_right.resize(frm_right_dmnsns[X], frm_right_dmnsns[Y])
        frm_bottomfull.resize(frm_bottomfull_dmnsns[X], frm_bottomfull_dmnsns[Y])

        # frame style
        frm_topfull.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_topleft.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_topright.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_left.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_centerleft.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_centerright.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_right.setFrameShape(QtGui.QFrame.StyledPanel)
        frm_bottomfull.setFrameShape(QtGui.QFrame.StyledPanel)

        # place frames into splitters (vert)
        splitter_hrz1 = QtGui.QSplitter(QtCore.Qt.Horizontal)
        splitter_hrz1.addWidget(frm_topleft) 
        splitter_hrz1.addWidget(frm_topright)

        splitter_hrz2 = QtGui.QSplitter(QtCore.Qt.Horizontal)
        splitter_hrz2.addWidget(frm_left)
        splitter_hrz2.addWidget(frm_centerleft)
        splitter_hrz2.addWidget(frm_centerright)
        splitter_hrz2.addWidget(frm_right)

        # vertical splitter contains horizontal ones
        splitter_vert = QtGui.QSplitter(QtCore.Qt.Vertical)
        splitter_vert.addWidget(frm_topfull)
        splitter_vert.addWidget(splitter_hrz1)
        splitter_vert.addWidget(splitter_hrz2)
        splitter_vert.addWidget(frm_bottomfull)

        # Box layout to contain it all
        box_master = QtGui.QHBoxLayout(self)
        box_master.addWidget(splitter_vert)
        self.setLayout(box_master)
        #QtGui.QApplication.setStyle(QtGui.QStyleFactory.create('Cleanlooks'))

        ############################################################################################################################################
        #                  Put objects in correct frame using box layout class 
        ############################################################################################################################################

        ##########################
        # TX Config
        ##########################
        box_activate_tx = QtGui.QHBoxLayout()    
        box_activate_tx.addWidget(self.label_config_tx)
        box_activate_tx.addStretch(1)
        box_activate_tx.addWidget(self.btn_create_usrp_tx)

        box_addr_tx     = QtGui.QHBoxLayout()
        box_addr_tx.addWidget(self.label_addr_tx)
        box_addr_tx.addStretch(1)
        box_addr_tx.addWidget(self.address_input_bx_tx)

        box_rate_tx  = QtGui.QHBoxLayout()
        box_rate_tx.addWidget(self.label_rate_tx)
        box_rate_tx.addStretch(1)
        box_rate_tx.addWidget(self.rate_input_bx_tx)
        
        box_antenna_tx  = QtGui.QHBoxLayout()
        box_antenna_tx.addWidget(self.label_antenna_tx)
        box_antenna_tx.addStretch(1)
        box_antenna_tx.addWidget(self.antenna_select_tx)

        box_signal_tx   = QtGui.QHBoxLayout()
        box_signal_tx.addWidget(self.label_signal_tx)
        box_signal_tx.addStretch(1)
        box_signal_tx.addWidget(self.config_select_tx)

        box_config_left_tx   = QtGui.QVBoxLayout()
        box_config_left_tx.addLayout(box_addr_tx) 
        box_config_left_tx.addLayout(box_rate_tx)

        box_config_right_tx   = QtGui.QVBoxLayout()
        box_config_right_tx.addLayout(box_antenna_tx)
        box_config_right_tx.addLayout(box_signal_tx)

        box_config_both_tx   = QtGui.QHBoxLayout()
        box_config_both_tx.addLayout(box_config_left_tx)
        box_config_both_tx.addLayout(box_config_right_tx)

        box_config_tx   = QtGui.QVBoxLayout(frm_topleft)
        box_config_tx.addLayout(box_activate_tx)
        box_config_tx.addLayout(box_config_both_tx)


        ##########################
        # TX Signal
        ##########################
        # Carrier
        box_signal_tx = QtGui.QVBoxLayout(frm_left) 
        box_signal_tx.addWidget(self.label_cw_tx)
        box_signal_tx.addWidget(self.label_cw_tx_gain)
        box_signal_tx.addWidget(self.lcd_cw_amp_tx)
        box_signal_tx.addWidget(self.sld_cw_amp_tx)
        box_signal_tx.addWidget(self.label_cw_tx_freq)
        box_signal_tx.addWidget(self.lcd_cw_frq_tx)
        box_signal_tx.addWidget(self.sld_cw_frq_tx)

        # Modulation Tone
        box_signal_tx.addWidget(self.label_tone_ampl_tx)
        box_signal_tx.addWidget(self.lcd_tone_amp_tx)
        box_signal_tx.addWidget(self.sld_tone_amp_tx)
        box_signal_tx.addWidget(self.label_tone_freq_tx)
        box_signal_tx.addWidget(self.lcd_tone_frq_tx)
        box_signal_tx.addWidget(self.sld_tone_frq_tx)
        
        ###########################
        # TX Calibration
        ###########################
        # IQ Gain Balance
        box_iq_tx = QtGui.QVBoxLayout(frm_centerleft) 
        box_iq_tx.addWidget(self.label_cal_tx)
        box_iq_tx.addWidget(self.label_iq_I_correction_tx)
        box_iq_tx.addWidget(self.lcd_iq_I_correction_tx)
        box_iq_tx.addWidget(self.sld_iq_I_correction_tx)
        box_iq_tx.addWidget(self.label_iq_Q_correction_tx)
        box_iq_tx.addWidget(self.lcd_iq_Q_correction_tx)
        box_iq_tx.addWidget(self.sld_iq_Q_correction_tx)
        # IQ DC Offset
        box_iq_tx.addWidget(self.label_dc_I_offset_tx)
        box_iq_tx.addWidget(self.lcd_dc_I_offset_tx)
        box_iq_tx.addWidget(self.sld_dc_I_offset_tx)
        box_iq_tx.addWidget(self.label_dc_Q_offset_tx)
        box_iq_tx.addWidget(self.lcd_dc_Q_offset_tx)
        box_iq_tx.addWidget(self.sld_dc_Q_offset_tx)


        ##########################
        # RX Config
        ##########################
        box_activate_rx = QtGui.QHBoxLayout()    
        box_activate_rx.addWidget(self.label_config_rx)
        box_activate_rx.addStretch(1)
        box_activate_rx.addWidget(self.btn_create_usrp_rx)

        box_addr_rx     = QtGui.QHBoxLayout()
        box_addr_rx.addWidget(self.label_addr_rx)
        box_addr_rx.addStretch(1)
        box_addr_rx.addWidget(self.address_input_bx_rx)

        box_rate_rx  = QtGui.QHBoxLayout()
        box_rate_rx.addWidget(self.label_rate_rx)
        box_rate_rx.addStretch(1)
        box_rate_rx.addWidget(self.rate_input_bx_rx)
        
        box_antenna_rx  = QtGui.QHBoxLayout()
        box_antenna_rx.addWidget(self.label_antenna_rx)
        box_antenna_rx.addStretch(1)
        box_antenna_rx.addWidget(self.antenna_select_rx)

        

        box_config_left_rx   = QtGui.QVBoxLayout()
        box_config_left_rx.addLayout(box_addr_rx) 
        box_config_left_rx.addLayout(box_rate_rx)

        box_config_right_rx   = QtGui.QVBoxLayout()
        box_config_right_rx.addLayout(box_antenna_rx)
        #void

        box_config_both_rx   = QtGui.QHBoxLayout()
        box_config_both_rx.addLayout(box_config_left_rx)
        box_config_both_rx.addLayout(box_config_right_rx)

        box_config_rx   = QtGui.QVBoxLayout(frm_topright)
        box_config_rx.addLayout(box_activate_rx)
        box_config_rx.addLayout(box_config_both_rx)


        ##########################
        # RX Signal
        ##########################
        # CW
        box_signal_rx = QtGui.QVBoxLayout(frm_right) 
        box_signal_rx.addWidget(self.label_cw_rx)
        box_signal_rx.addWidget(self.label_cw_rx_gain)
        box_signal_rx.addWidget(self.lcd_cw_amp_rx)
        box_signal_rx.addWidget(self.sld_cw_amp_rx)
        box_signal_rx.addWidget(self.label_cw_rx_freq)
        box_signal_rx.addWidget(self.lcd_cw_frq_rx)
        box_signal_rx.addWidget(self.sld_cw_frq_rx)

        ###########################
        # RX Calibration
        ###########################
        # IQ Gain Balance
        box_iq_rx = QtGui.QVBoxLayout(frm_centerright) 
        box_iq_rx.addWidget(self.label_cal_rx)
        box_iq_rx.addWidget(self.label_iq_I_correction_rx)
        box_iq_rx.addWidget(self.lcd_iq_I_correction_rx)
        box_iq_rx.addWidget(self.sld_iq_I_correction_rx)
        box_iq_rx.addWidget(self.label_iq_Q_correction_rx)
        box_iq_rx.addWidget(self.lcd_iq_Q_correction_rx)
        box_iq_rx.addWidget(self.sld_iq_Q_correction_rx)
        # IQ DC Offset
        box_iq_rx.addWidget(self.label_dc_I_offset_rx)
        box_iq_rx.addWidget(self.lcd_dc_I_offset_rx)
        box_iq_rx.addWidget(self.sld_dc_I_offset_rx)
        box_iq_rx.addWidget(self.label_dc_Q_offset_rx)
        box_iq_rx.addWidget(self.lcd_dc_Q_offset_rx)
        box_iq_rx.addWidget(self.sld_dc_Q_offset_rx)



        #############################
        # Misc
        #############################
        box_misc = QtGui.QHBoxLayout(frm_bottomfull) 
        box_misc.addWidget(btn_sweep_cal_rx)
        box_misc.addWidget(btn_PSO_cal_rx)
        box_misc.addWidget(btn_PSO_viewer)
        #box_misc.addWidget(btn_PSO_test)
        box_misc.addStretch(1)     
        box_misc.addWidget(btn_quit)

        #####################################################################################################################################
        #          Signal and Slot - Connections
        #####################################################################################################################################
        ################ TRANSMITTER ###############
        # make usrp
        self.btn_create_usrp_tx.clicked.connect(self.create_usrp_tx_device_handler)
        # set tx antenna- TX
        self.antenna_select_tx.currentIndexChanged.connect(self.set_tx_antenna_handler)
        # tx device address -TX
        #self.address_input_bx_tx.returnPressed.connect(self.address_input_tx_text_changed_handler)
        self.address_input_bx_tx.textChanged[str].connect(self.address_input_tx_text_changed_handler)

        # TX Center Gain- TX
        self.sld_cw_amp_tx.valueChanged.connect(self.lcd_cw_amp_tx.display)
        self.sld_cw_amp_tx.valueChanged.connect(self.tx_cw_amp_sliderChanged_handler)
        # TX Carrier Freq- TX
        self.sld_cw_frq_tx.valueChanged.connect(self.lcd_cw_frq_tx.display)
        self.sld_cw_frq_tx.valueChanged.connect(self.tx_cw_frq_sliderChanged_handler)
        
        # IQ Balance I  - TX
        self.sld_iq_I_correction_tx.valueChanged.connect(self.tx_I_correction_sliderChanged_handler)
        # IQ Balancwe Q - TX
        self.sld_iq_Q_correction_tx.valueChanged.connect(self.tx_Q_correction_sliderChanged_handler)
        # DC Offset I   - TX
        self.sld_dc_I_offset_tx.valueChanged.connect(self.tx_dc_I_offset_sliderChanged_handler)
        # DC Offset Q   - TX
        self.sld_dc_Q_offset_tx.valueChanged.connect(self.tx_dc_Q_offset_sliderChanged_handler)
        
        # tx rate
        self.rate_input_bx_tx.returnPressed.connect(self.tx_rate_changed_handler)
        
        # Modulation tone amplitude
        self.sld_tone_amp_tx.valueChanged.connect(self.lcd_tone_amp_tx.display)
        # Modulation tone freq
        self.sld_tone_frq_tx.valueChanged.connect(self.lcd_tone_frq_tx.display)
        self.sld_tone_frq_tx.valueChanged.connect(self.tx_tone_frq_sliderChanged_handler)

        ################ RECEIVER ###############
        # make usrp  - RX
        self.btn_create_usrp_rx.clicked.connect(self.create_usrp_rx_device_handler)
        # set tx antenna - RX
        self.antenna_select_rx.currentIndexChanged.connect(self.set_rx_antenna_handler)
        # rx device address - RX
        #self.address_input_bx_rx.returnPressed.connect(self.address_input_rx_text_changed_handler)
        self.address_input_bx_rx.textChanged[str].connect(self.address_input_rx_text_changed_handler)

        # RX Center Gain - RX
        self.sld_cw_amp_rx.valueChanged.connect(self.lcd_cw_amp_rx.display)
        self.sld_cw_amp_rx.valueChanged.connect(self.rx_cw_amp_sliderChanged_handler)
        # RX Carrier Freq - RX
        self.sld_cw_frq_rx.valueChanged.connect(self.lcd_cw_frq_rx.display)
        self.sld_cw_frq_rx.valueChanged.connect(self.rx_cw_frq_sliderChanged_handler)
        
        # IQ Balance I   - RX
        self.sld_iq_I_correction_rx.valueChanged.connect(self.rx_I_gain_correction_sliderChanged_handler)
        # IQ Balancwe Q  - RX
        self.sld_iq_Q_correction_rx.valueChanged.connect(self.rx_Q_gain_correction_sliderChanged_handler)
        # DC Offset I    - RX
        self.sld_dc_I_offset_rx.valueChanged.connect(self.rx_dc_I_offset_sliderChanged_handler)
        # DC Offset Q    - RX
        self.sld_dc_Q_offset_rx.valueChanged.connect(self.rx_dc_Q_offset_sliderChanged_handler)
        
        # rx rate - RX
        self.rate_input_bx_rx.returnPressed.connect(self.rx_rate_changed_handler)


        ##### MISC #########
        # quit program
        btn_quit.clicked.connect(QtCore.QCoreApplication.instance().quit)
        # calbrate RX
        btn_sweep_cal_rx.clicked.connect(self.do_rx_sweep_calibration_handler)
        btn_PSO_cal_rx.clicked.connect(self.do_rx_PSO_calibration_handler)
        # PSO viewer
        btn_PSO_viewer.clicked.connect(self.PSO_viewer_handler)
        # PSO test
        #btn_PSO_test.clicked.connect(self.launch_PSO_test)

    def keyPressEvent(self, e):
        # press escape and widget will close
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()


    #####################################################################################################################################
    #                 UHD Event Handlers
    #####################################################################################################################################

    # create tx device
    def create_usrp_tx_device_handler(self):
        self.usrp_tx = liblv_uhd_pub_api.create_usrp_obj(str(self.device_address_tx))
        # set some optimum defaults 
        liblv_uhd_pub_api.assign_tx_rate(self.usrp_tx, self.tx_rate)
        liblv_uhd_pub_api.assign_tx_gain(self.usrp_tx, self.tx_center_gain)
        liblv_uhd_pub_api.set_tx_antenna(self.usrp_tx, str(self.tx_antenna))
        # set carrier f 
        liblv_uhd_pub_api.set_tx_center_frequency(self.usrp_tx, float(self.tx_center_freq * MHZ))
        # create a transmitter thread (uses default tone and ampl) 
        liblv_uhd_pub_api.make_tx_thread(self.usrp_tx)
        # reset tone and ampl 
        liblv_uhd_pub_api.update_transmission_tone(float(self.tx_wave_freq * KHZ), self.tx_wave_ampl)
        # set default calibration values
        #liblv_uhd_pub_api.set_tx_iq_balance_ratio(self.usrp_tx, complex(float(self.tx_iq_I_correction), float(self.tx_iq_Q_correction)))
        #liblv_uhd_pub_api.set_tx_iq_balance_ratio(self.usrp_tx, complex(float(self.tx_iq_I_correction), float(self.tx_iq_Q_correction)))
        #liblv_uhd_pub_api.set_tx_dc_offset(self.usrp_tx, complex(float(self.tx_dc_I_offset), float(self.tx_dc_Q_offset)))
        #liblv_uhd_pub_api.set_tx_dc_offset(self.usrp_tx, complex(float(self.tx_dc_I_offset), float(self.tx_dc_Q_offset)))


    # create rx device
    def create_usrp_rx_device_handler(self):
        self.usrp_rx = liblv_uhd_pub_api.create_usrp_obj(str(self.device_address_rx))
        # set some optimum defaults 
        liblv_uhd_pub_api.assign_rx_rate(self.usrp_rx, self.rx_rate)
        liblv_uhd_pub_api.assign_rx_gain(self.usrp_rx, self.rx_center_gain)
        liblv_uhd_pub_api.set_rx_antenna(self.usrp_rx, str(self.rx_antenna))
        # set carrier f 
        liblv_uhd_pub_api.set_rx_center_frequency(self.usrp_rx, float(self.rx_center_freq * MHZ))
        # create a transmitter thread (uses default tone and ampl) 
        liblv_uhd_pub_api.make_rx_thread(self.usrp_rx)
        # set default calibration values
        #liblv_uhd_pub_api.set_rx_iq_balance_ratio(self.usrp_rx, complex(float(self.rx_I_gain_correction), float(self.rx_Q_gain_correction)))
        #liblv_uhd_pub_api.set_rx_iq_balance_ratio(self.usrp_rx, complex(float(self.rx_I_gain_correction), float(self.rx_Q_gain_correction)))
        #liblv_uhd_pub_api.set_rx_dc_offset(self.usrp_rx, complex(float(self.rx_dc_I_offset), float(self.rx_dc_Q_offset)))
        #liblv_uhd_pub_api.set_rx_dc_offset(self.usrp_rx, complex(float(self.rx_dc_I_offset), float(self.rx_dc_Q_offset)))

    # set transmitter gain - tx
    def tx_cw_amp_sliderChanged_handler(self, val):
        if self.usrp_tx == 0:
            print "WARNING RX Device Not Created - data is populated"
            self.tx_center_gain = float(val)
        else:
            self.tx_center_gain = float(val)
            liblv_uhd_pub_api.assign_tx_gain(self.usrp_tx, self.tx_center_gain)

    # set receiver gain - rx
    def rx_cw_amp_sliderChanged_handler(self, val):
        if self.usrp_rx == 0:
            print "WARNING RX Device Not Created - data is populated"
            self.rx_center_gain = float(val)
        else:
            self.rx_center_gain = float(val)
            liblv_uhd_pub_api.assign_rx_gain(self.usrp_rx, self.rx_center_gain)

    # set center frequency - tx
    def tx_cw_frq_sliderChanged_handler(self, val):
        if self.usrp_tx == 0:
            print "WARNING TX Device Not Created - data is populated"
            self.tx_center_freq = float(val)
        else:
            self.tx_center_freq = float(val)
            liblv_uhd_pub_api.set_tx_center_frequency(self.usrp_tx, float(self.tx_center_freq * MHZ))
            self.lcd_cw_frq_tx.display(self.tx_center_freq)
            self.sld_cw_frq_tx.setValue(self.tx_center_freq) 

    # set center frequency - rx
    def rx_cw_frq_sliderChanged_handler(self, val):
        if self.usrp_rx == 0:
            print "WARNING RX Device Not Created - data is populated"
            self.rx_center_freq = float(val)
        else:
            self.rx_center_freq = float(val)
            liblv_uhd_pub_api.set_rx_center_frequency(self.usrp_rx, float(self.rx_center_freq * MHZ))
            self.lcd_cw_frq_rx.display(self.rx_center_freq)
            #self.sld_cw_frq_rx.setValue(self.rx_center_freq) ########## look here, how to do auto update whilst not conflicting with non cal use?

    # set_tx_iq_ I
    def tx_I_correction_sliderChanged_handler(self, val):
        if self.usrp_tx == 0:
            print "ERROR TX Device Not Created"
        else:
            self.tx_iq_I_correction = val / self.iqBalanceDivisor
            self.lcd_iq_I_correction_tx.display(self.tx_iq_I_correction)
            liblv_uhd_pub_api.set_tx_iq_balance_ratio(self.usrp_tx, complex(float(self.tx_iq_I_correction), float(self.tx_iq_Q_correction)))

    # set_tx_iq_ Q
    def tx_Q_correction_sliderChanged_handler(self, val):
        if self.usrp_tx == 0:
            print "ERROR TX Device Not Created"
        else:
            self.tx_iq_Q_correction = val / self.iqBalanceDivisor
            self.lcd_iq_Q_correction_tx.display(self.tx_iq_Q_correction)
            liblv_uhd_pub_api.set_tx_iq_balance_ratio(self.usrp_tx, complex(float(self.tx_iq_I_correction), float(self.tx_iq_Q_correction)))

    # set_tx_dc_ I
    def tx_dc_I_offset_sliderChanged_handler(self, val):
        if self.usrp_tx == 0:
            print "ERROR TX Device Not Created"
        else:
            self.tx_dc_I_offset = val / self.dcOffsetDivisor
            self.lcd_dc_I_offset_tx.display(self.tx_dc_I_offset)
            liblv_uhd_pub_api.set_tx_dc_offset(self.usrp_tx, complex(float(self.tx_dc_I_offset), float(self.tx_dc_Q_offset)))

    # set_tx_dc_ Q
    def tx_dc_Q_offset_sliderChanged_handler(self, val):
        if self.usrp_tx == 0:
            print "ERROR TX Device Not Created"
        else:
            self.tx_dc_Q_offset = val / self.dcOffsetDivisor
            self.lcd_dc_Q_offset_tx.display(self.tx_dc_Q_offset)
            liblv_uhd_pub_api.set_tx_dc_offset(self.usrp_tx, complex(float(self.tx_dc_I_offset), float(self.tx_dc_Q_offset)))

    # set_rx_iq_ I
    def rx_I_gain_correction_sliderChanged_handler(self, val):
        if self.usrp_rx == 0:
            print "ERROR RX Device Not Created"
        else:
            self.rx_I_gain_correction = val / self.iqBalanceDivisor
            self.lcd_iq_I_correction_rx.display(self.rx_I_gain_correction)
            liblv_uhd_pub_api.set_rx_iq_balance_ratio(self.usrp_rx, complex(float(self.rx_I_gain_correction), float(self.rx_Q_gain_correction)))
            self.show() # test if these actually work
            QtCore.QCoreApplication.processEvents() # unessesary?

    # set_rx_iq_ Q
    def rx_Q_gain_correction_sliderChanged_handler(self, val):
        if self.usrp_rx == 0:
            print "ERROR RX Device Not Created"
        else:
            self.rx_Q_gain_correction = val / self.iqBalanceDivisor
            self.lcd_iq_Q_correction_rx.display(self.rx_Q_gain_correction)
            liblv_uhd_pub_api.set_rx_iq_balance_ratio(self.usrp_rx, complex(float(self.rx_I_gain_correction), float(self.rx_Q_gain_correction)))
            self.show()
            QtCore.QCoreApplication.processEvents()

    # set_rx_dc_ I
    def rx_dc_I_offset_sliderChanged_handler(self, val):
        if self.usrp_rx == 0:
            print "ERROR RX Device Not Created"
        else:
            self.rx_dc_I_offset = val / self.dcOffsetDivisor
            self.lcd_dc_I_offset_rx.display(self.rx_dc_I_offset)
            liblv_uhd_pub_api.set_rx_dc_offset(self.usrp_rx, complex(float(self.rx_dc_I_offset), float(self.rx_dc_Q_offset)))
            self.show()
            QtCore.QCoreApplication.processEvents()

    # set_rx_dc_ Q
    def rx_dc_Q_offset_sliderChanged_handler(self, val):
        if self.usrp_rx == 0:
            print "ERROR RX Device Not Created"
        else:
            self.rx_dc_Q_offset = val / self.dcOffsetDivisor
            self.lcd_dc_Q_offset_rx.display(self.rx_dc_Q_offset)
            liblv_uhd_pub_api.set_rx_dc_offset(self.usrp_rx, complex(float(self.rx_dc_I_offset), float(self.rx_dc_Q_offset)))
            self.show()
            QtCore.QCoreApplication.processEvents()
        
    # tx device address
    def address_input_tx_text_changed_handler(self):
        self.device_address_tx = self.address_input_bx_tx.text()

    # rx device address
    def address_input_rx_text_changed_handler(self):
        self.device_address_rx = self.address_input_bx_rx.text()

    # tx rate
    def tx_rate_changed_handler(self):
        self.tx_rate = float(self.rate_input_bx_tx.text())

    # rx rate
    def rx_rate_changed_handler(self):
        self.rx_rate = float(self.rate_input_bx_rx.text())

    # tx antenna
    def set_tx_antenna_handler(self):
        self.tx_antenna = str(self.antenna_select_tx.currentText())

    # rx antenna
    def set_rx_antenna_handler(self):
        self.rx_antenna = str(self.antenna_select_rx.currentText())


    # set modulation tone frequency
    def tx_tone_frq_sliderChanged_handler(self, val):
        if self.usrp_tx == 0:
            print "WARNING TX Device Not Created - data is populated"
            self.tx_wave_freq = float(val)
        else:
            self.tx_wave_freq = float(val)
            liblv_uhd_pub_api.update_transmission_tone(float(self.tx_wave_freq * KHZ), self.tx_wave_ampl)

    def do_rx_sweep_calibration_handler(self):
        self.thr_sweep = threading.Thread(target=calibration_routine_sweep.rx_calibration(self))
        self.thr_sweep.start()


    def do_rx_PSO_calibration_handler(self):
        self.thr_PSO = threading.Thread(target=calibration_routine_PSO_v2.optimise_parameters(self))
        self.thr_PSO.start()

    def PSO_viewer_handler(self):
        calibration_routine_PSO_v2.launch_PSO_evo_viewer(self)


    #### still to implement?:

    # get_lo_locked 
    # kill_threads 


############################################################################################################################################
############################################################################################################################################
#                                                      Main Window                                                                         #
############################################################################################################################################
############################################################################################################################################

class applicationWindow(QtGui.QMainWindow):   		   # applicationWindow class inherits from QtGui.QWidget class
    
    def __init__(self):						           # applicationWindow class  constructor
        super(applicationWindow, self).__init__()	   # inherited class constructor
        # call initialisation routine
        self.initUI()						

    # initialisation method definition
    def initUI(self):						 	

    	# set up the main window properties
        self.setWindowTitle('UHD Controller')		     		     # title bar text
        self.resize(window_x, window_y)							     # window size
        self.center()											     # our centering on screen function
        self.setWindowIcon(QtGui.QIcon('~/mars/gui/signal.png'))     # fail
    
    	# The statusbar is a widget that is used for displaying status information.
    	self.statusBar().showMessage('Ready')  

        # an action example (open file)
        openFile = QtGui.QAction(QtGui.QIcon('open.png'), 'Open', self)
        openFile.setShortcut('Ctrl+O')
        openFile.setStatusTip('Open new File')
        openFile.triggered.connect(self.showFileOpenDialog)

    	# an action example (exit)
    	exitAction = QtGui.QAction(QtGui.QIcon('exit24.png'), 'Exit', self) # ICON FAIL
        exitAction.setShortcut('Ctrl+Q')						     # asigned hotkey
        exitAction.setStatusTip('Exit application')				     # updates status bar with roll over help text
        exitAction.triggered.connect(self.close)				     # triggered signal is connected to the close method

        # create menu bar - populated here with our exit action
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')						     # make file meneu in menu bar
        fileMenu.addAction(exitAction)							     # add our exit action example to file menu
        fileMenu.addAction(openFile) 

        # call and setup the widget
        master_widget = MasterWidget()
        self.setCentralWidget(master_widget)

        # do nothing button example
        #btn1 = QtGui.QPushButton('Button 1', self)                 # create button object
        #btn1.setToolTip('This is a <b>QPushButton</b> widget')     # rollover text for button
        #btn1.resize(btn1.sizeHint())                               # gives recomended size for button
        #btn1.move(10, 50 +topBarsOffsetY)                          # button position
        #btn1.clicked.connect(self.buttonClicked) 



        self.show()

    def center(self):						                        # centers window on screen (main window if you have 2)
        
        qr = self.frameGeometry()									# We get a rectangle specifying the geometry of the main window. This includes any window frame.
        cp = QtGui.QDesktopWidget().availableGeometry().center()	# We figure out the screen resolution of our monitor. And from this resolution, we get the center point.
        qr.moveCenter(cp)											# Our rectangle has already its width and height. Now we set the center of the rectangle to the center of the screen. 
        self.move(qr.topLeft())										# We move the top-left point of the application window to the top-left point of the qr rectangle, thus centering the window on our screen.
    

    def closeEvent(self, event):		                            # acts on window frame x button 
        
        reply = QtGui.QMessageBox.question(self, 'Message',
            "Are you sure to quit?", QtGui.QMessageBox.Yes | 
            QtGui.QMessageBox.No, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()    

    def buttonClicked(self):
        # multiple event s can call same handler, sender() method determines which one
        sender = self.sender()
        self.statusBar().showMessage(sender.text() + ' was pressed')

    def mousePressEvent(self, event):
        
        self.c.closeApp.emit()

    def showFileOpenDialog(self):

        fname, _ = QtGui.QFileDialog.getOpenFileName(self, 'Open file', '/home')
        
        f = open(fname, 'r')
        
        with f:
            data = f.read()
            self.textEdit.setText(data)

############################################################################################################################################
############################################################################################################################################
#                                                      Main                                                                                #
############################################################################################################################################
############################################################################################################################################

def main():
    
    app = QtGui.QApplication(sys.argv)
    appWin = applicationWindow()
    
    sys.exit(app.exec_())  		# exit clean with exit status

if __name__ == '__main__':
    main()