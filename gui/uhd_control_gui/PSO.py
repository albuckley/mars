

from collections import namedtuple
import random
import numpy
import math
from dataObject import DataObject

# control params
omega = 0.3925
phi_p = 2.5586
phi_g = 1.3358

# number of particles
S = 25

# best known position of all in swarm
g_x = 0.0
g_y = 0.0

# upper and lower bounds

I_lo = -1.0
I_up = 1.0
Q_lo = -1.0
Q_up = 1.0


#AParticle = namedtuple('AParticle', ['position_x', 'position_y', 'best_position_x', 'best_position_y', 'velocity_x', 'velocity_y'])

class AParticle(DataObject):
	__slots__ = ('position_x', 'position_y', 'best_position_x', 'best_position_y', 'velocity_x', 'velocity_y')




# this stuff didn't work, though at that earlier point it could have been results of different bug - apeared not to be able to access element of namedtuple that were complex
#pos  		= complex(random.uniform(I_lo, I_up),random.uniform(Q_lo, Q_up))
#best_pos 	= pos
#vel  		= complex(random.uniform(-abs(Q_up-Q_lo),abs(Q_up-Q_lo)),random.uniform(-abs(Q_up-Q_lo),abs(Q_up-Q_lo)))

#p[0] = AParticle(pos, best_pos,vel)	

p = []



def init_swarm(p, S, g_x, g_y):
	for i in range(S):

		pos_x =	random.uniform(I_lo, I_up)
		pos_y = random.uniform(Q_lo, Q_up)

		best_pos_x 	= pos_x
		best_pos_y 	= pos_y 

		vel_x = random.uniform(-abs(Q_up-Q_lo),abs(Q_up-Q_lo))
		vel_y = random.uniform(-abs(Q_up-Q_lo),abs(Q_up-Q_lo))

		new_p = AParticle(pos_x, pos_y, best_pos_x, best_pos_y, vel_x, vel_y)

		p.append(new_p)	

		if(function( p[i].position_x, p[i].position_y ) < function( g_x, g_y )):
			g_x = p[i].best_position_x
			g_y = p[i].best_position_y



def find_minimum(p, S, g_x, g_y):
	global omega, phi_p, phi_g

	for iteration in range(400):
		
		for i in range(S):


			# pick two random numbers
			r_p = random.uniform(0, 1)
			r_g = random.uniform(0, 1)

			# update particle velocity for each dimension
			p[i].velocity_x = omega*p[i].velocity_x + phi_p*r_p*(p[i].best_position_x - p[i].position_x) + phi_g*r_g*(g_x - p[i].position_x)
			p[i].velocity_y = omega*p[i].velocity_y + phi_p*r_p*(p[i].best_position_y - p[i].position_y) + phi_g*r_g*(g_y - p[i].position_y)

			# update particle position
			p[i].position_x = p[i].position_x + p[i].velocity_x
			p[i].position_y = p[i].position_y + p[i].velocity_y

			# if f(x) < f(p), update particle best known position
			if(function( p[i].position_x, p[i].position_y ) < function( p[i].best_position_x, p[i].best_position_y )):
				p[i].best_position_x = p[i].position_x
				p[i].best_position_y = p[i].position_y

				# if f(x) < f(g), update swarms best known position
				if(function( p[i].position_x, p[i].position_y ) < function( g_x, g_y )):
					g_x = p[i].best_position_x
					g_y = p[i].best_position_y
					print "best postion: ", g_x, g_y
		print p[5].best_position_x, p[5].best_position_y
	minimum = [g_x, g_y]
	return minimum


def function(x, y):

	# 4*x^2 +5*y^2 + 2*x + 3*y +2  >>>> around -0.3 and -0.3
	#z = 4*math.pow(x,2) +5*math.pow(y,2) + 2*x + 3*y +2
	return z


init_swarm(p, S, g_x, g_y)
minimum = find_minimum(p, S, g_x, g_y)

print "best postion: ", minimum[0], minimum[1]