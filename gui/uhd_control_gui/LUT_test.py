import csv

# paramter_code values:
CARRIER	= 0
IGAIN 	= 1
QGIAN	= 2
IDC		= 3
QDC 	= 4

with open('./cal_data/tx_manual.csv', 'rb') as data:
    rows = csv.reader(data)
    next(rows) #Skip the headers.
    cal_dat_tx_manual = [[float(item) for number, item in enumerate(row) if item ] for row in rows]



def look_up_calibration_value(carrier_f, paramter_code, data_matrix):
	n = 1
	while(not(carrier_f >= data_matrix[n-1][CARRIER] and carrier_f <=data_matrix[n][CARRIER])):
		n = n + 1
	if(carrier_f == data_matrix[n][CARRIER]):
		return data_matrix[n][paramter_code]
	elif(carrier_f == data_matrix[n-1][CARRIER]):
		return data_matrix[n-1][paramter_code]
	else:
		return interpolator(carrier_f, n, CARRIER, paramter_code, data_matrix)


def interpolator(value, index, reference, param_column, matrix):
	ratio = (value-matrix[index-1][reference])/(matrix[index][reference] - matrix[index-1][reference])
	interpolated_value = matrix[index-1][param_column] + ratio *(matrix[index][param_column]-matrix[index-1][param_column])
	return interpolated_value

CW = 3750

print look_up_calibration_value(CW, QDC, cal_dat_tx_manual)