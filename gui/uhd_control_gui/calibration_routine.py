
import liblv_uhd_pub_api
import csv
import time
import bisect
import cal_LUT

best_supression = 0
#IQiterations_A     = 4
#IQiterations_B     = 2
DCiterations    = 4

CAL_iterations_A = 2
CAL_iterations_B = 2

power_AVG_window_length = 20

# convergent algorythm step size
step_I      = 0.001
step_Q      = step_I
step_dc_I   = step_I
step_dc_Q   = step_I
step_size   = 0.1

# convergence resolution
targetSupReso = 0.1 #dBm

# CW cal frequencies
CW_start    = 400 # 400 MHz
CW_stop     = 4000 #4000 # 4000 MHz
CW_step     = 20  # 100 MHz

# modulation frequency range
f_mod_start = 0
f_mod_stop  = 0
Num_of_Fmod_trials = 5

# cal values to start with
initial_tx_I_gain_correction    = - 0.4
initial_tx_iq_Q_correction      = - 0.4

# initialise measuments vector
Supres          = [0.0]
# force entry to while loops for each IQiterations
delta_Supres    = 100
# initialise best result with a bad result
best_supression = 0

# initialise result vector index
index_all    = 0    # data tracking index
all_I        = [0.0]
all_Q        = [0.0]
all_supres   = [0.0]
all_deltaS   = [0.0]
all_index    = [0]

# initialise trial data 
r            = 0   # result index
result_CW    = ['CW']
result_Fmod  = ['Fmod'] 
result_Igain = ['Igain']
result_Qgain = ['Qgain']
result_Idc   = ['Idc']
result_Qdc   = ['Qdc']
result_SBpwr = ['SBpwr']
result_CWpwr = ['CWpwr']
result_Tonepwr = ['Tonepwr']
result_Imagpwr = ['Imagpwr']

# paramter_code values for LUT:
CARRIER = 0
IGAIN   = 1
QGIAN   = 2
IDC     = 3
QDC     = 4

# index for supression loop data
n = 0

# value from C++ wrapper
cal_data = [0.0, 0.0, 0.0]

# open files for recording data
results_filename = "results.csv"

# open the manual tx cal data file
with open('./cal_data/tx_manual_MARS.csv', 'rb') as data:
    rows = csv.reader(data)
    next(rows) #Skip the headers.
    cal_dat_tx_manual = [[float(item) for number, item in enumerate(row) if item ] for row in rows]

cw_samples = [500,750,1000,1500,2250,2500,2750,3000,3500,3800]

def rx_calibration(self):

    ######################################
    # calibrate for range of frequencies #
    ######################################
    #for CW in range(CW_start, CW_stop, CW_step):  
    for CW in cw_samples: 
        global step_size
        # user feedback display
        print CW
        #if CW == 500:
        #    break

        # get manual tx calibration data for this CW
        self.tx_iq_I_correction = cal_LUT.look_up_calibration_value(CW, IGAIN, cal_dat_tx_manual)
        self.tx_iq_Q_correction = cal_LUT.look_up_calibration_value(CW, QGIAN, cal_dat_tx_manual)
        self.tx_dc_I_offset     = cal_LUT.look_up_calibration_value(CW, IDC, cal_dat_tx_manual)
        self.tx_dc_Q_offset     = cal_LUT.look_up_calibration_value(CW, QDC, cal_dat_tx_manual)

        # set tx calibration
        self.tx_I_correction_sliderChanged_handler(self.tx_iq_I_correction * self.iqBalanceDivisor)
        self.tx_Q_correction_sliderChanged_handler(self.tx_iq_Q_correction * self.iqBalanceDivisor)
        self.tx_dc_I_offset_sliderChanged_handler(self.tx_dc_I_offset  * self.dcOffsetDivisor)
        self.tx_dc_Q_offset_sliderChanged_handler(self.tx_dc_Q_offset  * self.dcOffsetDivisor)

        # new frequencies
        self.tx_cw_frq_sliderChanged_handler(CW)
        self.rx_cw_frq_sliderChanged_handler(CW)  
        self.tx_tone_frq_sliderChanged_handler(CW + CW/2 + 7)
        #f_mod_start = CW + CW/2 + 20
        #f_mod_stop  = CW + CW/2 - 20

        #self.tx_wave_freq = find_mod_tone_with_best_intermod(self, f_mod_start, f_mod_stop)
        #self.tx_tone_frq_sliderChanged_handler(self.tx_wave_freq)


        # search paramters
        step_size   = 0.1
        search_bound   = 0.3
        range_low__Ign = -search_bound
        range_high_Ign = search_bound
        range_low__Qgn = -search_bound
        range_high_Qgn = search_bound
        range_low__Idc = -0.1
        range_high_Idc = 0.1
        range_low__Qdc = -0.1
        range_high_Qdc = 0.1

        self.rx_I_gain_correction   = 0.0
        self.rx_Q_gain_correction   = 0.0
        self.rx_dc_I_offset         = 0.0
        self.rx_dc_Q_offset         = 0.0

        self.rx_I_gain_correction_sliderChanged_handler(self.rx_I_gain_correction * self.iqBalanceDivisor)
        self.rx_Q_gain_correction_sliderChanged_handler(self.rx_Q_gain_correction * self.iqBalanceDivisor)
        self.rx_dc_I_offset_sliderChanged_handler(self.rx_dc_I_offset * self.dcOffsetDivisor) 
        self.rx_dc_Q_offset_sliderChanged_handler(self.rx_dc_Q_offset * self.dcOffsetDivisor)

        for iteration1 in range(4): #2
            #print "iteration1 ", iteration1
            step_size = step_size * 0.1

            if iteration1 == 0: # good to run twice on first iteration as each value effects the other and resolution is crude here
                for iteration2 in range(2): #3
                    #print "iteration2 ", iteration2
                    if iteration2 == 0:
                        self.rx_dc_I_offset       = sweep_for_best_supression(self, 'offset', 'I dc', self.rx_dc_I_offset, range_low__Idc, range_high_Idc, step_size)
                        self.rx_dc_I_offset_sliderChanged_handler(self.rx_dc_I_offset * self.dcOffsetDivisor) # assigning best value
                        #print "\n"
                        self.rx_dc_Q_offset       = sweep_for_best_supression(self, 'offset', 'Q dc', self.rx_dc_Q_offset, range_low__Qdc, range_high_Qdc, step_size)
                        self.rx_dc_Q_offset_sliderChanged_handler(self.rx_dc_Q_offset * self.dcOffsetDivisor)
                        #print "\n"
                        #print "Ig ", self.rx_I_gain_correction, "Qg ", self.rx_Q_gain_correction, "Id ", self.rx_dc_I_offset, "Qd ", self.rx_dc_Q_offset
                    else:
                        self.rx_I_gain_correction = sweep_for_best_supression(self, 'gain', 'I gain', self.rx_I_gain_correction, range_low__Ign, range_high_Ign, step_size)
                        self.rx_I_gain_correction_sliderChanged_handler(self.rx_I_gain_correction * self.iqBalanceDivisor)
                        #print "\n"
                        self.rx_Q_gain_correction = sweep_for_best_supression(self, 'gain', 'Q gain', self.rx_Q_gain_correction, range_low__Qgn, range_high_Qgn, step_size)
                        self.rx_Q_gain_correction_sliderChanged_handler(self.rx_Q_gain_correction * self.iqBalanceDivisor)
                        #print "\n"
                        #print "Ig ", self.rx_I_gain_correction, "Qg ", self.rx_Q_gain_correction, "Id ", self.rx_dc_I_offset, "Qd ", self.rx_dc_Q_offset
                        

            self.rx_dc_I_offset       = sweep_for_best_supression(self, 'offset', 'I dc', self.rx_dc_I_offset, range_low__Idc, range_high_Idc, step_size)
            self.rx_dc_I_offset_sliderChanged_handler(self.rx_dc_I_offset * self.dcOffsetDivisor)
            #print "\n"
            self.rx_dc_Q_offset       = sweep_for_best_supression(self, 'offset', 'Q dc', self.rx_dc_Q_offset, range_low__Qdc, range_high_Qdc, step_size)
            self.rx_dc_Q_offset_sliderChanged_handler(self.rx_dc_Q_offset * self.dcOffsetDivisor)
            #print "\n"

            self.rx_I_gain_correction = sweep_for_best_supression(self, 'gain', 'I gain', self.rx_I_gain_correction, range_low__Ign, range_high_Ign, step_size)
            self.rx_I_gain_correction_sliderChanged_handler(self.rx_I_gain_correction * self.iqBalanceDivisor)
            #print "\n"
            self.rx_Q_gain_correction = sweep_for_best_supression(self, 'gain', 'Q gain', self.rx_Q_gain_correction, range_low__Qgn, range_high_Qgn, step_size)
            self.rx_Q_gain_correction_sliderChanged_handler(self.rx_Q_gain_correction * self.iqBalanceDivisor)
            #print "\n"
            #print "Ig ", self.rx_I_gain_correction, "Qg ", self.rx_Q_gain_correction, "Id ", self.rx_dc_I_offset, "Qd ", self.rx_dc_Q_offset

            # rescale sweep range
            range_low__Ign = self.rx_I_gain_correction - step_size
            range_high_Ign = self.rx_I_gain_correction + step_size

            range_low__Qgn = self.rx_Q_gain_correction - step_size
            range_high_Qgn = self.rx_Q_gain_correction + step_size

            range_low__Idc = self.rx_dc_I_offset - step_size
            range_high_Idc = self.rx_dc_I_offset + step_size

            range_low__Qdc = self.rx_dc_Q_offset - step_size
            range_high_Qdc = self.rx_dc_Q_offset + step_size


        SB_supression = get_SB_supression_AVG(self)
        CW_power      = get_DC_pwr_AVG(self)
        tone_power    = get_tone_dbrms_AVG(self)
        imag_power    = get_imag_dbrms_AVG(self)

        print "\n\n\n"
        print "Final Values"
        print "I Gain:\t", self.rx_I_gain_correction
        print "Q Gain:\t", self.rx_Q_gain_correction
        print "I DC:\t", self.rx_dc_I_offset
        print "Q Dc:\t", self.rx_dc_Q_offset
        print "Best Supression: \t", SB_supression
        print "Dc power level: \t", CW_power
        print "tone power level: \t", tone_power
        print "imag power level: \t", imag_power

        record_results(self, CW, self.tx_wave_freq, self.rx_I_gain_correction, self.rx_Q_gain_correction, self.rx_dc_I_offset, self.rx_dc_Q_offset, SB_supression, CW_power, tone_power, imag_power)
    

    # end CW loop indent

    # print to file:
    resultsFile     = csv.writer(open(results_filename, "wb"))
    for writre_index in range(len(result_CW)):
        resultsFile.writerow([ result_CW[writre_index], result_Fmod[writre_index], result_Igain[writre_index], result_Qgain[writre_index], result_Idc[writre_index], result_Qdc[writre_index], result_SBpwr[writre_index], result_CWpwr[writre_index], result_Tonepwr[writre_index], result_Imagpwr[writre_index] ])

        # record results of calibration at this CW
        # record_results(self, CW)  <<< add dc stuff


    print "writing data to file"
    # store results to files
    #for rr in range(len(result_i)):
        #resultsFile.writerow(result_i[rr], result_cw[rr], result_I[rr], result_Q[rr], result_S[rr], result_fm[rr])
    # store evolution data
    #for dd in range(len(all_index)):
        #evo_dataFile.writerow(all_index[dd], all_I[dd], all_Q[dd], all_supres[dd], all_deltaS[dd])

    # close files
    #resultsFile.close()
    #evo_dataFile.close()
    ##########################
    #### end algorythm  ######
    ##########################
    print " ===== Completed Successfully ====="


    


def  sweep_for_best_supression(self, power_type, param_type, active_param, corr_start, corr_stop, corr_step):
    # tell python we don't intend to redefine this variable as local
    global n, step_size
    # reset measurements vector
    n = 0;
    best_supres = 0.0
    best_value = 0.0


    # sweep for corrections
    for correction in range(int(corr_start/step_size), int(corr_stop/step_size), int(corr_step/step_size)):
        
        active_param = correction * step_size
        if param_type == 'I gain':
            self.rx_I_gain_correction = active_param
            self.rx_I_gain_correction_sliderChanged_handler(self.rx_I_gain_correction * self.iqBalanceDivisor)
        elif param_type == 'Q gain' :
            self.rx_Q_gain_correction = active_param
            self.rx_Q_gain_correction_sliderChanged_handler(self.rx_Q_gain_correction * self.iqBalanceDivisor)
        elif param_type == 'I dc':
            self.rx_dc_I_offset = active_param
            self.rx_dc_I_offset_sliderChanged_handler(self.rx_dc_I_offset * self.dcOffsetDivisor)
        elif  param_type == 'Q dc' :
            self.rx_dc_Q_offset = active_param
            self.rx_dc_Q_offset_sliderChanged_handler(self.rx_dc_Q_offset * self.dcOffsetDivisor)
        # load AVG array of result data
        if power_type == 'gain':
            AVG_return_value = get_SB_supression_AVG(self)
        elif power_type == 'offset' :
            AVG_return_value = get_DC_pwr_AVG(self)
        
        Supres.append(AVG_return_value) 

        if(Supres[-1] < best_supres):
            best_supres = Supres[-1]
            best_value = active_param
        #print "power_type: ", power_type, "param_type: ", param_type, "\tcorrection\t", active_param, "\tpower    ", round(AVG_return_value, 3), "\tbest_supress\t", round(best_supres, 3) , "\tbest_corr\t", best_value
    return best_value




def get_SB_supression_AVG(self):
    #print "inside get_SB_supression_AVG()"
    sumData = 0
    for sample_index in range(power_AVG_window_length):
        #print "GET AVG SB  ", sample_index
        cal_data = liblv_uhd_pub_api.return_cal_data_SB_supression()
        #print "acquired sample ", sample_index, " from liblv_uhd_pub_api.return_cal_data()    value = ", cal_data
        sumData = sumData + cal_data
    average_SB_power = sumData / power_AVG_window_length
    #print "finnished get_SB_supression_AVG() with AVG = ", average_SB_power
    return average_SB_power

def get_DC_pwr_AVG(self):
    #print "inside get_SB_supression_AVG()"
    sumData = 0
    for sample_index in range(power_AVG_window_length):
        #print "GET AVG DC ", sample_index
        cal_data = liblv_uhd_pub_api.return_cal_data_CW_power()
        #print "acquired sample ", sample_index, " from liblv_uhd_pub_api.return_cal_data()    value = ", cal_data
        sumData = sumData + cal_data
    average_DC_power = sumData / power_AVG_window_length
    #print "finnished get_SB_supression_AVG() with AVG = ", average_DC_power
    return average_DC_power

def get_tone_dbrms_AVG(self):
    #print "inside get_tone_dbrms()"
    sumData = 0
    for sample_index in range(power_AVG_window_length):
        #print "GET TONE PWR ", sample_index
        cal_data = liblv_uhd_pub_api.return_tone_dbrms()
        #print "acquired sample ", sample_index, " from liblv_uhd_pub_api.return_tone_dbrms()    value = ", cal_data
        sumData = sumData + cal_data
    average_tone_power = sumData / power_AVG_window_length
    #print "finnished get_tone_dbrms() with AVG = ", average_DC_power
    return average_tone_power


def get_imag_dbrms_AVG(self):
    #print "inside get_imag_dbrms()"
    sumData = 0
    for sample_index in range(power_AVG_window_length):
        #print "GET IMAG PWR ", sample_index
        cal_data = liblv_uhd_pub_api.return_imag_dbrms()
        #print "acquired sample ", sample_index, " from liblv_uhd_pub_api.return_imag_dbrms()    value = ", cal_data
        sumData = sumData + cal_data
    average_imag_power = sumData / power_AVG_window_length
    #print "finnished get_imag_dbrms() with AVG = ", average_DC_power
    return average_imag_power


#######################################################################
#### record all results per CW - > CW, I_cal, Q_cal, Supress ##########
#######################################################################
def record_results(self, CW, Fmod, Igain, Qgain, Idc, Qdc, SB_supression, CW_power, tone_power, imag_power):
    result_CW.append(CW)
    result_Fmod.append(Fmod)
    result_Igain.append(round(Igain,6)) 
    result_Qgain.append(round(Qgain,6)) 
    result_Idc.append(round(Idc,6)) 
    result_Qdc.append(round(Qdc,6))
    result_SBpwr.append(round(SB_supression,1))
    result_CWpwr.append(round(CW_power,1))
    result_Tonepwr.append(round(tone_power,1))
    result_Imagpwr.append(round(imag_power,1))
    ++r




# find modulation frequency with lowest intermodulation products located at image frequency
def find_mod_tone_with_best_intermod(self, f_start, f_stop):
    global Num_of_Fmod_trials
    f_mod_step = (f_stop - f_start)/ Num_of_Fmod_trials
    # reset best result with a bad result
    best_supression_fmod = 0
    chosen_wave_f = 0
    for f_mod in range(f_start, f_stop, f_mod_step): 
        # set hardware
        self.tx_tone_frq_sliderChanged_handler(f_mod)
        # measure image rms power relative to signal (LSB SSB)
        supression_fmod = get_SB_supression_AVG(self)
        # values aught to be negative
        if(best_supression_fmod < supression_fmod):
            best_supression_fmod = supression_fmod
            chosen_wave_f = f_mod
        print "fmod trial: ", f_mod
    print "chose: ", chosen_wave_f
    return chosen_wave_f





