# Particle Swarm Optimisation ver. 3
# Maynooth Adaptive Radio Systems
# Alexander Buckley Feb 2014
#
# This first version uses a single 4 D swarm
# The swarm evaluates a cost function: 
# the combined carrier power (for DC offset) and image power (gain correction)
#
# no visual feedback (plotting)
#
# Initial conditions: random position

import random
import numpy
import math
import csv
import time
import sys

import liblv_uhd_pub_api
import cal_LUT
from dataObject import DataObject

from PySide import QtGui, QtCore, QtOpenGL
import pyqtgraph.opengl as gl
import numpy as np

ITERATIONS = 25#25

# control params
omega = -0.3593
phi_p = -0.7238
phi_g = 2.0289

# option to start with particle along perimiter instead of random
DO_PERIMITER_INIT = 1

# number of particles
S = 8 * 8 #25 #25    # must be divisible by 8 for initialisation perimiter method to work well (4 sides spanning 4 quadrants)

# constraints box (just use +ve numbers, *(-1) applied later when needed)
limit = 1.0
limit_gains_x = limit
limit_gains_y = limit
limit_ofset_x = limit
limit_ofset_y = limit

# upper and lower bounds (random number generator)
bound = 1.0 #0.5
Iofset_lo = -bound
Iofset_up =  bound
Qofset_lo = -bound
Qofset_up =  bound

Igains_lo = -bound
Igains_up =  bound
Qgains_lo = -bound
Qgains_up =  bound

power_AVG_window_length = 10

class AParticle(DataObject):
    __slots__ = ('position_Igains', 'position_Qgains', 'position_Iofset', 'position_Qofset','best_position_Igains', 'best_position_Qgains', 'best_position_Iofset', 'best_position_Qofset','velocity_Igains', 'velocity_Qgains', 'velocity_Iofset', 'velocity_Qofset')

class Solution(DataObject):
    __slots__ = ('I_gains','Q_gains','I_ofset','Q_ofset')

swarm_best_count  = 0
my_best = 0.0
swarm_best = 0.0
solutions = Solution(0.0, 0.0, 0.0, 0.0)  # best known position of all in swarm, ie 'g'

p = []

cw_samples = [500,750,1000,1500,2250,2500,2750,3000,3500,3800]

# CW cal frequencies
CW_start    = 400 # 400 MHz
CW_stop     = 4000 #4000 # 4000 MHz
CW_step     = 20  # 100 MHz

#### Results stuff
# initialise result vector index
index_all    = 0    # data tracking index
all_I        = [0.0]
all_Q        = [0.0]
all_supres   = [0.0]
all_deltaS   = [0.0]
all_index    = [0]

# initialise trial data 
r            = 0   # result index
result_CW    = ['CW']
result_Fmod  = ['Fmod'] 
result_Igain = ['Igain']
result_Qgain = ['Qgain']
result_Idc   = ['Idc']
result_Qdc   = ['Qdc']
result_SBpwr = ['SBpwr']
result_CWpwr = ['CWpwr']
result_Tonepwr = ['Tonepwr']
result_Imagpwr = ['Imagpwr']

# paramter_code values for LUT:
CARRIER = 0
IGAIN   = 1
QGIAN   = 2
IDC     = 3
QDC     = 4

# open files for recording data
results_filename = "results.csv"

# open the manual tx cal data file
with open('./cal_data/tx_manual_MARS.csv', 'rb') as data:
    rows = csv.reader(data)
    next(rows) #Skip the headers.
    cal_dat_tx_manual = [[float(item) for number, item in enumerate(row) if item ] for row in rows]

cw_samples = [500,750,1000,1500,2250,2500,2750,3000,3500,3800]

def init_swarm(self, p, S):
    # declare variables
    global swarm_best_count, my_best, swarm_best, solutions

    swarm_best_count = 0

    p = []

    
    pos = (0.0, 0.0, 0.0, 0.0)  #dimensions

    ini_PSO_plat_data(self,S)

    print "\n initializing swarm"

    for i in range(S):
        #print "making new particle ", i
        #sys.stdout.write(' p')
        #sys.stdout.write(str(i))
        QtCore.QCoreApplication.processEvents()

        #### init values ####

        # random method
        pos[0] = random.uniform(Igains_lo, Igains_up)  
        pos[1] = random.uniform(Qgains_lo, Qgains_up)
        pos[2] = random.uniform(Iofset_lo, Iofset_up)
        pos[3] = random.uniform(Qofset_lo, Qofset_up)

        # best position
        best_pos = pos

        # velocity
        vel[0] = random.uniform(-abs(Igains_up-Igains_lo),abs(Igains_up-Igains_lo))
        vel[1] = random.uniform(-abs(Qgains_up-Qgains_lo),abs(Qgains_up-Qgains_lo))
        vel[2] = random.uniform(-abs(Iofset_up-Iofset_lo),abs(Iofset_up-Iofset_lo))
        vel[3] = random.uniform(-abs(Qofset_up-Qofset_lo),abs(Qofset_up-Qofset_lo))

        #### create new data particle
        new_p = AParticle(pos[0], pos[1], pos[2], pos[3], best_pos[0], best_pos[1], best_pos[2], best_pos[3], vel[0], vel[1], vel[2], vel[3])

        #### add particle to swarm
        p.append(new_p) 

        # first iteration will always be individual best
        p[i].best_position_Igains = p[i].position_Igains
        p[i].best_position_Qgains = p[i].position_Qgains
        p[i].best_position_Iofset = p[i].position_Iofset
        p[i].best_position_Qofset = p[i].position_Qofset
   
        if(i==0): # first init value will always be best
            solutions.I_gains = p[i].best_position_Igains
            solutions.Q_gains = p[i].best_position_Qgains
            solutions.I_ofset = p[i].best_position_Iofset
            solutions.Q_ofset = p[i].best_position_Qofset


        # calculate power 
        new_pwr = pwr_function(self, p[i].position_Igains, p[i].position_Qgains, p[i].position_Iofset, p[i].position_Qofset)

        #set_gain_points(self, p[i].position_Igains * 10, p[i].position_Qgains * 10, p[i].position_Iofset * 10, p[i].position_Qofset * 10, new_gain_pwr, i)
        #set_individual_best_gain_points(self, p[i].best_position_Igains * 10, p[i].best_position_Qgains * 10, new_gain_pwr, p[i].best_position_Iofset * 10, p[i].best_position_Qofset * 10, new_gain_pwr,i) # literaly different on this use

        #### check which one is best in swarm
        if( new_pwr < swarm_best):
            #print new_gain_pwr, " is less than swarm ", swarm_gain_best
            solutions.I_gains = p[i].best_position_Igains
            solutions.Q_gains = p[i].best_position_Qgains
            solutions.I_ofset = p[i].best_position_Iofset
            solutions.Q_ofset = p[i].best_position_Qofset

            swarm_best = new_pwr
            swarm_best_count = swarm_best_count + 1
            #set_swarm_best_gain_points(self, solutions.I_gains * 10, solutions.Q_gains * 10, solutions.I_ofset * 10, solutions.Q_ofset * 10, swarm_best, swarm_best_count)
            #print "gain swarm best count: ", swarm_best_count_gain
        
        QtCore.QCoreApplication.processEvents()


    print " completed init swarm"

    return solutions


def find_minimum(self, p, S):
    global omega, phi_p, phi_g, swarm_best_count, my_best, swarm_best, solutions
    my_best = 0.0
    swarm_best = 0.0
    for iteration in range(1, ITERATIONS):  # should i ofset the count by one given init ran and populated arrays??
        print " iteration ", iteration, " of ", ITERATIONS
        for i in range(S):
            QtCore.QCoreApplication.processEvents()

            # pick two random numbers p, g
            r_p = random.uniform(0, 1)  
            r_g = random.uniform(0, 1)

            # update particle velocity for each dimension 
            p[i].velocity_Igains = omega*p[i].velocity_Igains + phi_p*r_p*(p[i].best_position_Igains - p[i].position_Igains) + phi_g*r_g*(solutions.I_gains - p[i].position_Igains)
            p[i].velocity_Qgains = omega*p[i].velocity_Qgains + phi_p*r_p*(p[i].best_position_Qgains - p[i].position_Qgains) + phi_g*r_g*(solutions.Q_gains - p[i].position_Qgains)
            p[i].velocity_Iofset = omega*p[i].velocity_Iofset + phi_p*r_p*(p[i].best_position_Iofset - p[i].position_Iofset) + phi_g*r_g*(solutions.I_ofset - p[i].position_Iofset)
            p[i].velocity_Qofset = omega*p[i].velocity_Qofset + phi_p*r_p*(p[i].best_position_Qofset - p[i].position_Qofset) + phi_g*r_g*(solutions.Q_ofset - p[i].position_Qofset)

            # update particle temp position variable 'Particle erratic'
            Pe_w = p[i].position_Igains + p[i].velocity_Igains
            Pe_x = p[i].position_Qgains + p[i].velocity_Qgains
            Pe_y = p[i].position_Iofset + p[i].velocity_Iofset
            Pe_z = p[i].position_Qofset + p[i].velocity_Qofset

            # Make sure particle within constraints
            p[i].position_Igains = check_in_box(Pe_w, p[i].position_Igains, limit_gains_x)
            p[i].position_Qgains = check_in_box(Pe_x, p[i].position_Qgains, limit_gains_y)
            p[i].position_Iofset = check_in_box(Pe_y, p[i].position_Iofset, limit_ofset_x)
            p[i].position_Qofset = check_in_box(Pe_z, p[i].position_Qofset, limit_ofset_y)

            # f(x)
            new_pwr = pwr_function(self, p[i].position_Igains, p[i].position_Qgains, p[i].position_Iofset, p[i].position_Qofset)
            # f(p)
            #my_best = pwr_function(self, p[i].best_position_Igains, p[i].best_position_Qgains, p[i].best_position_Iofset, p[i].best_position_Qofset)
            # f(g)
            #swarm_best = pwr_function(self, solutions.I_gains, solutions.Q_gains, solutions.I_ofset, solutions.Q_ofset ) 


            # if f(x) < f(p), update particle best known position
            if(new_pwr < my_best):
                p[i].best_position_Igains = p[i].position_Igains
                p[i].best_position_Qgains = p[i].position_Qgains
                p[i].best_position_Iofset = p[i].position_Iofset
                p[i].best_position_Qofset = p[i].position_Qofsetofset
                my_best = new_pwr

            # if f(x) < f(g), update swarms best known position
            if(new_pwr < swarm_best):
                solutions.I_gains = p[i].position_Igains
                solutions.Q_gains = p[i].position_Qgains
                solutions.I_ofset = p[i].position_Iofset
                solutions.Q_ofset = p[i].position_Qofset
                swarm_best = new_pwr
                swarm_best_count = swarm_best_count + 1
  
            #print "  best postion gains: ", solutions.I_gains,"\t", solutions.Q_gains,"\t", "best postion ofset: ", solutions.I_ofset,"\t", solutions.Q_ofset

        print " swarm gain best ", swarm_gain_best, " swarm ofset best ", swarm_ofset_best


    print "\n completed minimum search"
    print "best gains: ", solutions.I_gains, solutions.Q_gains
    print "best ofset: ", solutions.I_ofset, solutions.Q_ofset
    print "\n"
    return solutions



def pwr_function(self, w, x, y, z):
    #z = 3*math.pow(x,2) +6*math.pow(y,2) + 2*x + 3*y +1
    # DO COST FUNCTION OF BOTH
    self.rx_I_gain_correction = w
    self.rx_Q_gain_correction = x
    self.rx_dc_I_offset = y
    self.rx_dc_Q_offset = z

    self.rx_I_gain_correction_sliderChanged_handler(self.rx_I_gain_correction * self.iqBalanceDivisor)
    self.rx_Q_gain_correction_sliderChanged_handler(self.rx_Q_gain_correction * self.iqBalanceDivisor)
    self.rx_dc_I_offset_sliderChanged_handler(self.rx_dc_I_offset * self.dcOffsetDivisor)
    self.rx_dc_Q_offset_sliderChanged_handler(self.rx_dc_Q_offset * self.dcOffsetDivisor)

    carrier_pwr = get_DC_pwr_AVG(self)
    imag_power = get_imag_dbrms_AVG(self)

    power_cost = carrier_pwr + imag_power
    return power_cost


def function(w, x, y, z): 
# expand this for 4 d test
    # 4*x^2 +5*y^2 + 2*x + 3*y +2  >>>> around -0.3 and -0.3
    z = 4*math.pow(x,2) +5*math.pow(y,2) + 2*x + 3*y +2
    return z


###############################################################################################
###############################################################################################
###############################################################################################
#                                   MAIN routine
###############################################################################################
###############################################################################################
###############################################################################################
def optimise_parameters(self):


    #for CW in range(CW_start, CW_stop, CW_step): 
    for CW in cw_samples: 
    #for CW in range(3000, 3200, 200):
        print " Frequency (cw): ", CW

        # get manual tx calibration data for this CW
        self.tx_iq_I_correction = cal_LUT.look_up_calibration_value(CW, IGAIN, cal_dat_tx_manual)
        self.tx_iq_Q_correction = cal_LUT.look_up_calibration_value(CW, QGIAN, cal_dat_tx_manual)
        self.tx_dc_I_offset     = cal_LUT.look_up_calibration_value(CW, IDC, cal_dat_tx_manual)
        self.tx_dc_Q_offset     = cal_LUT.look_up_calibration_value(CW, QDC, cal_dat_tx_manual)

        # set tx calibration
        self.tx_I_correction_sliderChanged_handler(self.tx_iq_I_correction * self.iqBalanceDivisor)
        self.tx_Q_correction_sliderChanged_handler(self.tx_iq_Q_correction * self.iqBalanceDivisor)
        self.tx_dc_I_offset_sliderChanged_handler(self.tx_dc_I_offset  * self.dcOffsetDivisor)
        self.tx_dc_Q_offset_sliderChanged_handler(self.tx_dc_Q_offset  * self.dcOffsetDivisor)

        # new frequencies
        self.tx_cw_frq_sliderChanged_handler(CW)
        self.rx_cw_frq_sliderChanged_handler(CW)  
        self.tx_tone_frq_sliderChanged_handler(CW + CW/2 + 7)

        # do PSO
        solutions = init_swarm(self, p, S)
        minimum   = find_minimum(self, p, S, solutions)

        SB_supression = 0
        CW_power      = get_DC_pwr_AVG(self)
        tone_power    = get_tone_dbrms_AVG(self)
        imag_power    = get_imag_dbrms_AVG(self)

        self.rx_I_gain_correction   = solutions.I_gains
        self.rx_Q_gain_correction   = solutions.Q_gains
        self.rx_dc_I_offset         = solutions.I_ofset
        self.rx_dc_Q_offset         = solutions.Q_ofset


        print "\n"
        print "Final Values"
        print "I Gain:\t", self.rx_I_gain_correction
        print "Q Gain:\t", self.rx_Q_gain_correction
        print "I DC:\t", self.rx_dc_I_offset
        print "Q Dc:\t", self.rx_dc_Q_offset
        print "Best Supression: \t", SB_supression
        print "Dc power level: \t", CW_power
        print "tone power level: \t", tone_power
        print "imag power level: \t", imag_power
        print "\n\n\n"

        record_results(self, CW, self.tx_wave_freq, self.rx_I_gain_correction, self.rx_Q_gain_correction, self.rx_dc_I_offset, self.rx_dc_Q_offset, SB_supression, CW_power, tone_power, imag_power)


    ##########################
    #### end algorythm  ######
    ##########################

    # print to file:
    resultsFile     = csv.writer(open(results_filename, "wb"))
    for writre_index in range(len(result_Igain)):
        resultsFile.writerow([ result_CW[writre_index], result_Fmod[writre_index], result_Igain[writre_index], result_Qgain[writre_index], result_Idc[writre_index], result_Qdc[writre_index], result_SBpwr[writre_index], result_CWpwr[writre_index], result_Tonepwr[writre_index], result_Imagpwr[writre_index] ])


    #resultsFile.close()
    print " ===== Completed Successfully ====="

###############################################################################################################
##
###############################################################################################################


def check_in_box(Pe, Pi, lim):
    count = 0
    Pnew = 2
    if(abs(Pe)>lim):
        while(abs(Pnew)>abs(lim)):
            count = count + 1
            #sys.stdout.write('.')
            Pnew = bounce(Pe, Pi, lim)
            Pe = Pnew
            #if(count > 5):
                #print "Pe ", Pe, " Pi ", Pi, "Pnew", Pnew, "  out of control bouncing"
            #if(count > 10):
                #sys.exit()
    else:
        Pnew = Pe
    return Pnew

def bounce(Pe, Pi, lim):
    if(Pe<lim):
        lim = lim * (-1)
    return 2 * lim - Pe


def get_SB_supression_AVG(self):
    #print "inside get_SB_supression_AVG()"
    sumData = 0
    for sample_index in range(power_AVG_window_length):
        #sys.stdout.write('@')
        #print "GET AVG SB  ", sample_index
        cal_data = liblv_uhd_pub_api.return_cal_data_SB_supression()
        #print "acquired sample ", sample_index, " from liblv_uhd_pub_api.return_cal_data()    value = ", cal_data
        sumData = sumData + cal_data
    average_SB_power = sumData / power_AVG_window_length
    #print "finnished get_SB_supression_AVG() with AVG = ", average_SB_power
    return average_SB_power

def get_DC_pwr_AVG(self):
    #print "inside get_SB_supression_AVG()"
    sumData = 0
    #sys.stdout.write(' ')
    for sample_index in range(power_AVG_window_length):
        #sys.stdout.write('D')
        #print "GET AVG DC ", sample_index
        cal_data = liblv_uhd_pub_api.return_cal_data_CW_power()
        #print "acquired sample ", sample_index, " from liblv_uhd_pub_api.return_cal_data()    value = ", cal_data
        sumData = sumData + cal_data
    average_DC_power = sumData / power_AVG_window_length
    #print "finnished get_SB_supression_AVG() with AVG = ", average_DC_power
    return average_DC_power

def get_tone_dbrms_AVG(self):
    #print "inside get_tone_dbrms()"
    sumData = 0
    #sys.stdout.write(' ')
    for sample_index in range(power_AVG_window_length):
        #sys.stdout.write('T')
        #print "GET TONE PWR ", sample_index
        cal_data = liblv_uhd_pub_api.return_tone_dbrms()
        #print "acquired sample ", sample_index, " from liblv_uhd_pub_api.return_tone_dbrms()    value = ", cal_data
        sumData = sumData + cal_data
    average_tone_power = sumData / power_AVG_window_length
    #print "finnished get_tone_dbrms() with AVG = ", average_DC_power
    return average_tone_power


def get_imag_dbrms_AVG(self):
    #print "inside get_imag_dbrms()"
    sumData = 0
    #sys.stdout.write(' ')
    for sample_index in range(power_AVG_window_length):
        #sys.stdout.write('S')
        #print "GET IMAG PWR ", sample_index
        cal_data = liblv_uhd_pub_api.return_imag_dbrms()
        #print "acquired sample ", sample_index, " from liblv_uhd_pub_api.return_imag_dbrms()    value = ", cal_data
        sumData = sumData + cal_data
    average_imag_power = sumData / power_AVG_window_length
    #print "finnished get_imag_dbrms() with AVG = ", average_DC_power
    return average_imag_power



#######################################################################
#### record all results per CW - > CW, I_cal, Q_cal, Supress ##########
#######################################################################
def record_results(self, CW, Fmod, Igain, Qgain, Idc, Qdc, SB_supression, CW_power, tone_power, imag_power):
    result_CW.append(CW)
    result_Fmod.append(Fmod)
    result_Igain.append(round(Igain,6)) 
    result_Qgain.append(round(Qgain,6)) 
    result_Idc.append(round(Idc,6)) 
    result_Qdc.append(round(Qdc,6))
    result_SBpwr.append(round(SB_supression,1))
    result_CWpwr.append(round(CW_power,1))
    result_Tonepwr.append(round(tone_power,1))
    result_Imagpwr.append(round(imag_power,1))
    ++r


